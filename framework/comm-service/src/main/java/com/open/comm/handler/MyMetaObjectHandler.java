package com.open.comm.handler;

import com.baomidou.mybatisplus.mapper.MetaObjectHandler;
import org.apache.ibatis.reflection.MetaObject;
import org.springframework.stereotype.Component;

import java.util.Date;


/**
 * mybatisplus自定义填充公共字段 ,即没有传的字段自动填充
 */
@Component
public class MyMetaObjectHandler extends MetaObjectHandler {
	//新增填充
	@Override
	public void insertFill(MetaObject metaObject) {
		Object createTime = metaObject.getValue("createTime");
		Object updateTime = metaObject.getValue("updateTime");

		Date date = null;
		System.out.println("------------into-----------");
		if (createTime == null) {
			date = new Date();
			metaObject.setValue("createTime", date);
		}
		if (updateTime == null) {
			if (date == null)
				date = new Date();
			metaObject.setValue("updateTime", date);
		}

	}

	//更新填充
	@Override
	public void updateFill(MetaObject metaObject) {
		Object updateTime = metaObject.getValue("updateTime");
		if (updateTime == null) {
			metaObject.setValue("updateTime", new Date());
		}
		insertFill(metaObject);
	}
}