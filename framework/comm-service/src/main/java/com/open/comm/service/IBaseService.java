package com.open.comm.service;

import com.baomidou.mybatisplus.service.IService;

/**
 * @date: 2017-06-10 23:12
 * @author: xfz
 */
public interface IBaseService<T> extends IService<T> {
}
