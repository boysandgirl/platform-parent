package com.open.comm.service;


import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.baomidou.mybatisplus.mapper.Wrapper;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.baomidou.mybatisplus.toolkit.CollectionUtils;
import com.open.comm.bean.DataEntity;
import org.apache.ibatis.logging.Log;
import org.apache.ibatis.logging.LogFactory;
import org.apache.ibatis.session.SqlSession;

import java.util.List;

/**
 * @date: 2017-06-10 23:11
 * @author: xfz
 */
public class BaseServiceImpl<M extends BaseMapper<T>, T extends DataEntity<T>> extends ServiceImpl<M, T> {
	private static final Log logger = LogFactory.getLog(BaseServiceImpl.class);

	@Override
	public boolean insert(T entity) {
		entity.preInsert();
		return super.insert(entity);
	}

	@Override
	public boolean insertBatch(List<T> entityList) {

		return insertBatch(entityList, 30);
	}

	@Override
	public boolean insertBatch(List<T> entityList, int batchSize) {
		if (CollectionUtils.isEmpty(entityList)) {
			throw new IllegalArgumentException("Error: entityList must not be empty");
		} else {
			SqlSession batchSqlSession = this.sqlSessionBatch();

			try {
				int size = entityList.size();

				for (int i = 0; i < size; ++i) {
					T t = entityList.get(i);
					t.preInsert();
					this.baseMapper.insert(t);
					if (i % batchSize == 0) {
						batchSqlSession.flushStatements();
					}
				}

				batchSqlSession.flushStatements();
				return true;
			} catch (Exception var6) {
				logger.warn("Error: Cannot execute insertBatch Method. Cause:" + var6);
				return false;
			}
		}
	}

	@Override
	public boolean insertOrUpdateBatch(List<T> entityList) {

		return insertOrUpdateBatch(entityList, 30);

	}

	@Override
	public boolean insertOrUpdateBatch(List<T> entityList, int batchSize) {


		if (CollectionUtils.isEmpty(entityList)) {
			throw new IllegalArgumentException("Error: entityList must not be empty");
		} else {
			try {
				SqlSession batchSqlSession = this.sqlSessionBatch();
				int size = entityList.size();

				for (int i = 0; i < size; ++i) {
					T t = entityList.get(i);
					if (t.getId() != null) {
						t.preUpdate();
					} else {
						t.preInsert();
					}
					this.insertOrUpdate(t);
					if (i % batchSize == 0) {
						batchSqlSession.flushStatements();
					}
				}

				batchSqlSession.flushStatements();
				return true;
			} catch (Exception var6) {
				logger.warn("Error: Cannot execute insertOrUpdateBatch Method. Cause:" + var6);
				return false;
			}
		}

	}

	@Override
	public boolean insertOrUpdate(T entity) {
		if (entity != null) {
			if (entity.getId() != null) {
				entity.preUpdate();
			} else {
				entity.preInsert();
			}
			return super.insertOrUpdate(entity);
		}
		return false;

	}

	@Override
	public boolean update(T entity, Wrapper<T> wrapper) {
		entity.preUpdate();
		return super.update(entity, wrapper);
	}

	@Override
	public boolean updateBatchById(List<T> entityList) {
		if (CollectionUtils.isEmpty(entityList)) {
			throw new IllegalArgumentException("Error: entityList must not be empty");
		} else {
			SqlSession batchSqlSession = this.sqlSessionBatch();

			try {
				int size = entityList.size();

				for (int i = 0; i < size; ++i) {
					T t = entityList.get(i);
					t.preUpdate();
					this.baseMapper.updateById(t);
					if (i % 30 == 0) {
						batchSqlSession.flushStatements();
					}
				}

				batchSqlSession.flushStatements();
				return true;
			} catch (Exception var5) {
				logger.warn("Error: Cannot execute insertBatch Method. Cause:" + var5);
				return false;
			}
		}
	}

	@Override
	public boolean updateById(T entity) {

		if (entity != null) {
			entity.preUpdate();
			return super.updateById(entity);
		}
		return false;
	}
}
