package com.open.comm.utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

public class DateUtil {

	/**
	 * 一天的毫秒数
	 */
	public static final long MILLI_SECOND_PER_DAY = 86400000;
	/**
	 * 一小时的毫秒数
	 */
	public static final long MILLI_SECOND_PER_HOUR = 3600000;
	/**
	 * 一分钟的毫秒数
	 */
	public static final long MILLI_SECOND_PER_MIN = 60000;
	/**
	 * 一秒钟的毫秒数
	 */
	public static final long MIN_NS = 1000;// 一秒钟的毫秒数

	public static final int DAY = 1;
	public static final int HOUR = 2;
	public static final int MIN = 3;
	public static final int SECOND = 4;
	public static final int DAY_OF_WEEK = 7;

	private static final long ONE_MINUTE = 60000L;
	private static final long ONE_HOUR = 3600000L;
	private static final long ONE_DAY = 86400000L;
	private static final long ONE_WEEK = 604800000L;

	private static final String ONE_SECOND_AGO = "秒前";
	private static final String ONE_MINUTE_AGO = "分钟前";
	private static final String ONE_HOUR_AGO = "小时前";
	private static final String ONE_DAY_AGO = "天前";
	private static final String ONE_MONTH_AGO = "月前";
	private static final String ONE_YEAR_AGO = "年前";


	/**
	 * 日期枚举类
	 *
	 * @author 徐福周
	 */
	public enum DateStyle {
		YYYY_EN("yyyy"), YYYY_CN("yyyy年"),

		MM_DD("MM-dd"), YYYY_MM("yyyy-MM"), YYYY_MM_DD("yyyy-MM-dd"), MM_DD_HH_MM(
				"MM-dd HH:mm"), MM_DD_HH_MM_SS("MM-dd HH:mm:ss"), YYYY_MM_DD_HH_MM(
				"yyyy-MM-dd HH:mm"), YYYY_MM_DD_HH_MM_SS("yyyy-MM-dd HH:mm:ss"),

		MM_DD_EN("MM/dd"), YYYY_MM_EN("yyyy/MM"), YYYY_MM_DD_EN("yyyy/MM/dd"), MM_DD_HH_MM_EN(
				"MM/dd HH:mm"), MM_DD_HH_MM_SS_EN("MM/dd HH:mm:ss"), YYYY_MM_DD_HH_MM_EN(
				"yyyy/MM/dd HH:mm"), YYYY_MM_DD_HH_MM_SS_EN(
				"yyyy/MM/dd HH:mm:ss"),

		MM_DD_CN("MM月dd日"), YYYY_MM_CN("yyyy年MM月"), YYYY_MM_DD_CN("yyyy年MM月dd日"), MM_DD_HH_MM_CN(
				"MM月dd日 HH:mm"), MM_DD_HH_MM_SS_CN("MM月dd日 HH:mm:ss"), YYYY_MM_DD_HH_MM_CN(
				"yyyy年MM月dd日 HH:mm"), YYYY_MM_DD_HH_MM_SS_CN(
				"yyyy年MM月dd日 HH:mm:ss"),

		HH_MM("HH:mm"), HH_MM_SS("HH:mm:ss");

		private String value;

		DateStyle(String value) {
			this.value = value;
		}

		public String getValue() {
			return value;
		}
	}

	/**
	 * 星期枚举类
	 *
	 * @author 徐福周
	 */
	public enum Week {

		SUNDAY("星期日", "Sunday", "Sun.", 7),
		MONDAY("星期一", "Monday", "Mon.", 1),
		TUESDAY("星期二", "Tuesday", "Tues.", 2),
		WEDNESDAY("星期三", "Wednesday", "Wed.", 3),
		THURSDAY("星期四", "Thursday", "Thur.", 4),
		FRIDAY("星期五", "Friday", "Fri.", 5),
		SATURDAY("星期六", "Saturday", "Sat.", 6);

		String name_cn;
		String name_en;
		String name_enShort;
		int number;

		Week(String name_cn, String name_en, String name_enShort, int number) {
			this.name_cn = name_cn;
			this.name_en = name_en;
			this.name_enShort = name_enShort;
			this.number = number;
		}

		public String getChineseName() {
			return name_cn;
		}

		public String getName() {
			return name_en;
		}

		public String getShortName() {
			return name_enShort;
		}

		public int getNumber() {
			return number;
		}
	}

	/**
	 * 获取SimpleDateFormat
	 *
	 * @param parttern 日期格式
	 * @return SimpleDateFormat对象
	 * @throws RuntimeException 异常：非法日期格式
	 */
	private static SimpleDateFormat getDateFormat(String parttern)
			throws RuntimeException {
		return new SimpleDateFormat(parttern);
	}

	/**
	 * 根据日期格式转化时间
	 *
	 * @param date    时间
	 * @param partern ---日期轉化的格式如：yyyy-MM-dd HH:mm:ss
	 * @return
	 */
	public static String getFormatTime(Date date, String partern) {
		return (getDateFormat(partern)).format(date);
	}

	/**
	 * 获取日期中的某数值。如获取月份
	 *
	 * @param date     日期
	 * @param dateType 日期格式
	 * @return 数值
	 */
	private static int getInteger(Date date, int dateType) {
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		return calendar.get(dateType);
	}

	/**
	 * 增加日期中某类型的某数值。如增加日期
	 *
	 * @param date     日期字符串
	 * @param dateType 类型
	 * @param amount   数值
	 * @return 计算后日期字符串
	 */
	private static String addInteger(String date, int dateType, int amount) {
		String dateString = null;
		DateStyle dateStyle = getDateStyle(date);
		if (dateStyle != null) {
			Date myDate = StringToDate(date, dateStyle);
			myDate = addInteger(myDate, dateType, amount);
			dateString = DateToString(myDate, dateStyle);
		}
		return dateString;
	}

	/**
	 * 增加日期中某类型的某数值。如增加日期
	 *
	 * @param date     日期
	 * @param dateType 类型
	 * @param amount   数值
	 * @return 计算后日期
	 */
	private static Date addInteger(Date date, int dateType, int amount) {
		Date myDate = null;
		if (date != null) {
			Calendar calendar = Calendar.getInstance();
			calendar.setTime(date);
			calendar.add(dateType, amount);
			myDate = calendar.getTime();
		}
		return myDate;
	}

	/**
	 * 获取精确的日期
	 *
	 * @param timestamps 时间long集合
	 * @return 日期
	 */
	private static Date getAccurateDate(List<Long> timestamps) {
		Date date = null;
		long timestamp = 0;
		Map<Long, long[]> map = new HashMap<Long, long[]>();
		List<Long> absoluteValues = new ArrayList<Long>();

		if (timestamps != null && timestamps.size() > 0) {
			if (timestamps.size() > 1) {
				for (int i = 0; i < timestamps.size(); i++) {
					for (int j = i + 1; j < timestamps.size(); j++) {
						long absoluteValue = Math.abs(timestamps.get(i)
								- timestamps.get(j));
						absoluteValues.add(absoluteValue);
						long[] timestampTmp = {timestamps.get(i),
								timestamps.get(j)};
						map.put(absoluteValue, timestampTmp);
					}
				}

				// 有可能有相等的情况。如2012-11和2012-11-01。时间戳是相等的
				long minAbsoluteValue = -1;
				if (!absoluteValues.isEmpty()) {
					// 如果timestamps的size为2，这是差值只有一个，因此要给默认值
					minAbsoluteValue = absoluteValues.get(0);
				}
				for (int i = 0; i < absoluteValues.size(); i++) {
					for (int j = i + 1; j < absoluteValues.size(); j++) {
						if (absoluteValues.get(i) > absoluteValues.get(j)) {
							minAbsoluteValue = absoluteValues.get(j);
						} else {
							minAbsoluteValue = absoluteValues.get(i);
						}
					}
				}

				if (minAbsoluteValue != -1) {
					long[] timestampsLastTmp = map.get(minAbsoluteValue);
					if (absoluteValues.size() > 1) {
						timestamp = Math.max(timestampsLastTmp[0],
								timestampsLastTmp[1]);
					} else if (absoluteValues.size() == 1) {
						// 当timestamps的size为2，需要与当前时间作为参照
						long dateOne = timestampsLastTmp[0];
						long dateTwo = timestampsLastTmp[1];
						if ((Math.abs(dateOne - dateTwo)) < 100000000000L) {
							timestamp = Math.max(timestampsLastTmp[0],
									timestampsLastTmp[1]);
						} else {
							long now = new Date().getTime();
							if (Math.abs(dateOne - now) <= Math.abs(dateTwo
									- now)) {
								timestamp = dateOne;
							} else {
								timestamp = dateTwo;
							}
						}
					}
				}
			} else {
				timestamp = timestamps.get(0);
			}
		}

		if (timestamp != 0) {
			date = new Date(timestamp);
		}
		return date;
	}

	/**
	 * 判断字符串是否为日期字符串
	 *
	 * @param date 日期字符串
	 * @return true or false
	 */
	public static boolean isDate(String date) {
		boolean isDate = false;
		if (date != null) {
			if (StringToDate(date) != null) {
				isDate = true;
			}
		}
		return isDate;
	}

	/**
	 * 获取日期字符串的日期风格。失敗返回null。
	 *
	 * @param date 日期字符串
	 * @return 日期风格
	 */
	public static DateStyle getDateStyle(String date) {
		DateStyle dateStyle = null;
		Map<Long, DateStyle> map = new HashMap<Long, DateStyle>();
		List<Long> timestamps = new ArrayList<Long>();
		for (DateStyle style : DateStyle.values()) {
			Date dateTmp = StringToDate(date, style.getValue());
			if (dateTmp != null) {
				timestamps.add(dateTmp.getTime());
				map.put(dateTmp.getTime(), style);
			}
		}
		dateStyle = map.get(getAccurateDate(timestamps).getTime());
		return dateStyle;
	}

	/**
	 * 将日期字符串转化为日期。失败返回null。
	 *
	 * @param date 日期字符串
	 * @return 日期
	 */
	public static Date StringToDate(String date) {
		DateStyle dateStyle = null;
		return StringToDate(date, dateStyle);
	}

	/**
	 * 将日期字符串转化为日期。失败返回null。
	 *
	 * @param date     日期字符串
	 * @param parttern 日期格式
	 * @return 日期
	 */
	public static Date StringToDate(String date, String parttern) {
		Date myDate = null;
		if (date != null) {
			try {
				myDate = getDateFormat(parttern).parse(date);
			} catch (Exception e) {
				return myDate;
			}
		}
		return myDate;
	}

	/**
	 * 将日期字符串转化为日期。失败返回null。
	 *
	 * @param date      日期字符串
	 * @param dateStyle 日期风格
	 * @return 日期
	 */
	public static Date StringToDate(String date, DateStyle dateStyle) {
		Date myDate = null;
		if (dateStyle == null) {
			List<Long> timestamps = new ArrayList<Long>();
			for (DateStyle style : DateStyle.values()) {
				Date dateTmp = StringToDate(date, style.getValue());
				if (dateTmp != null) {
					timestamps.add(dateTmp.getTime());
				}
			}
			myDate = getAccurateDate(timestamps);
		} else {
			myDate = StringToDate(date, dateStyle.getValue());
		}
		return myDate;
	}

	/**
	 * 将日期转化为日期字符串。失败返回null。
	 *
	 * @param date     日期
	 * @param parttern 日期格式
	 * @return 日期字符串
	 */
	public static String DateToString(Date date, String parttern) {
		String dateString = null;
		if (date != null) {
			try {
				dateString = getDateFormat(parttern).format(date);
			} catch (Exception e) {
			}
		}
		return dateString;
	}

	/**
	 * 将日期转化为日期字符串。失败返回null。
	 *
	 * @param date      日期
	 * @param dateStyle 日期风格
	 * @return 日期字符串
	 */
	public static String DateToString(Date date, DateStyle dateStyle) {
		String dateString = null;
		if (dateStyle != null) {
			dateString = DateToString(date, dateStyle.getValue());
		}
		return dateString;
	}

	/**
	 * 将日期字符串转化为另一日期字符串。失败返回null。
	 *
	 * @param date     旧日期字符串
	 * @param parttern 新日期格式
	 * @return 新日期字符串
	 */
	public static String StringToString(String date, String parttern) {
		return StringToString(date, null, parttern);
	}

	/**
	 * 将日期字符串转化为另一日期字符串。失败返回null。
	 *
	 * @param date      旧日期字符串
	 * @param dateStyle 新日期风格
	 * @return 新日期字符串
	 */
	public static String StringToString(String date, DateStyle dateStyle) {
		return StringToString(date, null, dateStyle);
	}

	/**
	 * 将日期字符串转化为另一日期字符串。失败返回null。
	 *
	 * @param date         旧日期字符串
	 * @param olddParttern 旧日期格式
	 * @param newParttern  新日期格式
	 * @return 新日期字符串
	 */
	public static String StringToString(String date, String olddParttern,
	                                    String newParttern) {
		String dateString = null;
		if (olddParttern == null) {
			DateStyle style = getDateStyle(date);
			if (style != null) {
				Date myDate = StringToDate(date, style.getValue());
				dateString = DateToString(myDate, newParttern);
			}
		} else {
			Date myDate = StringToDate(date, olddParttern);
			dateString = DateToString(myDate, newParttern);
		}
		return dateString;
	}

	/**
	 * 将日期字符串转化为另一日期字符串。失败返回null。
	 *
	 * @param date         旧日期字符串
	 * @param olddDteStyle 旧日期风格
	 * @param newDateStyle 新日期风格
	 * @return 新日期字符串
	 */
	public static String StringToString(String date, DateStyle olddDteStyle,
	                                    DateStyle newDateStyle) {
		String dateString = null;
		if (olddDteStyle == null) {
			DateStyle style = getDateStyle(date);
			dateString = StringToString(date, style.getValue(),
					newDateStyle.getValue());
		} else {
			dateString = StringToString(date, olddDteStyle.getValue(),
					newDateStyle.getValue());
		}
		return dateString;
	}

	/**
	 * 增加日期的年份。失败返回null。
	 *
	 * @param date       日期
	 * @param yearAmount 增加数量。可为负数
	 * @return 增加年份后的日期字符串
	 */
	public static String addYear(String date, int yearAmount) {
		return addInteger(date, Calendar.YEAR, yearAmount);
	}

	/**
	 * 增加日期的年份。失败返回null。
	 *
	 * @param date       日期
	 * @param yearAmount 增加数量。可为负数
	 * @return 增加年份后的日期
	 */
	public static Date addYear(Date date, int yearAmount) {
		return addInteger(date, Calendar.YEAR, yearAmount);
	}

	/**
	 * 增加日期的月份。失败返回null。
	 *
	 * @param date       日期
	 * @param yearAmount 增加数量。可为负数
	 * @return 增加月份后的日期字符串
	 */
	public static String addMonth(String date, int yearAmount) {
		return addInteger(date, Calendar.MONTH, yearAmount);
	}

	/**
	 * 增加日期的月份。失败返回null。
	 *
	 * @param date       日期
	 * @param yearAmount 增加数量。可为负数
	 * @return 增加月份后的日期
	 */
	public static Date addMonth(Date date, int yearAmount) {
		return addInteger(date, Calendar.MONTH, yearAmount);
	}

	/**
	 * 增加日期的天数。失败返回null。
	 *
	 * @param date      日期字符串
	 * @param dayAmount 增加数量。可为负数
	 * @return 增加天数后的日期字符串
	 */
	public static String addDay(String date, int dayAmount) {
		return addInteger(date, Calendar.DATE, dayAmount);
	}

	/**
	 * 增加日期的天数。失败返回null。
	 *
	 * @param date      日期
	 * @param dayAmount 增加数量。可为负数
	 * @return 增加天数后的日期
	 */
	public static Date addDay(Date date, int dayAmount) {
		return addInteger(date, Calendar.DATE, dayAmount);
	}

	/**
	 * 增加日期的周期。失败返回null。
	 *
	 * @param date       日期字符串
	 * @param hourAmount 增加数量。可为负数
	 * @return 增加小时后的日期字符串
	 */
	public static String addWeek(String date, int hourAmount) {
		return addInteger(date, Calendar.DAY_OF_WEEK, hourAmount);
	}

	public static Date addWeek(Date date, int dayAmount) {
		return addInteger(date, Calendar.DAY_OF_WEEK, dayAmount);
	}

	/**
	 * 增加日期的小时。失败返回null。
	 *
	 * @param date       日期字符串
	 * @param hourAmount 增加数量。可为负数
	 * @return 增加小时后的日期字符串
	 */
	public static String addHour(String date, int hourAmount) {
		return addInteger(date, Calendar.HOUR_OF_DAY, hourAmount);
	}

	/**
	 * 增加日期的小时。失败返回null。
	 *
	 * @param date       日期
	 * @param hourAmount 增加数量。可为负数
	 * @return 增加小时后的日期
	 */
	public static Date addHour(Date date, int hourAmount) {
		return addInteger(date, Calendar.HOUR_OF_DAY, hourAmount);
	}

	/**
	 * 增加日期的分钟。失败返回null。
	 *
	 * @param date       日期字符串
	 * @param hourAmount 增加数量。可为负数
	 * @return 增加分钟后的日期字符串
	 */
	public static String addMinute(String date, int hourAmount) {
		return addInteger(date, Calendar.MINUTE, hourAmount);
	}

	/**
	 * 增加日期的分钟。失败返回null。
	 *
	 * @param date       日期
	 * @param hourAmount 增加数量。可为负数
	 * @return 增加分钟后的日期
	 */
	public static Date addMinute(Date date, int hourAmount) {
		return addInteger(date, Calendar.MINUTE, hourAmount);
	}

	/**
	 * 增加日期的秒钟。失败返回null。
	 *
	 * @param date       日期字符串
	 * @param hourAmount 增加数量。可为负数
	 * @return 增加秒钟后的日期字符串
	 */
	public static String addSecond(String date, int hourAmount) {
		return addInteger(date, Calendar.SECOND, hourAmount);
	}

	/**
	 * 增加日期的秒钟。失败返回null。
	 *
	 * @param date       日期
	 * @param hourAmount 增加数量。可为负数
	 * @return 增加秒钟后的日期
	 */
	public static Date addSecond(Date date, int hourAmount) {
		return addInteger(date, Calendar.SECOND, hourAmount);
	}

	/**
	 * 获取日期的年份。失败返回0。
	 *
	 * @param date 日期字符串
	 * @return 年份
	 */
	public static int getYear(String date) {
		return getYear(StringToDate(date));
	}

	/**
	 * 获取日期的年份。失败返回0。
	 *
	 * @param date 日期
	 * @return 年份
	 */
	public static int getYear(Date date) {
		return getInteger(date, Calendar.YEAR);
	}

	/**
	 * 获取日期的月份。失败返回0。
	 *
	 * @param date 日期字符串
	 * @return 月份
	 */
	public static int getMonth(String date) {
		return getMonth(StringToDate(date));
	}

	/**
	 * 获取日期的月份。失败返回0。
	 *
	 * @param date 日期
	 * @return 月份
	 */
	public static int getMonth(Date date) {
		return getInteger(date, Calendar.MONTH);
	}

	/**
	 * 获取日期的天数。失败返回0。
	 *
	 * @param date 日期字符串
	 * @return 天
	 */
	public static int getDay(String date) {
		return getDay(StringToDate(date));
	}

	/**
	 * 获取日期的天数。失败返回0。
	 *
	 * @param date 日期
	 * @return 天
	 */
	public static int getDay(Date date) {
		return getInteger(date, Calendar.DATE);
	}

	/**
	 * 获取日期的小时。失败返回0。
	 *
	 * @param date 日期字符串
	 * @return 小时
	 */
	public static int getHour(String date) {
		return getHour(StringToDate(date));
	}

	/**
	 * 获取日期的小时。失败返回0。
	 *
	 * @param date 日期
	 * @return 小时
	 */
	public static int getHour(Date date) {
		return getInteger(date, Calendar.HOUR_OF_DAY);
	}

	/**
	 * 获取日期的分钟。失败返回0。
	 *
	 * @param date 日期字符串
	 * @return 分钟
	 */
	public static int getMinute(String date) {
		return getMinute(StringToDate(date));
	}

	/**
	 * 获取日期的分钟。失败返回0。
	 *
	 * @param date 日期
	 * @return 分钟
	 */
	public static int getMinute(Date date) {
		return getInteger(date, Calendar.MINUTE);
	}

	/**
	 * 获取日期的秒钟。失败返回0。
	 *
	 * @param date 日期字符串
	 * @return 秒钟
	 */
	public static int getSecond(String date) {
		return getSecond(StringToDate(date));
	}

	/**
	 * 获取日期的秒钟。失败返回0。
	 *
	 * @param date 日期
	 * @return 秒钟
	 */
	public static int getSecond(Date date) {
		return getInteger(date, Calendar.SECOND);
	}

	/**
	 * 获取日期 。默认yyyy-MM-dd格式。失败返回null。
	 *
	 * @param date 日期字符串
	 * @return 日期
	 */
	public static String getDate(String date) {
		return StringToString(date, DateStyle.YYYY_MM_DD);
	}

	/**
	 * 获取日期。默认yyyy-MM-dd格式。失败返回null。
	 *
	 * @param date 日期
	 * @return 日期
	 */
	public static String getDate(Date date) {
		return DateToString(date, DateStyle.YYYY_MM_DD);
	}

	/**
	 * 获取日期的时间。默认HH:mm:ss格式。失败返回null。
	 *
	 * @param date 日期字符串
	 * @return 时间
	 */
	public static String getTime(String date) {
		return StringToString(date, DateStyle.HH_MM_SS);
	}

	/**
	 * 获取日期的时间。默认HH:mm:ss格式。失败返回null。
	 *
	 * @param date 日期
	 * @return 时间
	 */
	public static String getTime(Date date) {
		return DateToString(date, DateStyle.HH_MM_SS);
	}

	/**
	 * 获取日期的星期。失败返回null。
	 *
	 * @param date 日期字符串
	 * @return 星期
	 */
	public static Week getWeek(String date) {
		Week week = null;
		DateStyle dateStyle = getDateStyle(date);
		if (dateStyle != null) {
			Date myDate = StringToDate(date, dateStyle);
			week = getWeek(myDate);
		}
		return week;
	}

	/**
	 * 获取日期的星期。失败返回null。
	 *
	 * @param date 日期
	 * @return 星期
	 */
	public static Week getWeek(Date date) {
		Week week = null;
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		int weekNumber = calendar.get(Calendar.DAY_OF_WEEK) - 1;
		switch (weekNumber) {
			case 0:
				week = Week.SUNDAY;
				break;
			case 1:
				week = Week.MONDAY;
				break;
			case 2:
				week = Week.TUESDAY;
				break;
			case 3:
				week = Week.WEDNESDAY;
				break;
			case 4:
				week = Week.THURSDAY;
				break;
			case 5:
				week = Week.FRIDAY;
				break;
			case 6:
				week = Week.SATURDAY;
				break;
		}
		return week;
	}

	/**
	 * 获取两个日期相差的天数
	 *
	 * @param date      日期字符串
	 * @param otherDate 另一个日期字符串
	 * @return 相差天数
	 */
	public static int getIntervalDays(String date, String otherDate) {
		return getIntervalDays(StringToDate(date), StringToDate(otherDate));
	}

	/**
	 * 获取两个日期相差的天数
	 *
	 * @param date      日期
	 * @param otherDate 另一个日期
	 * @return 相差天数
	 */
	public static int getIntervalDays(Date date, Date otherDate) {
		date = DateUtil.StringToDate(DateUtil.getDate(date));
		long time = Math.abs(date.getTime() - otherDate.getTime());
		return (int) time / (24 * 60 * 60 * 1000);
	}

	/**
	 * 获取两个日期相差的分钟数
	 *
	 * @param date      日期
	 * @param otherDate 另一个日期
	 * @return 相差天数
	 */
	public static int getIntervalMinis(Date date, Date otherDate) {
		date = DateUtil.StringToDate(DateUtil.getDate(date));
		long time = Math.abs(date.getTime() - otherDate.getTime());
		return (int) time / (24 * 60 * 60);
	}

	/**
	 * 得到所传日期的当前月的第一天的字符串
	 *
	 * @param now
	 * @return
	 */
	public static String getThisMonthBeginString(Date now) {
		return getDateFormat(DateStyle.YYYY_MM_DD.getValue()).format(
				getMonthBeginDate(now));
	}

	public static Date getDateStart(Date date) {
		if (null == date) {
			return null;
		}
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		calendar.set(Calendar.HOUR_OF_DAY, 0);
		calendar.set(Calendar.MINUTE, 0);
		calendar.set(Calendar.SECOND, 0);
		return calendar.getTime();
	}

	public static Date getDateEnd(Date date) {
		if (null == date) {
			return null;
		}
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		calendar.set(Calendar.HOUR_OF_DAY, 23);
		calendar.set(Calendar.MINUTE, 59);
		calendar.set(Calendar.SECOND, 59);
		return calendar.getTime();
	}

	/**
	 * 得到所传日期的当前月的最后一天的字符串
	 *
	 * @param now
	 * @return
	 */
	public static String getThisMonthEndString(Date now) {
		return getDateFormat(DateStyle.YYYY_MM_DD.getValue()).format(
				getMonthEndDate(now));
	}

	/**
	 * 得到所传日期的当前月的第一天的日期
	 *
	 * @param now
	 * @return
	 */
	public static Date getMonthBeginDate(Date now) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(now);
		cal.set(Calendar.DAY_OF_MONTH, 1);
		return cal.getTime();
	}

	/**
	 * 得到所传日期的当前月的最后一天的日期
	 *
	 * @param now
	 * @return
	 */
	public static Date getMonthEndDate(Date now) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(now);
		cal.set(Calendar.DAY_OF_MONTH, cal.getMaximum(Calendar.DAY_OF_MONTH));

		return cal.getTime();
	}

	/**
	 * 得到所传日期的当前周的第一天的字符串
	 *
	 * @param now
	 * @return
	 */
	public static String getWeekBeginString(Date now) {
		return getDateFormat(DateStyle.YYYY_MM_DD.getValue()).format(
				getWeekBeginDate(now));
	}

	/**
	 * 得到所传日期的当前周的第一天的日期
	 *
	 * @param now
	 * @return
	 */
	public static Date getWeekBeginDate(Date now) {
		Calendar cal = Calendar.getInstance();
		int dayofweek = cal.get(Calendar.DAY_OF_WEEK) - 1;
		if (dayofweek == 0) {
			dayofweek = 7;
		}
		cal.add(Calendar.DATE, -dayofweek + 1);
		return cal.getTime();
	}

	/**
	 * 格林威治时间对应的秒数
	 *
	 * @param now
	 * @return
	 */
	public static int getGSMTime(Date now) {
		long gsmTime = -28800000;// 1970-01-01 对应的毫秒数
		long nowTime = now.getTime();// 现在对应的毫秒数
		return Integer.valueOf((nowTime - gsmTime) / 1000 + "");

	}

	/**
	 * 格林威治秒数对应的日期
	 *
	 * @param gsm
	 * @return
	 */
	public static Date getGSMDate(int gsm) {
		long gsmTime = -28800000;// 1970-01-01 对应的毫秒数
		long time = gsm * 1000l + gsmTime;
		return new Date(time);

	}

	public static final int toUnix(Date date) {
		return (int) Math.floor(date.getTime() / 1000L);
	}

	/**
	 * @param lo        毫秒数
	 * @param dateStyle 日期格式
	 * @Description: long类型转换成日期
	 */
	public static String longToDate(long lo, DateStyle dateStyle) {
		String dateString = null;
		if (dateStyle != null) {
			Date date = new Date(lo);
			dateString = DateToString(date, dateStyle.getValue());
		}
		return dateString;
	}

	/**
	 * @param lo        毫秒数
	 * @param dateStyle 日期格式
	 * @Description: long类型转换成日期
	 */
	public static String longToDate(String lo, DateStyle dateStyle) {
		String dateString = null;
		Long DateMinus = Long.parseLong(lo);
		if (dateStyle != null) {
			Date date = new Date(DateMinus);
			dateString = DateToString(date, dateStyle.getValue());
		}
		return dateString;
	}

	/**
	 * 获得对比创建的时间和当前的时间---实现几分钟前，几小时前，几天前
	 *
	 * @param createAt 创建的时间
	 * @return
	 */
	public static String getInterval(Date createAt) {
		// 定义最终返回的结果字符串。
		String interval = null;

		long millisecond = new Date().getTime() - createAt.getTime();

		long second = millisecond / 1000;

		if (second <= 0) {
			second = 0;
		}
		// *--------------微博体（标准）
		if (second == 0) {
			interval = "刚刚";
		} else if (second < 30) {
			interval = second + "秒以前";
		} else if (second >= 30 && second < 60) {
			interval = "半分钟前";
		} else if (second >= 60 && second < 60 * 60) {// 大于1分钟 小于1小时
			long minute = second / 60;
			interval = minute + "分钟前";
		} else if (second >= 60 * 60 && second < 60 * 60 * 24) {// 大于1小时 小于24小时
			long hour = (second / 60) / 60;
			if (hour <= 24) {
				interval = hour + "小时前";
			} else {
				interval = "今天"
						+ getFormatTime(createAt, DateStyle.HH_MM.getValue());
			}
		} else if (second >= 60 * 60 * 24 && second <= 60 * 60 * 24 * 2) {// 大于1D
			// 小于2D
			interval = "昨天"
					+ getFormatTime(createAt, DateStyle.HH_MM.getValue());
		} else if (second >= 60 * 60 * 24 * 2 && second <= 60 * 60 * 24 * 7) {// 大于2D小时
			// 小于
			// 7天
			long day = ((second / 60) / 60) / 24;
			interval = day + "天前";
		} else if (second <= 60 * 60 * 24 * 365 && second >= 60 * 60 * 24 * 7) {// 大于7天小于365天
			interval = getFormatTime(createAt, DateStyle.MM_DD_HH_MM.getValue());
		} else if (second >= 60 * 60 * 24 * 365) {// 大于365天
			interval = getFormatTime(createAt,
					DateStyle.YYYY_MM_DD_HH_MM.getValue());
		} else {
			interval = "0";
		}
		return interval;
	}

	/**
	 * 获取促销商品活动的结束时间的字符串
	 *
	 * @param activityEndDateTime
	 * @return
	 * @throws Exception
	 */
	public static String getActivityEndDateTimeString(String activityEndDateTime) {

		long diff;
		long day = 0;
		long hour = 0;
		long min = 0;
		long sec = 0;
		// 获得两个时间的毫秒时间差异

		try {
			diff = StringToDate(activityEndDateTime).getTime()
					- (new Date()).getTime();
			if (diff < 0) {
				return null;
			}
		} catch (Exception e) {
			return null;
		}
		day = diff / MILLI_SECOND_PER_DAY;// 计算差多少天
		hour = diff % MILLI_SECOND_PER_DAY / MILLI_SECOND_PER_HOUR + day * 24;// 计算差多少小时
		min = diff % MILLI_SECOND_PER_DAY % MILLI_SECOND_PER_HOUR
				/ MILLI_SECOND_PER_MIN + day * 24 * 60;// 计算差多少分钟
		sec = diff % MILLI_SECOND_PER_DAY % MILLI_SECOND_PER_HOUR
				% MILLI_SECOND_PER_MIN / MIN_NS;// 计算差多少秒

		StringBuilder buff = new StringBuilder();

		if (day > 0) {
			buff.append(day).append("天");
		}

		if ((hour - day * 24) > 0) {
			buff.append(hour - day * 24).append("小时");
		}

		if ((min - day * 24 * 60) > 0) {
			buff.append(min - day * 24 * 60).append("分钟");
		}

		if (sec > 0) {
			buff.append(sec).append("秒");
		}

		// String cha = day + "天" + (hour - day * 24) + "小时" + (min - day * 24 *
		// 60) + "分钟" + sec + "秒";

		return buff.toString();

	}

	/**
	 * 是否闰年
	 *
	 * @param year 年
	 * @return
	 */
	public static boolean isLeapYear(int year) {
		return (year % 4 == 0 && year % 100 != 0) || (year % 400 == 0);
	}

	/**
	 * 根据时间，返回往前或者往后间隔的时间
	 *
	 * @param dateTime
	 * @param count    正数为加，负数为减
	 * @param type     1、天 2、小时 3、分钟 4、秒
	 * @return
	 */
	private static Date plusOrMinusDatetime(Date dateTime, long count, int type) {
		long interval = 1000l;
		if (type == 1) {
			interval = DateUtil.MILLI_SECOND_PER_DAY;
		} else if (type == 2) {
			interval = DateUtil.MILLI_SECOND_PER_HOUR;
		} else if (type == 3) {
			interval = DateUtil.MILLI_SECOND_PER_MIN;
		}
		try {
			// 防止同时修改dateTime的值
			Date temp = new Date(dateTime.getTime());
			temp.setTime(temp.getTime() + count * interval);
			return temp;
		} catch (Exception e) {
			return null;
		}
	}

	/**
	 * 根据时间，返回往前或者往后间隔的时间
	 *
	 * @param dateTime
	 * @param parttern
	 * @param count    正数为加，负数为减
	 * @param type     1、天 2、小时 3、分钟 4、秒
	 * @return
	 */
	public static Date plusOrMinusDatetime(String dateTime, String parttern,
	                                       long count, int type) {
		try {

			return plusOrMinusDatetime(getDateFormat(parttern).parse(dateTime),
					count, type);
		} catch (Exception e) {
			return null;
		}
	}

	public static int getQuarter(Date currentDate) {
		int quarter = 0;
		int month = getMonth(currentDate) + 1;
		if (month == 1 || month == 2 || month == 3) {
			quarter = 1;
		} else if (month == 4 || month == 5 || month == 6) {
			quarter = 2;
		} else if (month == 7 || month == 8 || month == 9) {
			quarter = 3;
		} else if (month == 10 || month == 11 || month == 12) {
			quarter = 4;
		}
		return quarter;
	}

	public static SimpleDateFormat sdf = new SimpleDateFormat(DateStyle.YYYY_MM_DD.value);

	/**
	 * 获取指定时间
	 *
	 * @param day
	 * @return
	 */
	public static String getAppointDateStr(Date date, int day) {
		Calendar instance = Calendar.getInstance();
		if (null != date) {
			instance.setTime(date);
		}
		instance.add(Calendar.DATE, day);
		return sdf.format(instance.getTime());
	}

	public static SimpleDateFormat sdfymdhds = new SimpleDateFormat(DateStyle.YYYY_MM_DD_HH_MM_SS.value);

	/**
	 * 获取指定时间
	 *
	 * @param day
	 * @return
	 */
	public static String getAppointDateStrHMS(Date date, int day) {
		Calendar instance = Calendar.getInstance();
		if (null != date) {
			instance.setTime(date);
		}
		instance.add(Calendar.DATE, day);
		return sdfymdhds.format(instance.getTime());
	}

	/**
	 * 根据当前日期获得所在周的日期区间（周日和周六日期） 美国
	 *
	 * @return
	 * @throws ParseException
	 * @author zhaoxuepu
	 */
	public static WeekOfDay getTimeInterval(Date date, DateStyle dateStyle) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		// 判断要计算的日期是否是周日，如果是则减一天计算周六的，否则会出问题，计算到下一周去了
		int dayWeek = cal.get(Calendar.DAY_OF_WEEK);// 获得当前日期是一个星期的第几天
		if (1 == dayWeek) {
			cal.add(Calendar.DAY_OF_MONTH, -1);
		}
		// System.out.println("要计算日期为:" + sdf.format(cal.getTime())); // 输出要计算日期
		// 设置一个星期的第一天，按中国的习惯一个星期的第一天是星期一
		//根据提出来的bug，要求第一天为星期天
		cal.setFirstDayOfWeek(Calendar.SUNDAY);
		// 获得当前日期是一个星期的第几天
		int day = cal.get(Calendar.DAY_OF_WEEK);
		// 根据日历的规则，给当前日期减去星期几与一个星期第一天的差值
		cal.add(Calendar.DATE, cal.getFirstDayOfWeek() - day);
		String imptimeBegin = null;
		// System.out.println("所在周星期一的日期：" + imptimeBegin);
		Date imtimeBeginDate = cal.getTime();
		imptimeBegin = DateToString(imtimeBeginDate, dateStyle);
		cal.add(Calendar.DATE, 6);
		String imptimeEnd = null;
		Date imtimeEndDate = cal.getTime();
		imptimeEnd = DateToString(imtimeEndDate, dateStyle);
		// System.out.println("所在周星期日的日期：" + imptimeEnd);
		WeekOfDay weekOfDay = new WeekOfDay(imtimeBeginDate, imtimeEndDate, imptimeBegin + "-" + imptimeEnd);
		return weekOfDay;
	}

	/**
	 * 根据当前日期获得所在周的日期区间（周一和周日日期） 中国
	 *
	 * @return
	 * @throws ParseException
	 * @author zhaoxuepu
	 */
	public static WeekOfDay getChineseTimeInterval(Date date, DateStyle dateStyle) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		// 判断要计算的日期是否是周日，如果是则减一天计算周六的，否则会出问题，计算到下一周去了
		int dayWeek = cal.get(Calendar.DAY_OF_WEEK);// 获得当前日期是一个星期的第几天
		if (1 == dayWeek) {
			cal.add(Calendar.DAY_OF_MONTH, -1);
		}
		// System.out.println("要计算日期为:" + sdf.format(cal.getTime())); // 输出要计算日期
		// 设置一个星期的第一天，按中国的习惯一个星期的第一天是星期一
		//根据提出来的bug，要求第一天为星期天
		cal.setFirstDayOfWeek(Calendar.MONDAY);
		// 获得当前日期是一个星期的第几天
		int day = cal.get(Calendar.DAY_OF_WEEK);
		// 根据日历的规则，给当前日期减去星期几与一个星期第一天的差值
		cal.add(Calendar.DATE, cal.getFirstDayOfWeek() - day);
		String imptimeBegin = null;
		// System.out.println("所在周星期一的日期：" + imptimeBegin);
		Date imtimeBeginDate = cal.getTime();
		imptimeBegin = DateToString(imtimeBeginDate, dateStyle);
		cal.add(Calendar.DATE, 6);
		String imptimeEnd = null;
		Date imtimeEndDate = cal.getTime();
		imptimeEnd = DateToString(imtimeEndDate, dateStyle);
		// System.out.println("所在周星期日的日期：" + imptimeEnd);
		WeekOfDay weekOfDay = new WeekOfDay(imtimeBeginDate, imtimeEndDate, imptimeBegin + "-" + imptimeEnd);
		return weekOfDay;
	}


	/**
	 * 根据当前日期获得上周的日期区间（上周周一和周日日期）
	 * 根据提出的bug获得上周周日到周六的日期
	 *
	 * @return
	 * @author zhaoxuepu
	 */
	public static String getLastTimeInterval() {
		Calendar calendar1 = Calendar.getInstance();
		Calendar calendar2 = Calendar.getInstance();
		int dayOfWeek = calendar1.get(Calendar.DAY_OF_WEEK) - 1;
		int offset1 = 1 - dayOfWeek;
		int offset2 = 7 - dayOfWeek;
		calendar1.add(Calendar.DATE, offset1 - 7);
		calendar2.add(Calendar.DATE, offset2 - 7);
		// System.out.println(sdf.format(calendar1.getTime()));// last Monday
		String lastBeginDate = sdf.format(calendar1.getTime());
		// System.out.println(sdf.format(calendar2.getTime()));// last Sunday
		String lastEndDate = sdf.format(calendar2.getTime());
		return lastBeginDate + "," + lastEndDate;
	}

	public static String format(Date date) {
		long delta = new Date().getTime() - date.getTime();
		if (delta < 1L * ONE_MINUTE) {
			long seconds = toSeconds(delta);
			return (seconds <= 0 ? 1 : seconds) + ONE_SECOND_AGO;
		}
		if (delta < 45L * ONE_MINUTE) {
			long minutes = toMinutes(delta);
			return (minutes <= 0 ? 1 : minutes) + ONE_MINUTE_AGO;
		}
		if (delta < 24L * ONE_HOUR) {
			long hours = toHours(delta);
			return (hours <= 0 ? 1 : hours) + ONE_HOUR_AGO;
		}
		if (delta < 48L * ONE_HOUR) {
			return "昨天" + getFormatTime(date, DateStyle.HH_MM.getValue());
		}
		if (delta < 30L * ONE_DAY) {
			long days = toDays(delta);
			return (days <= 0 ? 1 : days) + ONE_DAY_AGO;
		}
		if (delta < 12L * 4L * ONE_WEEK) {
			long months = toMonths(delta);
			return (months <= 0 ? 1 : months) + ONE_MONTH_AGO;
		} else {
			long years = toYears(delta);
			return (years <= 0 ? 1 : years) + ONE_YEAR_AGO;
		}
	}

	private static long toSeconds(long date) {
		return date / 1000L;
	}

	private static long toMinutes(long date) {
		return toSeconds(date) / 60L;
	}

	private static long toHours(long date) {
		return toMinutes(date) / 60L;
	}

	private static long toDays(long date) {
		return toHours(date) / 24L;
	}

	private static long toMonths(long date) {
		return toDays(date) / 30L;
	}

	private static long toYears(long date) {
		return toMonths(date) / 365L;
	}


	public static void main(String[] args) {
		System.out.println(getActivityEndDateTimeString("2016-03-28"));
		try {
			System.out.println(getInterval(getDateFormat(
					DateStyle.YYYY_MM_DD_HH_MM_SS.getValue()).parse(
					"2016-02-09 21:20:45")));
		} catch (ParseException e) {
			e.printStackTrace();
		} catch (RuntimeException e) {
			e.printStackTrace();
		}

		System.out.println(DateToString(new Date(),
				DateStyle.YYYY_MM_DD.getValue()));

		System.out.println(getDateStyle("2015-02-01 21:22:25"));

		System.out.println(getFormatTime(new Date(),
				DateStyle.YYYY_MM_DD_HH_MM_SS_CN.getValue()));

		System.out.println(getWeek(new Date()).number);

		System.out.println(getIntervalDays("2012-11-10", "2012-11-1"));


		System.out.println(DateToString(
				plusOrMinusDatetime("2015-02-01 21:22:25",
						DateStyle.YYYY_MM_DD_HH_MM_SS.getValue(), -5, 1),
				DateStyle.YYYY_MM_DD.getValue()));

		System.out.println("---------------------------------");

		int type = 2;
		Date currentDate = new Date();
		switch (type) {
			case 1:
				for (int i = 1; i <= 15; i++) {
					int dayAmount = -i;
					String intervalTime = DateUtil.DateToString(DateUtil.addDay(new Date(), dayAmount), DateUtil.DateStyle.YYYY_MM_DD_CN);
					System.out.println(intervalTime);
				}
				break;
			case 2:

				for (int i = 1; i <= 5; i++) {
					int dayAmount = -i;
					WeekOfDay intervalTime = DateUtil.getTimeInterval(currentDate, DateUtil.DateStyle.YYYY_MM_DD_CN);
					currentDate = DateUtil.addDay(intervalTime.firstWeekOfDay, dayAmount);
					System.out.println(intervalTime.intervalTime);
				}
				break;
			case 3:
				for (int i = 1; i <= 6; i++) {
					int dayAmount = -i;
					String intervalTime = DateUtil.DateToString(DateUtil.addMonth(new Date(), dayAmount), DateUtil.DateStyle.YYYY_MM_CN);
					System.out.println(intervalTime);
				}
				break;
			case 4:
				for (int i = 1; i <= 3; i++) {
					int dayAmount = -i;
					String intervalTime = DateUtil.DateToString(DateUtil.addYear(new Date(), dayAmount), DateStyle.YYYY_CN);
					System.out.println(intervalTime);
				}
				break;
			default:
				break;
		}


		String currentDateStr = "2017-06-04 06:41:16";
		MyLogsUtils.printLogs(getInterval(StringToDate(currentDateStr)));
		MyLogsUtils.printLogs(format(StringToDate(currentDateStr)));

	}

	public static class WeekOfDay {

		public Date firstWeekOfDay;

		public Date endWeekOfDay;

		public String intervalTime;

		public WeekOfDay(Date firstWeekOfDay, Date endWeekOfDay, String intervalTime) {
			this.firstWeekOfDay = firstWeekOfDay;
			this.endWeekOfDay = endWeekOfDay;
			this.intervalTime = intervalTime;
		}

	}
}