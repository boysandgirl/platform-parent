package com.open.comm.utils;

/**
 * Created by xuwushun on 2017/3/6.
 */
public enum PermissionEnum {
	MERCHANT_LOGIN("merchant.login", "商户管理平台登录"), CERTIFICATE("certificate", "资质认证"), STORE("store", "门店管理")
	, USER("user", "人员管理"), MERCHANT_DATA("merchant.data", "商户资料"), ORDER_STATISTIC("order.statistic", "交易统计")
	, ORDER_FLOW("order.flow", "交易流水"), ROLE("role", "角色管理"), MERCHANT_CHANNEL("merchant.channel", "渠道信息");
	private String code;
	private String name;

	PermissionEnum(String code, String name) {
		this.code = code;
		this.name = name;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
}
