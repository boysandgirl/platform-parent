/**
 * @(#)ParameterException.java 2011-12-20 Copyright 2011 it.kedacom.com, Inc.
 *                             All rights reserved.
 */

package com.open.comm.utils.exception;
/**
 * 
 * 描述：redis服务器异常
 * 作者： 徐福周
 * 时间：2016年6月7日
 * 版本号：1.0
 */
public class RedisServiceException extends RuntimeException {

	/** serialVersionUID */
	private static final long serialVersionUID = 6417641452178955756L;
	
	public static  final   String messageException="redis service already closed";

	public RedisServiceException() {
		super();
	}

	public RedisServiceException(String message) {
		super(message);
	}

	public RedisServiceException(Throwable cause) {
		super(cause);
	}

	public RedisServiceException(String message, Throwable cause) {
		super(message, cause);
	}
	
	public String getMessageException() {
		return messageException;
	}
}
