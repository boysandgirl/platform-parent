package com.open.comm.utils.exception;

/**
 * 
 * 描述：未授权异常
 * 作者： 徐福周
 * 时间：2016年6月7日
 * 版本号：1.0
 */
public class UnPermissionException extends RuntimeException{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public UnPermissionException() {
		super();
		// TODO Auto-generated constructor stub
	}

	public UnPermissionException(String message, Throwable cause,
			boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
		// TODO Auto-generated constructor stub
	}

	public UnPermissionException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	public UnPermissionException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	public UnPermissionException(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}
	
	/** 
     *  
     * @param userName 用户名
     * @param pri 权限
     */  
    public UnPermissionException(String userName,String pri){  
        super("User:"+userName+"don't have the Permission to Operate:"+pri);  
    }  

}
