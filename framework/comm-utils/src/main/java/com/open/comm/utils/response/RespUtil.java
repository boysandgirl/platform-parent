package com.open.comm.utils.response;

import org.apache.log4j.Logger;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import java.io.ByteArrayOutputStream;
import java.io.OutputStream;
import java.util.zip.GZIPOutputStream;

/**
 * 描述：数据压缩工具类
 * 作者：xfz
 * 时间：2016年2月19日  下午4:08:55
 */
public class RespUtil {
	protected static Logger logger = Logger.getLogger(RespUtil.class);

	/**
	 * 描述：返回的时候压缩数据【格式：gzip】
	 *
	 * @param response
	 * @param object
	 */
	public static void RespByGzip(HttpServletResponse response, Object object) {
		try {
			String respData = JacksonUtil.toJSon(object);
			ByteArrayOutputStream baos = new ByteArrayOutputStream();
			// 把数据压缩到缓冲字节流中
			GZIPOutputStream out = new GZIPOutputStream(baos);
			// 取出数据原始字节
			byte[] bs = respData.getBytes("utf-8");
			logger.info("原始数据大小=" + bs.length);
			out.write(bs);
			out.close();
			// 取出压缩后的数据
			bs = baos.toByteArray();
			logger.info("压缩后的数据大小=" + bs.length);
			// 告知客户端压缩格式和文件长度
			response.setHeader("Access-Control-Allow-Origin", "*");
			response.setHeader("Content-Encoding", "gzip");
			response.setContentType("application/json");
			response.setContentLength(bs.length);
			ServletOutputStream so = response.getOutputStream();
			so.write(bs);
		} catch (Exception e) {
			logger.error("返回压缩数据异常");
			//throw new LcException("返回压缩数据异常");
		}
	}

	/**
	 * 描述：返回的文本数据【类型：String】
	 *
	 * @param response
	 * @param object
	 */
	public static void RespByText(HttpServletResponse response, Object object) {
		try {
			String respData = JacksonUtil.toJSon(object);
			response.setHeader("Access-Control-Allow-Origin", "*");
			response.setHeader("Content-type", "application/json;charset=utf-8");
			response.setContentType("application/json;charset=utf-8");
			OutputStream out = response.getOutputStream();
			out.write(respData.getBytes("utf-8"));
			out.close();
		} catch (Exception e) {
			logger.error("返回普通文本数据异常");
			//throw new LcException("返回普通文本数据异常");
		}
	}

	/**
	 * 描述：返回的文本数据【类型：String】
	 *
	 * @param response
	 * @param imgData
	 */
	public static void RespByImg(HttpServletResponse response, byte[] imgData) {
		try {
//			String respData = JacksonUtil.toJSon(object);
			response.setHeader("Access-Control-Allow-Origin", "*");
			response.setHeader("Content-type", "image/*");
			response.setContentType("image/*;charset=utf-8");
			OutputStream out = response.getOutputStream();
			out.write(imgData);
			out.close();
		} catch (Exception e) {
			logger.error("返回图片数据异常");
			//throw new LcException("返回普通文本数据异常");
		}
	}

}
