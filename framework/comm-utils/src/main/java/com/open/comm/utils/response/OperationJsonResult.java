package com.open.comm.utils.response;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import javax.servlet.http.HttpServletResponse;

public class OperationJsonResult {

	/**
	 * 状态码（1000：成功；1001：成功，请求用户确认；1002：成功，给予用户警告；1013：系统异常；1014：token失效；1015：操作错误；1016：参数不正确(错误或者不完整)；1017：未知错误,1018 :无权限）
	 */
	private static final Integer RESP_SUCCESS = 1000;//1000：成功；
	private static final Integer RESP_SUCCESS_CONFIRM = 1001;//1001：成功，
	private static final Integer RESP_SUCCESS_WARNING = 1002;//1002：成功，给予用户警告；
	private static final Integer RESP_FAILURE_SYSEX = 1013;//1013：系统异常；
	private static final Integer RESP_FAILURE_KEYWRONG = 1014;//1014：token失效；
	private static final Integer RESP_FAILURE_OPERATION = 1015;//1015：操作错误；
	private static final Integer RESP_FAILURE_PAMINCOM = 1016;//1016：参数不正确(错误或者不完整)；
	private static final Integer RESP_FAILURE_UNKNOWN = 1017;//1017：未知错误,

	private static final Integer RESP_FAILURE_UNPERMISSION = 1018;//1018 :无权限

	/**
	 * 标识操作结果类型
	 */
	public enum OPERATION_RESULT_TYPE {
		success,

		warning,

		failure,

		confirm
	}

	/**
	 * 返回结果类型
	 */
	@JsonSerialize
	private String result;

	/**
	 * 状态码
	 */
	@JsonSerialize
	private Integer code;

	/**
	 * 消息（当返回的是warning,failure,confirm）
	 */
	@JsonSerialize
	private String message;

	/**
	 * 业务数据（成功类型才有业务数据，注：确认和警告也算成功类型）
	 */
	@JsonSerialize
	private Object data;

	/**
	 * 业务数据的附属数据（如：分页信息总数、第几页等）
	 */
	@JsonSerialize
	private Object subData;


	public Object getSubData() {
		return subData;
	}

	public void setSubData(Object subData) {
		this.subData = subData;
	}

	public String getResult() {
		return result;
	}

	public void setResult(String result) {
		this.result = result;
	}

	public Integer getCode() {
		return code;
	}

	public void setCode(Integer code) {
		this.code = code;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public Object getData() {
		return data;
	}

	public void setData(Object data) {
		this.data = data;
	}


	/**
	 * 成功 【无业务数据，无消息】
	 *
	 * @param response     返回对象
	 * @param compressData 是否压缩数据
	 */
	public static void success(HttpServletResponse response, boolean compressData) {
		respData(response, compressData, new OperationJsonResult(OPERATION_RESULT_TYPE.success, null, null, null, RESP_SUCCESS));
	}

	/**
	 * 成功 【有消息】
	 *
	 * @param response
	 * @param compressData 是否压缩数据
	 * @param message
	 */
	public static void success(HttpServletResponse response, boolean compressData, String message) {
		respData(response, compressData, new OperationJsonResult(OPERATION_RESULT_TYPE.success, message, null, null, RESP_SUCCESS));
	}

	/**
	 * 成功 【有业务数据】
	 *
	 * @param response
	 * @param compressData 是否压缩数据
	 * @param data
	 */
	public static void success(HttpServletResponse response, boolean compressData, Object data) {
		respData(response, compressData, new OperationJsonResult(OPERATION_RESULT_TYPE.success, null, data, null, RESP_SUCCESS));
	}

	/**
	 * 成功 【有业务数据和业务附属数据】
	 *
	 * @param response
	 * @param compressData 是否压缩数据
	 * @param data
	 */
	public static void success(HttpServletResponse response, boolean compressData, Object data, Object subData) {
		respData(response, compressData, new OperationJsonResult(OPERATION_RESULT_TYPE.success, null, data, subData, RESP_SUCCESS));
	}

	/**
	 * 确认【有消息】
	 *
	 * @param response
	 * @param compressData 是否压缩数据
	 * @param message
	 */
	public static void confirm(HttpServletResponse response, boolean compressData, String message) {
		respData(response, compressData, new OperationJsonResult(OPERATION_RESULT_TYPE.confirm, message, null, null, RESP_SUCCESS_CONFIRM));
	}

	/**
	 * 确认【有消息】【有业务数据】
	 *
	 * @param response
	 * @param compressData 是否压缩数据
	 * @param message
	 * @param data
	 */
	public static void confirm(HttpServletResponse response, boolean compressData, String message, Object data) {
		respData(response, compressData, new OperationJsonResult(OPERATION_RESULT_TYPE.confirm, message, data, null, RESP_SUCCESS_CONFIRM));
	}

	/**
	 * 确认【有消息】【有业务数据和业务附属数据】
	 *
	 * @param response
	 * @param compressData 是否压缩数据
	 * @param message
	 * @param data
	 */
	public static void confirm(HttpServletResponse response, boolean compressData, String message, Object data, Object subData) {
		respData(response, compressData, new OperationJsonResult(OPERATION_RESULT_TYPE.confirm, message, data, subData, RESP_SUCCESS_CONFIRM));
	}

	/**
	 * 警告【有消息】
	 *
	 * @param response
	 * @param compressData 是否压缩数据
	 * @param message
	 */
	public static void warning(HttpServletResponse response, boolean compressData, String message) {
		respData(response, compressData, new OperationJsonResult(OPERATION_RESULT_TYPE.warning, message, null, null, RESP_SUCCESS_WARNING));
	}

	/**
	 * 警告【有消息】【有业务数据】
	 *
	 * @param response
	 * @param compressData
	 * @param message
	 * @param data
	 */
	public static void warning(HttpServletResponse response, boolean compressData, String message, Object data) {
		respData(response, compressData, new OperationJsonResult(OPERATION_RESULT_TYPE.warning, message, data, null, RESP_SUCCESS_WARNING));
	}

	/**
	 * 警告【有消息】【有业务数据】
	 *
	 * @param response
	 * @param compressData
	 * @param message
	 * @param data
	 * @param subData
	 */
	public static void warning(HttpServletResponse response, boolean compressData, String message, Object data, Object subData) {
		respData(response, compressData, new OperationJsonResult(OPERATION_RESULT_TYPE.warning, message, data, subData, RESP_SUCCESS_WARNING));
	}

	/**
	 * 失败【操作】【有消息】
	 *
	 * @param response
	 * @param compressData 是否压缩数据
	 * @param message
	 */
	public static void failureOperation(HttpServletResponse response, boolean compressData, String message) {
		respData(response, compressData, new OperationJsonResult(OPERATION_RESULT_TYPE.failure, message, null, null, RESP_FAILURE_OPERATION));
	}

	/**
	 * 失败操作 【有消息】 【有data】数据
	 *
	 * @param response
	 * @param compressData
	 * @param message
	 * @param data
	 */
	public static void failureOperation(HttpServletResponse response, boolean compressData, String message, Object data) {
		respData(response, compressData, new OperationJsonResult(OPERATION_RESULT_TYPE.failure, message, data, null, RESP_FAILURE_OPERATION));
	}

	/**
	 * 失败操作 【有消息】 【有data】数据 【subData】数据
	 *
	 * @param response
	 * @param compressData
	 * @param message
	 * @param data
	 * @param subData
	 */
	public static void failureOperation(HttpServletResponse response, boolean compressData, String message, Object data, Object subData) {
		respData(response, compressData, new OperationJsonResult(OPERATION_RESULT_TYPE.failure, message, data, subData, RESP_FAILURE_OPERATION));
	}

	/**
	 * 授权失败【操作】【有消息】
	 *
	 * @param response
	 * @param compressData 是否压缩数据
	 * @param message
	 */
	public static void failurePermission(HttpServletResponse response, boolean compressData, String message) {
		respData(response, compressData, new OperationJsonResult(OPERATION_RESULT_TYPE.failure, message, null, null, RESP_FAILURE_UNPERMISSION));
	}

	/**
	 * 失败【系统异常】【有消息】
	 *
	 * @param response
	 * @param compressData 是否压缩数据
	 * @param message
	 */
	public static void failure(HttpServletResponse response, boolean compressData, String message) {
		respData(response, compressData, new OperationJsonResult(OPERATION_RESULT_TYPE.failure, message, null, null, RESP_FAILURE_SYSEX));
	}

	/**
	 * 失败【系统异常】【有消息】【有数据data】
	 * @param response
	 * @param compressData
	 * @param message
	 * @param data
	 */
	public static void failure(HttpServletResponse response, boolean compressData, String message, Object data) {
		respData(response, compressData, new OperationJsonResult(OPERATION_RESULT_TYPE.failure, message, data, null, RESP_FAILURE_SYSEX));
	}

	/**
	 * 失败【系统异常】【有消息】【有数据data】【有数据subData】
	 * @param response
	 * @param compressData
	 * @param message
	 * @param data
	 * @param subData
	 */
	public static void failure(HttpServletResponse response, boolean compressData, String message, Object data, Object subData) {
		respData(response, compressData, new OperationJsonResult(OPERATION_RESULT_TYPE.failure, message, data, subData, RESP_FAILURE_SYSEX));
	}

	/**
	 * 失败【key错误】【有消息】【token失效】
	 *
	 * @param response
	 * @param compressData 是否压缩数据
	 * @param message
	 */
	public static void failureKey(HttpServletResponse response, boolean compressData, String message) {
		respData(response, compressData, new OperationJsonResult(OPERATION_RESULT_TYPE.failure, message, null, null, RESP_FAILURE_KEYWRONG));
	}

	/**
	 * 失败【参数不完整】【有消息】
	 *
	 * @param response
	 * @param compressData 是否压缩数据
	 * @param message
	 */
	public static void failurePamincom(HttpServletResponse response, boolean compressData, String message) {
		respData(response, compressData, new OperationJsonResult(OPERATION_RESULT_TYPE.failure, message, null, null, RESP_FAILURE_PAMINCOM));
	}

	/**
	 * 失败【未知错误】【有消息】
	 *
	 * @param response
	 * @param compressData 是否压缩数据
	 * @param message
	 */
	public static void failureUnknown(HttpServletResponse response, boolean compressData, String message) {
		respData(response, compressData, new OperationJsonResult(OPERATION_RESULT_TYPE.failure, message, null, null, RESP_FAILURE_UNKNOWN));
	}

	/**
	 * 描述：判断是否要压缩数据
	 *
	 * @param response
	 * @param compressData 是否压缩数据
	 * @param object
	 */
	private static void respData(HttpServletResponse response, boolean compressData, Object object) {
		if (compressData) {
			RespUtil.RespByGzip(response, object);
		} else {
			RespUtil.RespByText(response, object);
		}
	}


	public OperationJsonResult(OPERATION_RESULT_TYPE result, String message, Object data, Object subData, Integer code) {
		this.code = code;
		this.result = result.name();
		this.message = message;
		this.data = data;
		this.subData = subData;
	}

}