package com.open.comm.utils;

/**
 * @author : 徐福周
 * @Description 自定义logs输出记录, 防止输出地方满天飞
 * @Date : 2017-06-03
 * @Time : 21:43
 * Blogs http://www.cnblogs.com/xfzlovezjj/
 */
public class MyLogsUtils {

	public static void printLogs(String message) {

		System.out.println("xufuzhou_logs_println:\n" + message);
	}
}
