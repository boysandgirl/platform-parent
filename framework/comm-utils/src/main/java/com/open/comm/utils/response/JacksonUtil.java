package com.open.comm.utils.response;

import com.alibaba.fastjson.JSON;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.module.SimpleModule;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;

import java.util.List;

/**
 * The class JacksonUtil
 * <p>
 * json字符与对像转换
 *
 * @version: $Revision$ $Date$ $LastChangedBy$
 */
public final class JacksonUtil {

	public static ObjectMapper objectMapper;


	public static ObjectMapper getInstance() {
		if (objectMapper == null) {
			objectMapper = new ObjectMapper();
			/**
			 * 序列换成json时,将所有的long变成string
			 * 因为js中得数字类型不能包含所有的java long值
			 */
			SimpleModule simpleModule = new SimpleModule();
			simpleModule.addSerializer(Long.class, ToStringSerializer.instance);
			simpleModule.addSerializer(Long.TYPE, ToStringSerializer.instance);
			objectMapper.registerModule(simpleModule);
		}

		return objectMapper;
	}

	/**
	 * 使用泛型方法，把json字符串转换为相应的JavaBean对象。
	 * (1)转换为普通JavaBean：readValue(json,Student.class)
	 * (2)转换为List,如List<Student>,将第二个参数传递为Student
	 * [].class.然后使用Arrays.asList();方法把得到的数组转换为特定类型的List
	 *
	 * @param jsonStr
	 * @param valueType
	 * @return
	 * @throws Exception
	 */
	public static <T> T readValue(String jsonStr, Class<T> valueType) throws Exception {

		if (jsonStr == null) {
			return null;
		}
		if (jsonStr == "") {
			return null;
		}
		if (objectMapper == null) {
			objectMapper = getInstance();

		}

		try {
			return objectMapper.readValue(jsonStr, valueType);
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception(e.getMessage());
		}

	}

	/**
	 * json数组转List
	 *
	 * @param jsonStr
	 * @param valueTypeRef
	 * @return
	 * @throws Exception
	 */
	public static <T> T readValue(String jsonStr, TypeReference<T> valueTypeRef) throws Exception {
		if (jsonStr == null) {
			return null;
		}
		if (objectMapper == null) {
			objectMapper = getInstance();
		}

		try {
			return objectMapper.readValue(jsonStr, valueTypeRef);
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception(e.getMessage());
		}

	}

	public static <T> List<T> readValue(String jsonStr, JavaType valueTypeRef) throws Exception {
		if (jsonStr == null) {
			return null;
		}
		if (objectMapper == null) {
			objectMapper = getInstance();

		}

		try {
			return objectMapper.readValue(jsonStr, valueTypeRef);
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception(e.getMessage());
		}

	}

	/**
	 * 把JavaBean转换为json字符串
	 *
	 * @param object
	 * @return
	 */
	public static String toJSon(Object object) {
		if (objectMapper == null) {
			objectMapper = getInstance();

		}

		try {
			return objectMapper.writeValueAsString(object);
		} catch (Exception e) {
			e.printStackTrace();
		}

		return null;
	}

	/**
	 * 普通对象的序列化
	 *
	 * @param target
	 * @return 2016年6月28日
	 * @author 徐福周
	 */
	public static String serialize(Object target) {
		return JSON.toJSONString(target);
	}

	/**
	 * 普通对象的解析
	 *
	 * @param jsonString
	 * @param clazz
	 * @return 2016年6月28日
	 * @author 徐福周
	 */
	public static Object deserialize(String jsonString, Class clazz) {

		// 序列化结果是普通对象
		return JSON.parseObject(jsonString, clazz);
	}

	/**
	 * 获取泛型的Collection Type
	 *
	 * @param collectionClass 泛型的Collection
	 * @param elementClasses  元素类
	 * @return JavaType Java类型
	 * @since 1.0
	 */
	public static JavaType getCollectionType(Class<?> collectionClass, Class<?>... elementClasses) {
		if (objectMapper == null) {
			objectMapper = getInstance();
		}
		return objectMapper.getTypeFactory().constructParametricType(collectionClass, elementClasses);
	}
}