package com.open.comm.utils.page;

import java.io.Serializable;

public class BaseRequest extends CommonRequest implements Serializable {

	@Override
	public String toString() {
		return "BaseRequest{" +
				"ticket='" + super.getTicket() + '\'' +
				"permission='" + super.getPermission() + '\'' +
				'}';
	}
}
