package com.open.comm.utils.exception;

/**
 * 
 * 描述：token过期异常 作者： 徐福周 时间：2016年6月7日 版本号：1.0
 */
public class TokenDismissException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public TokenDismissException() {
		super();
	}

	public TokenDismissException(String message, Throwable cause,
			boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}

	public TokenDismissException(String message, Throwable cause) {
		super(message, cause);
	}

	/**
	 * 
	 * @param message
	 */
	public TokenDismissException(String message) {
		super(message);

	}

	public TokenDismissException(Throwable cause) {
		super(cause);
	}

}
