package com.open.comm.utils.exception;

/**
 * 
 * 描述：系统业务异常
 * 作者： 徐福周
 * 时间：2016年6月7日
 * 版本号：1.0
 */
public class LcException extends RuntimeException{

	/**
	 * 
	 */
	private static final long serialVersionUID = 4358030676461433335L;

	public LcException() {
		super();
	}


	public LcException(String message, Throwable cause) {
		super(message, cause);
	}

	public LcException(String message) {
		super(message);
	}

	public LcException(Throwable cause) {
		super(cause);
	}
	
	

}
