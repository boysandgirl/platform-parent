package com.open.comm.utils.page;

public class PageResponse<T> {

	private int total=0;

	private Object obj;

	private Object subObj;


	public Object getSubObj() {
		return subObj;
	}

	public void setSubObj(Object subObj) {
		this.subObj = subObj;
	}

	public PageResponse(int total, Object obj,Object subObj){
		this.total=total;
		this.obj=obj;
		this.subObj=subObj;
	}
	public PageResponse(int total, Object obj){
		this.total=total;
		this.obj=obj;
	}

	public PageResponse(){
		this.total=0;

	}

	public int getTotal() {
		return total;
	}

	public void setTotal(int total) {
		this.total = total;
	}

	public Object getObj() {
		return obj;
	}

	public void setObj(Object obj) {
		this.obj = obj;
	}
}
