package com.open.comm.utils.page;

import java.io.Serializable;

public class CommonRequest implements Serializable{

	private String ticket;

	private String permission;


	public String getTicket() {
		return ticket;
	}

	public void setTicket(String ticket) {
		this.ticket = ticket;
	}

	public String getPermission() {
		return permission;
	}

	public void setPermission(String permission) {
		this.permission = permission;
	}
}
