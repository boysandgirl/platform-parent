package com.open.comm.utils.exception;

/**
 * 
 * 描述：警告业务异常
 * 作者： 徐福周
 * 时间：2016年6月7日
 * 版本号：1.0
 */
public class WarningException extends RuntimeException{

	/**
	 * 
	 */
	private static final long serialVersionUID = 4358030676461433335L;

	public WarningException() {
		super();
	}

	public WarningException(String message, Throwable cause,
			boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}

	public WarningException(String message, Throwable cause) {
		super(message, cause);
	}

	public WarningException(String message) {
		super(message);
	}

	public WarningException(Throwable cause) {
		super(cause);
	}
	
	

}
