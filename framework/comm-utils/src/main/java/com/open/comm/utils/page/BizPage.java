package com.open.comm.utils.page;

import java.io.Serializable;
import java.util.List;

/**
 * 业务返回数据
 *
 * @Date 2017-06-23
 * @Time 14:02
 * @Author 徐福周
 */

public class BizPage<T> implements Serializable {

	public int total;

	private int pageNum;

	private int pageSize;

	private List<T> datas;

	public int getTotal() {
		return total;
	}

	public void setTotal(int total) {
		this.total = total;
	}

	public int getPageNum() {
		return pageNum;
	}

	public void setPageNum(int pageNum) {
		this.pageNum = pageNum;
	}

	public int getPageSize() {
		return pageSize;
	}

	public void setPageSize(int pageSize) {
		this.pageSize = pageSize;
	}

	public List<T> getDatas() {
		return datas;
	}

	public void setDatas(List<T> datas) {
		this.datas = datas;
	}
}
