package com.open.comm.utils.page;

import org.springframework.util.StringUtils;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

/**
 * 分页基础请求参数
 *
 * @date: 2017-06-11 11:29
 * @author: xfz
 */

public class BasePageRequest extends BaseRequest implements Serializable {

	/**
	 * 搜索属性
	 */

	private String searchProperty;
	/**
	 * 搜索值
	 */

	private String searchValue;

	/**
	 * 排序
	 */

	private int sort = 0;//排序 1升序 2 降序  0默认


	/**
	 * 排序字段
	 */

	private String sortProperty;


	/**
	 * 用于瀑布流 查询 如传入 上次的总页数
	 */

	private String boundValues;

	/**
	 * 每页大小
	 */

	private int pageSize = 10;
	/**
	 * 当前页码
	 */

	private int pageNum = 10;


	private Integer delStatus = 1;


	private Long userId;

	/**
	 * 获取起始页的位置
	 *
	 * @return
	 */
	public int getPageStartIndex() {
		return (pageNum - 1) * this.pageSize;
	}

	public int getNewPageStartIndex(int thisTotalCount) {
		Integer lastTotalCount = 0;
		if (!StringUtils.isEmpty(boundValues)) {
			try {
				lastTotalCount = Integer.parseInt(boundValues);//获取上次请求的总数量

				if (thisTotalCount >= lastTotalCount) {
					return thisTotalCount - lastTotalCount + getPageSize();//一删 一减少 妈的
				} else {
					return getPageStartIndex();
				}
			} catch (Exception e) {
			}
		}
		return lastTotalCount;
	}

	/**
	 * @param
	 * @description: 自定义 sql 构建基础的分页 参数
	 * @date: 2017/6/18 22:25
	 * @author: xfz
	 */

	public Map<String, Object> getMapParams() {
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("pageNum", getPageStartIndex());
		map.put("pageSize", pageSize);
		return map;
	}

	public String getSearchProperty() {
		return searchProperty;
	}

	public void setSearchProperty(String searchProperty) {
		this.searchProperty = searchProperty;
	}

	public String getSearchValue() {
		return searchValue;
	}

	public void setSearchValue(String searchValue) {
		this.searchValue = searchValue;
	}

	public int getSort() {
		return sort;
	}

	public void setSort(int sort) {
		this.sort = sort;
	}

	public String getBoundValues() {
		return boundValues;
	}

	public void setBoundValues(String boundValues) {
		this.boundValues = boundValues;
	}

	public int getPageSize() {
		return pageSize;
	}

	public void setPageSize(int pageSize) {
		if (pageSize <= 0 || pageSize >= 20) {
			pageSize = 10;
		}
		this.pageSize = pageSize;
	}

	public int getPageNum() {
		return pageNum;
	}

	public void setPageNum(int pageNum) {
		if (pageNum < 1) {
			pageNum = 1;
		}
		this.pageNum = pageNum;
	}

	public String getSortProperty() {
		return sortProperty;
	}

	public void setSortProperty(String sortProperty) {
		this.sortProperty = sortProperty;
	}

	public Integer getDelStatus() {
		return delStatus;
	}

	public void setDelStatus(Integer delStatus) {
		this.delStatus = delStatus;
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}


	public int getOffset() {
		return (this.getPageNum() - 1) * this.getPageSize();
	}
}
