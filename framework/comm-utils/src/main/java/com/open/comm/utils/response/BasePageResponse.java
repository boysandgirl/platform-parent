package com.open.comm.utils.response;

import java.io.Serializable;


public class BasePageResponse implements Serializable {
	//总数量
	private int total = 0;
	//总页数
	private int totalPage;
	//当前页码
	private int currentPage = 1;
	//每页总数
	private int pageSize = 10;

	public BasePageResponse() {
		initPage();
	}

	public BasePageResponse(int total, int currentPage, int pageSize) {
		this.total = total;
		this.currentPage = currentPage;
		this.pageSize = pageSize;
		initPage();
	}

	public void initPage() {
		if (pageSize <= 0) {
			this.totalPage = 0;
		} else {
			this.totalPage = (total + pageSize - 1) / pageSize;
		}
	}

	public int getTotal() {
		return total;
	}

	public void setTotal(int total) {
		this.total = total;
	}

	public int getTotalPage() {
		return totalPage;
	}

	public void setTotalPage(int totalPage) {
		this.totalPage = totalPage;
	}

	public int getCurrentPage() {
		return currentPage;
	}

	public void setCurrentPage(int currentPage) {
		this.currentPage = currentPage;
	}

	public int getPageSize() {
		return pageSize;
	}

	public void setPageSize(int pageSize) {
		this.pageSize = pageSize;
	}
}
