package com.open.comm.enumucation;

/**
 * 角色枚举类
 *
 * @date: 2017-06-22 0:22
 * @author: xfz
 */
public enum RoleEnum {

    //客服主管 客服员  接待员  带看员   超级管理员 管理员 测试人员
    SUERADMIN("超级管理员"),
    ADMIN("管理员"),
    TEST("测试员"),
    CHARGEORCUSTOMERSERVICE("客服主管"),
    CUSTOMERSERVICE("客服员"),
    RECEPTIONIST("接待员"),
    LOOKMENBER("带看员"),
    OTHER("其他");

    private String name;

    private RoleEnum(String name){
        this.name=name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }


    public static boolean checkRole(String role){

        if(role.equals(SUERADMIN.getName())){
            return true;
        }else if(role.equals(ADMIN.getName())){
            return true;
        }else if(role.equals(TEST.name)){
            return  true;
        }else if(CHARGEORCUSTOMERSERVICE.name.equals(role)){
            return true;
        }else if(CUSTOMERSERVICE.name.equals(role)){
            return true;
        }else if(RECEPTIONIST.name.equals(role)){
            return true;
        }else if(LOOKMENBER.name.equals(role)){
            return true;
        }else if(OTHER.name.equals(role)){
            return true;
        }
        return false;
    }
}
