package com.open.comm.bean;


import com.baomidou.mybatisplus.activerecord.Model;
import org.apache.commons.lang3.builder.ReflectionToStringBuilder;

import java.io.Serializable;


public abstract class BaseEntity<T extends Model> extends Model<T> implements Serializable {

	/**
	 *
	 */
	private static final long serialVersionUID = 1L;


	private Long id;


	public BaseEntity() {

	}

	public BaseEntity(Long id) {
		this();
		this.id = id;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}


	/**
	 * 插入之前执行方法，子类实现
	 */
	public abstract void preInsert();

	/**
	 * 更新之前执行方法，子类实现
	 */
	public abstract void preUpdate();


	@Override
	public String toString() {
		return ReflectionToStringBuilder.toString(this);
	}

}