package com.open.comm.bean;

import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.enums.FieldStrategy;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.Date;


public abstract class DataEntity<T extends Model> extends BaseEntity<T> {

	/**
	 *
	 */
	private static final long serialVersionUID = 1L;
	/**
	 * 删除状态--已删除
	 */
	public static final Integer STATUS_DEL_HASDEL = 0;

	/**
	 * 删除状态--正常
	 */
	public static final Integer STATUS_DEL_NORMAL = 1;

	/**
	 * 删除状态--待删除
	 */
	public static final Integer STATUS_DEL_WAIT = 2;


	/**
	 * 创建日期
	 */
	@TableField(value = "create_time", validate = FieldStrategy.NOT_EMPTY)
	private Date createTime;

	/**
	 * 更新日期 默认没删除
	 */
	@TableField(value = "update_time", validate = FieldStrategy.NOT_EMPTY)
	private Date updateTime;

	@JsonProperty
	private Integer delStatus = 1;


	@JsonProperty
	private Integer sort;

	public DataEntity() {
		super();
	}

	public DataEntity(Long id) {
		super(id);
	}

	/**
	 * 插入之前执行方法，需要手动调用
	 */
	@Override
	public void preInsert() {
		this.updateTime = new Date();
		this.createTime = this.updateTime;
	}

	/**
	 * 更新之前执行方法，需要手动调用
	 */
	@Override
	public void preUpdate() {
		this.updateTime = new Date();
	}

	@JsonProperty

	public Date getCreateTime() {
		return createTime == null ? null : (Date) createTime.clone();
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime == null ? null : (Date) createTime.clone();
	}

	@JsonProperty
	public Date getUpdateTime() {
		return updateTime == null ? null : (Date) updateTime.clone();
	}

	public void setUpdateTime(Date updateTime) {

		this.updateTime = updateTime == null ? null : (Date) updateTime.clone();
	}

	/**
	 * 删除状态
	 *
	 * @return 创建日期
	 */
	public Integer getDelStatus() {
		return delStatus;
	}

	public void setDelStatus(Integer delStatus) {
		this.delStatus = delStatus;
	}

	public Integer getSort() {
		return sort;
	}

	public void setSort(Integer sort) {
		this.sort = sort;
	}
}
