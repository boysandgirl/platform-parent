package com.open.comm.web.editor;


import com.open.comm.web.utils.DateHelper;

import java.beans.PropertyEditorSupport;

/**
 * @author xfz
 */
public class DateEditor extends PropertyEditorSupport {

	@Override
	public void setAsText(String text) {
		setValue(DateHelper.parseDate(text));
	}

}
