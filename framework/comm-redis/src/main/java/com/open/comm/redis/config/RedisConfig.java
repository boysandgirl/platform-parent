package com.open.comm.redis.config;

import org.apache.commons.pool2.impl.GenericObjectPoolConfig;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.connection.jedis.JedisConnectionFactory;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.util.StringUtils;
import redis.clients.jedis.JedisPoolConfig;

/**
 * @Date 2017-06-21
 * @Time 14:05
 * @Author 徐福周
 */
@Configuration
public class RedisConfig {

	private static Logger logger = Logger.getLogger(RedisConfig.class);

	private static final long DEFAULT_MAX_WAIT_MILLIS = -1L;
	@Value("${spring.redis.hostName?:120.25.234.48}")
	private String hostName;

	@Value("${spring.redis.port?:6379}")
	private int port;

	@Value("${spring.redis.timeout?:0}")
	private int timeout;

	@Value("${spring.redis.password?:suerpay}")
	private String password;

	@Value("${spring.redis.pool.max-idle?:" + GenericObjectPoolConfig.DEFAULT_MAX_IDLE + "}")
	private Integer maxIdle;

	@Value("${spring.redis.pool.max-active?:" + GenericObjectPoolConfig.DEFAULT_MAX_TOTAL + "}")
	private Integer maxTotal;

	@Value("${spring.redis.pool. min-idle?:" + GenericObjectPoolConfig.DEFAULT_MIN_IDLE + "}")
	private Integer minIdle;

	@Value("${spring.redis.pool.max-wait?:" + DEFAULT_MAX_WAIT_MILLIS + "}")
	private long maxWaitMillis;

	@Bean
	@ConfigurationProperties(prefix = "spring.redis")
	public JedisPoolConfig getRedisConfig() {
		JedisPoolConfig config = new JedisPoolConfig();

		config.setMaxIdle(maxIdle);//#最大空闲连接数
		config.setMaxTotal(maxTotal);//最大连接数
		config.setMinIdle(minIdle);//初始化连接数
		config.setMaxWaitMillis(maxWaitMillis);//最大等待时间
		return config;
	}

	@Bean
	@ConfigurationProperties(prefix = "spring.redis")
	public JedisConnectionFactory getConnectionFactory() {
		JedisConnectionFactory factory = new JedisConnectionFactory();//这边默认配置是本地的redis 需要的话 动态配置对应的文件
		factory.setHostName(hostName);
		factory.setPort(port);
		if (!StringUtils.isEmpty(password)) {
			factory.setPassword(password);
		}
		factory.setTimeout(timeout);
		JedisPoolConfig config = getRedisConfig();
		factory.setPoolConfig(config);
		logger.info("JedisConnectionFactory bean init success.");
		return factory;
	}


	@Bean
	public RedisTemplate<?, ?> getRedisTemplate() {
		RedisTemplate<?, ?> template = new StringRedisTemplate(getConnectionFactory());
		return template;
	}

}