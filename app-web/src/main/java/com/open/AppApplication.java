package com.open;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.Banner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.support.SpringBootServletInitializer;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * app应用启动
 *
 * @Date 2017-06-23
 * @Time 9:39
 * @Author 徐福周
 */
@SpringBootApplication
public class AppApplication  extends SpringBootServletInitializer {

	private static final Logger LOGGER = LoggerFactory.getLogger(AppApplication.class);

	@Override
	protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
		return application.sources(AppApplication.class);
	}


	@RequestMapping
	@ResponseBody
	public String hello() {
		return "Hello World!";
	}


	public static void main(String[] args) {
		SpringApplication application = new SpringApplication(AppApplication.class);
		application.setBannerMode(Banner.Mode.OFF);
		application.run(args);
		LOGGER.info("Web admin started!!!");
	}
}
