﻿//http://doc.star7th.com/index.php/home/item/show?item_id=1408
var BASEPATH = '';
try{
	BASEPATH = document.getElementById('base').getAttribute('href');
}catch(e){
	alert('请在<head>内添加<base id="base" href="${base}" />');
}
var service = {
	/*
	 * 获取小区
	 */
	getCommunity : {
		ajaxType:'POST',
	 	url:'/wechat/client/community/selList.jhtml'
	},
	/*
	 * 获取小区下所有楼号
	 * @param communityID         	小区ID
	 */
	getBuild : {
		communityID:'',
		ajaxType:'POST',
	 	url:'/wechat/client/build/selList.jhtml'
	},
	/*
	 * 获取楼号底下的所有房间
	 * @param buildID         		楼号ID
	 */
	getRoom : {
		buildID:'',
		ajaxType:'POST',
	 	url:'/wechat/client/room/selList.jhtml'
	},
	/*
	 * 获取房间地下的业主及住户
	 * @param roomID         		房号ID
	 */
	getOwner : {
		roomID:'',
		ajaxType:'POST',
	 	url:'/wechat/client/owner/selList.jhtml'
	},
	/*
	 * 获取快递状态或者配送单状态接口
	 * @param no         			快递号（二选一）
	 * @param bizNo         		业务号（二选一）
	 */
	spStatus : {
		no:'',
		bizNo:'',
		ajaxType:'GET',
	 	url:'/bizserver/wechat/common/shipping/status.jhtml'
	},
	/*
	 * 根据快递号通过快递100接口查询出快递动态，并合并自身系统物流动态
	 * @param no          快递号 二选一
	 * @param id          快递id二选一
	 */
	query : {
		no:'',
		id:'',
		ajaxType:'GET',
	 	url:'/bizserver/wechat/client/express/query.jhtml'
	},
	/*
	 * 判断系统是否有改订单号，如果没有，跳转快递100
	 */
	hasNo : {
		no:'',
		ajaxType:'GET',
	 	url:'/bizserver/wechat/client/express/hasNo.jhtml'
	},
	/*
	 * 通过快递公司编码，查询出快递公司相关信息
	 * @param code          快递编号
	 */
	expressCompanyInfo : {
		code:'',
		ajaxType:'GET',
	 	url:'/bizserver/wechat/client/expressCompany/info.jhtml'
	},
	/*
	 * 根据登陆的业主，查询该业主所拥有（已录入物业系统）的快递信息
	 * @param no          		快递号
	 * @param pageNum          	当前页数
	 */
	queryExpList : {
		no:'',
		pageNum:1,
		ajaxType:'GET',
	 	url:'/bizserver/wechat/client/express/list.jhtml'
	},
	/*
	 * 提交预约配送
	 * @param id          			快递ID
	 * @param shipType          	配送方式 0:自提（pick）1:配送(ship)
	 * @param appointTime          	预约时间
	 */
	appoint : {
		id:'',
		shipType:'',
		appointTime:'',
		ajaxType:'POST',
	 	url:'/bizserver/wechat/client/shipping/appoint.jhtml'
	},
	/*
	 * 业主签收快递
	 * @param bizId          			业务ID
	 * @param bizType          			业务类型
	 */
	signEx : {
		bizId:'',
		bizType:'express_receive',
		ajaxType:'GET',
	 	url:'/bizserver/wechat/client/shipping/sign.jhtml'
	},
	/*
	 * 业主签收快递
	 * @param bizId          		业务ID
	 * @param bizType          		业务类型
	 * @param grade          		满意度
	 * @param review           		评价 限制250字符
	 */
	reviewEx : {
		bizId:'',
		bizType:'express_receive',
		grade:'0',
		review:'',
		ajaxType:'POST',
	 	url:'/bizserver/wechat/client/shipping/review.jhtml'
	},
	/*
	 * 获取预约配送信息
	 * @param id          			配送ID
	 */
	appointInfo : {
		id:'',
		ajaxType:'GET',
	 	url:'/bizserver/wechat/client/shipping/appointInfo.jhtml'
	},
	/*
	 * 物业中心配送订单列表
	 * @param type          			状态类型 0/null:所有 1：未预约 2：未配送 3：已配送 4：失效
	 * @param pageNum          			当前页数
	 * @param telOrOrder				电话号码或者订单号
	 * @param communityId				小区id
	 * @param buildId					楼栋id
	 * @param roomId					房间id
	 * @param startTime					开始时间
	 * @param endTime					结束时间
	 */
	listEx : {
		type:0,
		pageNum:1,
		telOrOrder:'',
		communityId:'',
		buildId:'',
		roomId:'',
		startTime:'',
		endTime:'',
		ajaxType:'GET',
	 	url:'/bizserver/wechat/admin/shipping/list.jhtml'
	},
	/*
	 * 物业中心退单
	 * @param id          			配送订单ID
	 */
	returnEx : {
		id:'',
		ajaxType:'GET',
	 	url:'/bizserver/wechat/admin/shipping/return.jhtml'
	},
	/*
	 * 物业中心恢复失效订单
	 * @param id          			配送订单ID
	 */
	recoverEx : {
		id:'',
		ajaxType:'GET',
	 	url:'/bizserver/wechat/admin/shipping/recover.jhtml'
	},
	/*
	 * 根据配送单ID获得业务详情
	 * @param id          			配送id
	 */
	infoEx : {
		id:'',
		ajaxType:'GET',
	 	url:'/bizserver/wechat/admin/shipping/info.jhtml'
	},
	/*
	 * 配送人员的配送订单列表
	 * @param type          			状态类型 0:待取件 1：配送中 2：已送达 3：已退回
	 * @param pageNum          			当前页数
	 * @param telOrOrder				电话号码或者订单号
	 * @param communityId				小区id
	 * @param buildId					楼栋id
	 * @param roomId					房间id
	 * @param startTime					开始时间
	 * @param endTime					结束时间
	 */
	shipListEx : {
		type:0,
		pageNum:1,
		telOrOrder:'',
		communityId:'',
		buildId:'',
		roomId:'',
		startTime:'',
		endTime:'',
		ajaxType:'GET',
	 	url:'/bizserver/wechat/admin/shipping/shiplist.jhtml'
	},
	/*
	 * 配送人员接单（取件）
	 * @param id          			配送订单ID
	 */
	orderShip : {
		id:'',
		ajaxType:'GET',
	 	url:'/bizserver/wechat/admin/shipping/orderShip.jhtml'
	},
	/*
	 * 配送人员确认送达业主手中
	 * @param id          			配送订单ID
	 */
	arriveShip : {
		id:'',
		ajaxType:'GET',
	 	url:'/bizserver/wechat/admin/shipping/arriveShip.jhtml'
	},
	/*
	 * 获取配送单，可选择的配送人员信息
	 * @param shippingId          	配送订单ID
	 */
	getShiperInfo : {
		shippingId : '',
		ajaxType:'GET',
	 	url:'/wechat/admin/shiper/shiperInfo.jhtml'
	},
	/*
	 * 指派配送人员
	 * @param id          			派送人员id
	 * @param shippingId          	配送订单ID
	 */
	distribution : {
		id : '',
		shippingId : '',
		ajaxType:'GET',
	 	url:'/wechat/admin/shiper/distribution.jhtml'
	},
	doService:function(_data,_callback){
		var _option = {
			url:'',
			ajaxType:'GET',
			data:{},
			callback:_callback||function(){}
		}
	  	_option = $.extend(_option,_data);
		var _newData = {};
		for(var i in _data){
			if(i!='ajaxType'&&i!='url'&&i!='undefined'){
				_newData[i] = _data[i];
			}
		}
	    $.ajax({
	    	url:BASEPATH+_option.url,
	    	type:_option.ajaxType,
	    	dataType:'JSON',
	    	data:_newData,
	    	success:function(ret){
	    		var _ret = '';
	    		if(window.jQuery){
	    			_ret = JSON.stringify(ret);
	    		}else{
	    			_ret =  ret;
	    		}
	    		_option.callback(_ret);
	    	}
		});
	}
}
