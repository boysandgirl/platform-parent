/*
 * 自定义分页加载更多
 * @param dom          		内容存储的对象可以传id,class
 * @param pageNum          	当前页数
 * @param pageSize          每页显示个数
 * @param totalPages        总页数
 * @param nextPageFunction  下一页（加载更多按钮）方法名，字符串类型
 * @param moreTxt          	下一页（加载更多按钮）文本
 * @param loadTxt          	下一页（加载更多按钮）触发文本
 * @param noneTxt          	下一页（加载更多按钮）无数据文本
 * @param callback          回调函数，json类型（dom|pageNum|pageSize|totalPages|loadMoreDom:但前显示加载更多按钮对象'id,class'）
 */
var loadMore = {
	init:function(_data){
		var _option = {
			dom:null,
			pageNum:'1',
			pageSize:'10',
			totalPages:'1',
			nextPageFunction:'nextPage',
			moreTxt:'点击加载更多',
			loadTxt:'疯狂的加载中...',
			noneTxt:'暂无内容',
			callback:function(){}
		}
		var _data = $.extend(_option,_data);
		if(_data.dom==null){
			return;
		}
		var _listDom = $(_data.dom);
		//判断loadMore是否存在
		if($(_listDom.attr('data-loadMoreDom')).length>0){
			$(_listDom.attr('data-loadMoreDom')).remove();
		}
		var _ret = {
			dom:_data.dom,
			pageNum:_data.pageNum,
			pageSize:_data.pageSize,
			totalPages:_data.totalPages,
			loadMoreDom:null
		}
		//创建下一页方法
		var _nextFuc = window[_data.nextPageFunction];
		
		if(_data.totalPages>_data.pageNum){
			
			var _loadMoreId = 'loadMore'+Math.floor(Math.random()*10000);
			var _loadMoreDom = $('<div id="'+_loadMoreId+'" isBusy="false" class="aui-text-center loadMore" style="color:#cccccc;font-size:14px;margin-bottom:15px;">'+_data.moreTxt+'</div>');
			_listDom.after(_loadMoreDom);
			_ret.loadMoreDom = '#'+_loadMoreId;
			_listDom.attr('data-loadMoreDom','#'+_loadMoreId);
			
			_data.pageNum++;
			_ret.pageNum = _data.pageNum;
			_loadMoreDom[CLICK](function(){
				if(_loadMoreDom.attr('isBusy')=='false'){
					_loadMoreDom.attr('isBusy','true');
					_loadMoreDom.text(_data.loadTxt);
					_nextFuc(_ret);
				}
			})
			_listDom.attr('data-pageNum',_data.pageNum);//对象data-pageNum自增
		}
		if(_data.totalPages==_data.pageNum&&$.trim(_listDom.html()).length==0){
			var _loadMoreDom = $('<div class="aui-text-center" style="color:#cccccc;font-size:14px;margin-bottom:15px;">'+_data.noneTxt+'</div>');
			_listDom.after(_loadMoreDom);
		}
		_option.callback(_ret);
	}
}