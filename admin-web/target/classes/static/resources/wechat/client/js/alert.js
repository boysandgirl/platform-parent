function getContextPath() {
    var pathName = document.location.pathname;
    return pathName.substr(0,pathName.indexOf("/",1));
}
if(!window.win){
    window.win = {};
}
var System = {
    id:0,
    func:function(){},
    cropperCallback:function(){},
    alert:function(_data,_callback){
    	var _default = {
            icon:'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAPkAAAD5CAYAAADlT5OQAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAA21pVFh0WE1MOmNvbS5hZG9iZS54bXAAAAAAADw/eHBhY2tldCBiZWdpbj0i77u/IiBpZD0iVzVNME1wQ2VoaUh6cmVTek5UY3prYzlkIj8+IDx4OnhtcG1ldGEgeG1sbnM6eD0iYWRvYmU6bnM6bWV0YS8iIHg6eG1wdGs9IkFkb2JlIFhNUCBDb3JlIDUuNS1jMDE0IDc5LjE1MTQ4MSwgMjAxMy8wMy8xMy0xMjowOToxNSAgICAgICAgIj4gPHJkZjpSREYgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIj4gPHJkZjpEZXNjcmlwdGlvbiByZGY6YWJvdXQ9IiIgeG1sbnM6eG1wTU09Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9tbS8iIHhtbG5zOnN0UmVmPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvc1R5cGUvUmVzb3VyY2VSZWYjIiB4bWxuczp4bXA9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC8iIHhtcE1NOk9yaWdpbmFsRG9jdW1lbnRJRD0ieG1wLmRpZDpkY2ViZDczNy1kZTM0LTgwNGQtOWIzOC1kNjUxN2U1NzViYmEiIHhtcE1NOkRvY3VtZW50SUQ9InhtcC5kaWQ6MDdDQjE3QUFCREM5MTFFNThGQTJCMTRFNTE1MEQ1QzUiIHhtcE1NOkluc3RhbmNlSUQ9InhtcC5paWQ6MDdDQjE3QTlCREM5MTFFNThGQTJCMTRFNTE1MEQ1QzUiIHhtcDpDcmVhdG9yVG9vbD0iQWRvYmUgUGhvdG9zaG9wIENDIChXaW5kb3dzKSI+IDx4bXBNTTpEZXJpdmVkRnJvbSBzdFJlZjppbnN0YW5jZUlEPSJ4bXAuaWlkOkM0OTU1MzJDQzRCREU1MTFCMzA2RDc1QTJERjM0N0M4IiBzdFJlZjpkb2N1bWVudElEPSJ4bXAuZGlkOmRjZWJkNzM3LWRlMzQtODA0ZC05YjM4LWQ2NTE3ZTU3NWJiYSIvPiA8L3JkZjpEZXNjcmlwdGlvbj4gPC9yZGY6UkRGPiA8L3g6eG1wbWV0YT4gPD94cGFja2V0IGVuZD0iciI/PvLY7ooAAB8lSURBVHja7J0J2FbTFsdXb4O5QSlJAzIkMrtSyJCZjCnCl1mZKdLllrpknmVsMISKdK+5DKGkDJeSORG6SCVCmu76t/d3vfJN77D2cM76Pc966qPv7H322f+zh7P2WtVoyAxSoqUaWz22Ndnq2783YKvDVputJlstttWy/qzB9jvbYvsnbIn9+Udrc9nmsc1n+4VtgTZ1mKzo3qrSf1NDmykK1mNrytacbX22zdgasbWwf65lRb1GEctcxLaQ7We2OWxfsn3D9jHbd2wz2b7WF0D4qMjDAiPyRmzbsG3Cth3bpmwt2VZ3XJe1rJGtQ1ngBfAB22ds77B9wjbd/rxcH6eKXCFqwrYtW1u2v7FtbUfmWFibbSdrXbL++xds77JNZnuD7T92+q+oyBMPptq7s+3BtgvbVnatnDSaWzvU/oxp/3tsk9heZJtoZwGKijwR7Mi2N9s+bO09TLtDoLa9d1hvth/YXmMbz/YS2/vaTVTksdGBrRPbgWQ2yZQ/U9+2Tyf7M6b2Y9nG2Km9oiIPEky/D2c7TIWdM9tYu5xtihX7k2Q28RQVufe157FsR5PZCVcKZ2drV9m1+0NsI8h8v1dU5M7ANPw4K+6a2hxitLM2gG0k2yNsr2iz5EZGm6DKrMt2AduHbE/ZEVwF7m4NfybbBDI79N11gFKRFxOsr68h4+F1Pdvm2iRegU/BEDKONxezNdQmUZHnC5xThrF9xNaLjD+4Eg4t2AaRcbPFy3dTbRIVeVXZge1RMt5aJ2pzBE8du4zCy/g2K35FRV4mcCl9kO1Nts7aHNGBE3k97TT+RrYNtUlU5NnTvvvIuF0ep80RPdiQO8+KHdP52iry9FKd7TIyu+UnqTYSB9yHL7Zr9jNU5OnjWPvwryATSEFJLjjVN5jMabiOKvLkg8MiOAUFL6qNtf+nCnjRPc/2cNrW62kSObymprLtqf091XSxS7QeKvLksBfbNLa/a/9WLIh4czuZY67bqMjjZQ27FnuBTHAGRVmVDmSOtv4jyTdZI8EPD66PG6W08/5k7Vf6IyorIrL+Rib2Wmnk1tJorqvb0a0OpdMfvx+ZTTn4xH+iIg8fnEnun+AOieios8lETv2UTORU/PlfMiGVSkMp498ty+G6CCKJQzj1rK1jX5KIEouNKgSWRNTYpG5Y4rQbItTAoeYeFXmYIKLpUDIhhpICxAt3zbft3+Gw8znJnK3+xdpXlfy7ja3BQ7A1mQM7CEa5dgLaG7OYu8ns45xKCYlDlxSRH0PmMEnM8dOWWTHDrfZ1Ml8CPgywnjOtjc/6b5gBbE8maisi5MD/v0nEz6KLHSzw50QVuX/gunhxpHXHqPky2zj755eR3sc8K/pS4cObEKf4SgNY7hphX8MSBcEm8altsIrcD1g3wrFhv8jqjWQECFr4NJkACMsoeSyz9wYbYAUD/4RD7POKyZ/8DjtLOVVF7hZMCUdHNCXEhs6/rU2i9IEZywPW8HI+iO0ItoMpjt38U8h8Tz+SzKZnVMT4nbyzXbOGLvDfbKfGJg6+0/dJqcBXBTv/D1qRY7O0l92HCB3sN2Djs62KXBZ0iEcDryM2z84iswN9AhmvKqVssAdxnRXQ3lb8iwOub137oj5KRS63Nrom4Po9bdec2FmGy+Qc1XBO4ODQ8WS+x/+TjB9AqIwiE41GRV5EsJY9M9C6Dbd7BFhnPqlaLRikQ8Y5A8RsO5fMRmWIIK7cDSrywqll198HB1i3++1au4TMWWWluMAx5xa2LdlOozDdTc+3L3kVeZ7ArXKKHSVDAgH+kS0FQR41UZ8b4GYKz7qzKDxfAuy7jFaR504DK/CQjgHiNBt2VruSJuXzwQoyex1Ys19K5vBNKODT2rMq8qrTmIxL5xaB1AcunN3IeG5NVq15ZymZPGkY2YcGVC84+bwSoqZCqxBGcHwzbRFIffrbl81Dqq3ggFMKAnDuHtDLdzcKMFdbSCLHGhwbWBsEMjXHKat+ZM5hK+Hyql1GXRTIs8KR1XEq8r+CXXQ4Gfg+q4wd3R52aq65seMCn7RakUlG6Rv0nzEq8j9A5gsc5/MdoglvX3yuGax6iZbPyHxu7RlAXQ4LZZkXgsjh6bSj5zpcwrYv2xeqk0QA78jWAazVEd//9rSLfBiZeGy+gDcVzj1frbpIHDPsWn2Q53pg+dcrrSJHhEyfWUNHsLUh8z1eSS597BTeZygnnLk4Om0iLyGzc+0L7MQiueFS1UAqwGYcvpa85bEOI8mT96YPkeNboi8nhgV27X299vvUMYvM3s8Qj3XAseMNki5yhPR9zlMDT7fT83Ha31PNyeQvJuDq5CG+gGuRI+HcGh4aF37FiNM1W/u4YtfIvnLRb0YmNmEiRX4fmfjcrsEJpgNIPdeUPzPCLh0XeigboZ7PS5rIMUU6yUNjDiRzFllRyuI1O8PzEYXmRjKhqhMhcniR3euhEeHgcpn2Y6US4CWHDbnPPZT9DJkcdNGL/GkPjXc2qYOLUnUQjw+x+VyHmkL8+cdjFznWw80dNxyC4N+m/VbJkfl2RH/Xcbn4pNszVpHDy+gUxw3Ww9PSQEkGOIUIV1jXOegwKLWMTeSYhox03FDwYtMTZEqhIKwUNsRcx5J7MjaRI7Cdy+/hyEmuXmxKsafuLmPnI5zVtbGIHGmEOzpsnFvJJNVTlGLyPZkoL787no22Dl3kGL1d+qUj6cI52h8VIfBZbS/HZY4KXeTDHE7TERb5UO2HijCIWuTSBRYhrHqHKnIkrOvscCq1h/Y/xRFwge3rsDz4eDQJUeQjHDYCAuUt1L6nOORKtsccv1iCEjkS1DV0dPMlZPJEK4prkLL4M0dlIZ78/qGIvD65292+kyJIMKckmn0clnVfKCK/w9ENY6PtTO1jimdmsR3uqCxEkenlW+TI7ulqs21/7V9KIDzBdrejspD3ra5PkbuKl4WEg99q31IC4nQ7qktTne1mXyLH6RkXkV7gA68JB5UQOchROciB3syHyG9ycHNzyWShUJQQQQKH3o7Kus61yA8h45kjTVe2ZdqXlIDBoZJpDspBcoZNXIr8Bgc3BR/e8dqHlAg40lE5N7gSOW6opfDN4Exvd+07SiR8QsYjTppD85lB5yNyFzeD6K6LtO8oEQHfdheBJgZKi7wDmeDwkrxJjoPPK0qRKHFQxhGUY6qlXEXe38FNnKR9RYkUpEB6wUE5F0uJHGuB3YUrj5M307SvKBHjIngpHHHWlBD55cIVRxrhntpHlMiZRfIhwVfLRStVFTmykXYRrvggMqmFFSV2MJ2Wjg13YbFFLj0FwSezgdo3lISA+O3SGXwakUnkWTSRSycNxHn0xdo3lATxTzt4SXJ2sUSOzbYWghVFGKdrtE8oCQOD1hXCZRxQbegHjYoh8rMcvPHUP11JIjhU8rNwGScXKvJ6ZD6+S77tbta+oCQUfDG6RbiMMwoVOXbUqwtW8E5diysJB+m7VghevylP2dsVInLps9y6FleSzjy2B4TLODZfkTdmay9YMSRf/0b7gJICpKMZH5WvyI8WrtjV+uyVlPAp26uC12/IU/Y98xF5V8FK4fztFH32SoqQ3oDrmqvIm7LtIlihwfrMlZQxhmTdto/g0TyTi8gPEazMcjLZTxUlTSwT7vfIZNQ+F5HvJ1gZBKafr89cSSF3Cl9/v6qKvBbbnoIVuV+ftSLEpmRylXVia8u2YWD1+4jtfcHr719Vke/Gto5QJeDi94z2RaWI7Mh2uxXQx2zj7GxxEpmz3RPJ5BNbO5D6SoY2257X5Y2rInLJnGNjSf6crZIOMBANZZvK1oPKjj0Ib81dyThdzaQqntoS5lHXU/ayRL5vpG8xJT20IRMmrCSH30Hgk1sCWC7im/k7PkXe0DagBD+xPaf9UykQfNpFRN/mef7+8WQ+Z/lktOC121Umcslv48iGslT7qFKgwLHGrlngdQ4j+ZiFFSG5L4UDKy0rEvmugoU/q31UKYBdrcAzRboewos383QvmK5LnttoW5HI2woWrFN1pZAp6GtFFHgpPuMKSuphl/JEjjjOOwoVOp3tC+2rSh60twKvJnBtHMJaLYEiL3ck34lyCNiex3pcUXIF8QUlT2+tTvIJQ8rjZcFrt+F1+XplibyNYKFvaH9V8hD4BAfltPR0f9+yzRC6dvVsPWeLvLXgDb2ufVbJgQ6OBA7W9XifkrpoVZbItxQqbKaux5UcwLmJlxyWtyihIm+9qshrCIp8kvZbpYrszfai4zJnpUXkWJfUFyrsLe27ShUF7nqDFlFUfe4XfcD2vdR0vdrQD2pki1xyPT5D+69SCR3JzxcYjKRzPN73CkF9NLCD9/9FvomKXPEEDkQ976nsqwK4f0l9bJIt8qZChWDD7Svtx0o54MSUL09IHFF9MoA2mC547WbZIm8uVMg07cdKOSBuga/zDEvYDg+kHd53JXKpkfxT7ctKGRxIfiMEYQ/g60Da4jPBazcvFTl8d5sITtcVJZuD2Z7yWD7iv00IqD2w8fedtMg3IBM1Q4LZ2qeVLA5i+7fH8jFF/1dgbYJQzV8KXXtDxGKHyBsJ3oCKXCkFsfyf9CzwJwJtGymdINJTPXwsrydUwFLBN1Qxwf23tVOb9cm4OX7O9h6ZCKBKcabIT6jAy+VzoevilF0dSZFjnTE34Ibdme0CMptA5YWgRr62IWx3qU7zBqGWxqjAK0RyE7BuRlDkCyjcmG53k3FnPIYqjjGPFwGyXrzL9jfVa3QCPyICgQPJjELrQuRSR+1+DLAxca+I9Hlqjr+Hs7mTyUT6VKrGkQEIfEwkbSUp8noQeYMIK54vr7DtUMDvI2b36arfSjmKZMMOV2WKPiai9pLMdlofIq8rdPHQ1uMPUnEO4tyZx0wgbQIf5XkG8URkbfaD4LXrQOS1UjBdR4DK44q8pj9N9fwXOgcg8McjbDdJrawmKfKQcp5dLXDNu1Tof6ILyef5SqLAAXzplwtdu2YaRI49h72Ern2XTt1X0pX85rmLWeBgqaBeakHkNRMu8n2Fr4+p+ykpH8FHeN4DeDzyNhQXudRIvjiQBnQRcveelE7du3kewSHwxxLQjksERS66Jl8SSAOu56ictE3d4TPwgAq8aCP5UkmRLxO6eI1AGvA3h2WlZep+AvnN8310ggReqpXqQtdelpGcJgTSgK4D9d2TcKFD4MM9C3x0wtq0puCM+rc0iHyKhzKTKvTuKnCxkVzsK1dGcO1cM5AGREbMBZ6EfnKCOuJJZE7kqcBlRF5TUuSLEy5ycLuncu+14kiCwO/zWP4xCRZ4qchrSIpcalevTkCNOJD87fbfF/nU/VTPAscIPpKSTW3Bay+ByKUSvtUPqBGxw36Ux/Jjnbrj2//dHsvvnPARvBTJzKqLIPIfIqx4PiCA3+Uey49t6g6B+4yIgyn6KEoH9QSvPU9S5HUCbMwBJHNYJZepewxCP92zwDunYIruVOTzBEWeCbBBL2G70rPQQ5669yRzZl5H8GSIfD5EKBXBpVFg6/Js+tpR3efUvXugAr/Ns8BHUvpoLD2SS4kcH/c3DLhhsT7v77H8IYEJ/SzPAu+SUoGDFkLXxRn1hZJrctA08MbtZ82n0ENYo5/NdqvnEfxRSi/NpKbqpSM5Yj7/Flnli0l/8rvrjjV6icfyz2W7xWP5XVM8goNqgoPhnBXdW/1auvH2VYpFTnZ93tdj+UPZTvQk8Js8C/wRSjfYu1pf6NorMxiV7n5LZXBoGVFjY8e9j8fyhzleo5+vAg+CjUnumOkX2SKXylm2VWQNPojMJzafa3QXQr+I7QaP99lFBf5/thS8thORb0Ims2JMwFmml2ehnygs8Gs93t+xlO5NtlVpLXjtP03XvxAqJCP8ppLiOrYLPU/dJYTeKwCBP6y6dibyP43kkil6W0Xa+DfYdWtS1ui92a7xvAZXgbubruPU5cfZIn+f5I6cbh/xA8DG1LkJmLr3Ib8++8fqGrzc5WwToWt/uqJ7q7nZIodDzKdChbWN/EHgG/LZnkf0EwoUuE9f/eN0BC8XyXTYM7LXzJQ1mktNR9aP/GHA3bOHx/IRV60kj9/r61ngmKKPUC2Xy66C136/LJHPECqsmvAbyxWD2c70WH6uDjN/JxMRx+cIrlN0f7PcMkU+PdJpiUtwBPMMz1P3blX4d5eR31N23XQErxSkDG/jWuRTBQvcK0EPx3emlAcqWaPDD/8KzyP4Q6rhSmlPcsEbv2X7oCyRf872meBI3jBBDwjnwX0GfhhejtD7kd/jszqCV539Ba89eUX3VsvLEjmYJFjwvgl7SL6PiQ5fZeqO0fsfngWuI3ggIs/+IVPR/4zopnyBzbASz1P3A8kEXbzMYz2OV4HnxOZkvpFL8Xr2DzUq+p9FpmNCHxhGVDgSPeip/Kc837+O4Lmzn+C1ERtiakUj+XskF9gRa/LdE/rQ0MmPS2Fn1RE8P44UvPZbvB7/pSKRI43xy4IV6JrgBzci4fe3Kid4nL3ETGPhwe6lVf9DWSGTn430DRYCcP44JgUdtZvdD1By52jh6z9TFZE/J1iB9ShZ38zLYmTCX2Y6RS+MYwWvjTMok6sichw0l/R+65aCB/l4QoWuU/TCwIkzSe/P8dnfxysSufRojuns6ikR+hEJup8TdYpeMNIOVGXqtjyRPy1YkTUpPRtUY9g6JWQEv181WjCnCV//2VxE/jLbXMHK9EzRg0U21YMjrn+JjuBFYR+SCxABJvFUfU4uIl9up5tS7MC2RYoe8FORCh1T9OGqz6JwjvD1yw3MUVHWUemDBhem7CFD6AfqFD2VIGjKIcJljMpH5BPIHFmTAkEKa6fsYeMbZgwHdXSKXlx6C19/Ak/Vv81H5GC0YMWQNeLcFD7wcRS2H3+JTtGLCr4kSUcUqjCGXmUil36bn5/SBz+ezEaMrsGTz5kk+8l4RWWDcWUif4PtE8EK1qOwcnS75AUKy/uvRNfgIlwsfP2xPFX/oRCRg8HClRyQ4g6AwwQhnMzrriO4CAgq0ki4jErzyldF5MPslECKJuQ3wopvXvUs9O72GSvF5yrh63/Fo/iLxRD5fDKeW5IMSnlngNDbq8ATBRy+pOMa3l2Vf5Sp4sVuFa7sepQuL7iymEgm2P5yh1NJFbgcLmLe31tMkb/MNlO4wsj0USPlHQPhtxBwf5lwOTgoMVR1KMalZOKqS/Ik25xiitzFm6m2TttXMsUKfbHgFH2INrMY+GLkYjO5yqG3cxE5pnYLhCsOV9cm2k9WBuI7QOC6F+kUXZzbctRVPrzF9qaEyLHDfqODRrpH+8lKXirylBppc67XZhVla5KN/FJKv1z+ca5vnJtJfmMII1gH7S8rwebYtCJcB2v8g7U5xXHhTDTbrsfFRP6jo5FW81n/AT6tvVHA72MGBs+6WdqUouDr0LYOysl5vZ/P2uFyBzeyvqOlQQwsZNuF8vM8RDrqHdle0WYUpYFdi0vzXT6DbCbPggY7uKHz7BpHMfRg25nMoaH5lfxbJK7sxbYV29vadOK4SvLYJ59fqkZDZuTze+uSCQ9VTfimcDhmM+1Df6GOFTCmh/hk08g+i9l2av+yNpEzujoS+ddsG/5lLda9VaW/mK/zCVIp3UTyR0U3JbMjfKH2pT+BvZGJ1hR/NCR3wTXyDjxRyPc8pMld4eDmLiDdbVfCBEE6qzsoZ3Yhs4VCRP4TucuHPZZtNe1TSkD0JdlECdkUFFmmUM8cbOd/5+Am4fL6mPYrJRCwATrQUVlYkj3lU+TgDEc3exDbJdq/FM+sTbJJQVel4FgLxRA5zppPdXTDOIS/p/YzxSOIuFvPUVn4Jv5xCCIH3R038nra1xQPwEHLVXCP38n4ilAoIsfhh7sd3Tw24F7S/qY4pluxRFdFzmL7JSSRA3hkzXfUAK3J7LgrigvakdtkE1OpiGdEiinyZeQ29/ihbLdo/1OEacH2ouMyOxfzYsU+3I6Ux084bIyzyTjLKIoESLP9Glsth2XiANiskEUOkChvicNGgdvr8doflSKTsQJ3GakIB4sGSNxIsfnJg+hwWP9I7ZdKEcHx3O0cl3m41NtKgkfZRjpuIOSDOlD7plIEkKuuneMy4SY7LSaRAxzB+85xQ8H9bx/to0oBILTS3o7LRCjuKyXXHVIgFlwnDw8JqYEP0b6q5AEcrQ5yXCa+Sh0qvbkgyWRy58ifzb/sTEJRqqqDCWz7eyi7C5kALNGKHFxGZpfSNTh/e4r2X6US1rCDkY+kk3B4Ge3iDeYChFme76kR+2o/VsqhGZkYeDt5KPtdttNcTVNc8DPbfp4eJJYLd2p/VlYBqaj+w7aFh7J/JRMmm5IkcgB/3B6eHujpZHbea2jfVsi4jU4id0dGVwVr/3lJFDlAKOdhnhoW39DfIY3+mnb+TsaPwxcISuo0Dn7Gw03i7LmvKKMIY/we2xHa11MH/M+xyTXAYx2QT/wG14VmPN0s1iOfeyob59Ef8/ywFbe0IeNN5tP1+Xm2U30U7EvkiHqxGxXpUHwB0zacmltfNZBoTglgmfYB+dt49iZygIwQvuO14dPeh2zHqBYSB7LM4PzEPZ77+QLy8w0+CJGDKSR08ibHzvAImU3B6qqNRLAvmZBkR3uuB45c46DL3DSLHCDIxAkB1OMMO606SDUSLQiXfDvbc+T2HHh5YEk6w3clMoE8nAfIXfz2ikDuNZxCGkomqaMSD0fZl3SPQOqDpegbIVQkE9BDuovCSWxYYtfqJ6p2gqe5XW6NojKyfnriYAoos2wmsAeGb4ih+Jojtvsw+7DaqZaCA96LOPyE5AMhbZxiH+CpkBoqE+DDw+H53gHVZw8yp+juDWikSDtd7dT8CnIbZLEyDiMHp8qSIHJwLVvPwOp0sp3CD9T1ujf2tzMrHCNuGVjdsKMfZC6ATMAP9A67Ng6JtexyAlE1ETq3jurOCQjphYg/z9iZVUgsJbOLPi7UxssE/nCHk9k1DY26bP3ZPiHjObeB6lAEOCs9awUUYuy+H8nkKH8t5EbMRPCg4WfegW1RgHXD5twAK3Zkc9ladVmUPomvGjiaDLfj/QKtJ545Qja/HUODxsAE26AzA60fMm0gmwtOuOEY46Gq1ZxpZmdFWAoNY9sx4Loi4ea25O+QVSJFXvrm3Db0qRGZgATYgPnIrt83Vf1WCI79jrEvcMyKWgReXyTywCnKX2Jp4ExkHQLZWXazDR06OPWEnXh8x4WbJeJ5NVZNrwQ5vq+xL24sx/DpKYZzAwMoQgepWMMhoaFxPvjaSOq7r7WbyWwkPc72Ats3KVpnQ9iIhw9vsC0iqz8OmiBj78gYGz/mmGfXkTknjDVw/UjqvLodtQ6zHWeiFTtS4yIs8PIECbspmc9dHclsnDaL9D6mk/nC81GsDyL2wIYQyNb2Dds+srrXtJ2/g50GziaTLgdif5PMru2iiO5nYzKhjXdh29la7P3rYTLJO5fFfBNJiF46x67Tb2Q7L/KRD1aagP4HK3SMJDPs8mQW27cB9JkW9uW6Cdv2ZMIrtU7YEuMC26eiJ0khis8n4zRxHyUjpFN9O9XtmPXffiPjs40XGzb0/kvmM84XZJJXLLB/FpIfvjYZZx+EK4YfwKa2PTey4m5hX0ZJBbHYT6YIvn+nUeTgaTui4DDJ4QnsgFjTb2etrDTNmN7DC+snOxPAn3C7/N3aYjv1RDDLWtZq2p/rW4HXsZbGKDlwaDo3aTeVxGQDCFqPb68IQnEzhXVKSZq1rCm5ga8cCDYxNok3l0nwg0NqpC0psLO9SnAgWEmrpAo86SIHcJHEd9mTyGFaGiUKsJnZ0c74Fib5RjMpeaCI2QYHjPu1byvM1WSy6YxPw81mUvRgvyfjKYfAA1O1n6cSBOnEJ79L2Fak5aYzKXzQ8COHowY2Wr7Tfp8K4GsAL0O41b6TtpvPpPjBI5kCvgFfm6a3esqAz8BFZBx3xqa1ETIp7wTYcOlt1+v3qiYSw69sg9g2Z7s+7Y2R0f6wEniPnWrf+A9oc0QLHH4Q1htBHvuQ2YchFbmy6toNKZuwOfOQNkc0IIDDrXb5hQQd32iTqMgrA5szOD+Mzyw3UVynwdIEMuP2t+I+h+1LbRIVea4gM+b5dvp3qXaiYHiXTFx+iLufjtwq8mKA015X2U6FEf55bRLn4GANAoTg4BFi/SEu/6/aLJVTQ5sgJ363a3UYToIhXQ/OfzfXphGdTUHc2BCdpc2hIne9bn/HTuMhdCS6gy+0ngIrnLlkvNOQrfQ5bQ4VuW9wXnuENQRZQNaPTmQCN66tzVNl8LkLQS7HWmH/rE2iIg+1o95vrQGZ7B/wlUdAw6baPH8B/gkvWVFjn0O/YqjIo5tylq7f0c7Icb63tZ1T2vYQMYJV4vQXItTqQSEVeaKm9BOsXW5H9R3IJMtDdNNtyMRUSxpz7L7FFLY3yEShnavdQUWeBmZbe8L+jJhqbewIDwccZF9BVJu6Ed0TosgiNnlpdFkIG7nhFuvjVpErJvjiq9YoS/jN7Cjf0gq/IZkwyAi6uI6nekLMM+3+A8SM6Dvv2peWfrdWkSs5CmqatWyq2RG+BVsTtnXJ7Oo3oD9CKTewLwE839KorKV/r26XD0uslUZyxd8XWvGi7Hl2aj3X/v1LK+SF+mji4n8CDACqJ6prFFaUIwAAAABJRU5ErkJggg==',
            title:'成功',
            btn:['返回']
        }
    	if(_callback){System.func = _callback;}else{System.func = function(){}}
    	var _data = $.extend(_default,_data);
    	if(!window.mui){
	        var _it = setInterval(function(){
	            $(".systemAlert_icon").parent().parent().addClass("systemAlert");
	        },50)
	        var _id = Math.floor(Math.random()*10000000);
	        var _data = $.extend(_default,_data);
	        var _content = '';
	        _content+='<div class="systemAlert_icon"><img src="'+_data.icon+'" /></div>';
	        _content+='<div class="systemAlert_title">'+_data.title+'</div>';
	        layer.open({
	            content: _content,
	            btn: _data.btn,
	            skin: 'demo-class',
	            shadeClose: false,
	            yes: function(index){
	                System.func('true');
	                layer.close(index);
	            }, no: function(index){
	                System.func('false');
	                layer.close(index);
	            }
	        });
	        setTimeout(function(){
	            clearInterval(_it);
	        },1000);
        }else{
        	mui.alert(_data.title, '', function() {
        		System.func('true');
			});
        }
    },
    confirm:function(_title,_callback){
    	if(!window.mui){
    		layer.open({
	            content: _title||'你是想确认呢，还是想取消呢？',
	            btn: ['确认', '取消'],
	            shadeClose: false,
	            yes: function(index){
	                //layer.open({content: '你点了确认', time: 1});
	                _callback(true);
	                layer.close(index);
	            }, no: function(index){
	                //layer.open({content: '你选择了取消', time: 1});
	                _callback(false);
	                layer.close(index);
	            }
	        });
    	}else{
    		var btnArray = ['取消', '确认'];
			mui.confirm(_title||'你是想确认呢，还是想取消呢？','', btnArray, function(e) {
				if (e.index == 1) {
					_callback(true);
				} else {
					_callback(false);
				}
			})
    	}
    },
    msg:function(_title,_callback){
    	var _callback = _callback||function(){}
        layer.open({
            content: _title||'您好',
            time: 2 //2秒后自动关闭
        });
        setTimeout(function(){
        	_callback();
        },2000)
    },
    toast:function(_title){
        var _wh = document.documentElement.clientHeight;
        layer.open({
            content: _title||'您好',
            className:'toast',
            shade:false,
            time: 2 //2秒后自动关闭
        });
        $('.toast').css({'top':_wh/2-120});
    },
    panel:function(_data,_callback){
        //var _cpt = 70;
        var _default = {
            title:'&nbsp;',
            html:'',
            style:'width:100%; height:100%; border:none;background:none;'
        }
        if(_callback){System.func = _callback;}else{System.func = function(){}}
        var _id = Math.floor(Math.random()*10000000);
        var _data = $.extend(_default,_data);
        window.win['layerIndex'] = layer.open({
            title:_data.title,
            type: 1,
            className:'SystemPanel',
            content: _data.html,
            style: _data.style
        });
        System.func();
        var _wh = $(window).height();
        $('.SystemPanel').height(_wh);
        return '#noticeLayer'+_id
    },
    cropper:function(_data,_callback){
        var _default = {
            aspectRatio: '2.5 / 1',
            blobURL:'',
            fileDom:null
        }
        _data = $.extend({},_default,_data);
        var files = _data.fileDom.files;
        var URL = window.URL || window.webkitURL;
        var blobURL ;
        if (files && files.length) {
            file = files[0];
            if (/^image\/\w+$/.test(file.type)) {
                blobURL = URL.createObjectURL(file);
                _data.blobURL = blobURL;
            } else {
                window.alert('Please choose an image file.');
                return;
            }
        }
        if(_callback){System.cropperCallback = _callback;}
        var _id = Math.floor(Math.random()*10000000);
        var _html = '';
        _html+='<div id="confirm_'+_id+'" class="window-bg" >';
        _html+='<div class="window confirm" style="width:100%;height:100%;position:relative;border-radius: 0px;-webkit-border-radius: 0px;-moz-border-radius: 0px;-ms-border-radius: 0px;-o-border-radius: 0px;border-radius: 0px;">';
        _html+='<div class="window-content" style="height:100%;-webkit-overflow-scrolling: touch;-webkit-transform: translateZ(0px);overflow: auto;"><iframe id="iframe_#confirm_'+_id+'" name="iframe_#confirm_'+_id+'" style="width: 100%;height:1000px;border: 0;overflow: auto;" frameborder="0"></iframe></div>';
        _html+='<button class="select-container-colbtn" style="z-index: 2;top:-5px;" onclick="System.closeWin(\'#confirm_'+_id+'\')"></button>';
        _html+='</div>';
        _html+='</div>';
        $('body').append(_html);
        setTimeout(function(){
            System.openWin('#confirm_'+_id);
            var _mt = ($(window).height()-$('#confirm_'+_id).find('.window').height())/2;
            $('#confirm_'+_id).find('.window').css('margin-top',_mt);
            $('#confirm_'+_id).find('.window-content iframe').attr('src',getContextPath()+'/resources/wechat/client/c.html?aspectRatio='+_data.aspectRatio+'&blobURL='+_data.blobURL+'&v='+_id);
            //清空fileDom
            $(_data.fileDom).val('');
        },500)
        System.id = '#confirm_'+_id;
        return '#confirm_'+_id
    },
    searchSelect:function(data,callback){
        var _id = 'searchSelect'+Math.floor(Math.random()*100000);
        var _html = '';
        _html+='<div id="'+_id+'" class="search-select-container">';
        _html+='    <div class="search-select-container-text">';
        _html+='        <span class="search-select-container-text-left"><i class="aui-iconfont aui-icon-left"></i></span>';
        _html+='        <span class="search-select-container-text-center"><i class="aui-iconfont aui-icon-roundclosefill"></i><input type="text" value="" placeholder="请输入要查找的内容" /></span>';
        _html+='        <span class="search-select-container-text-right"><i class="aui-iconfont aui-icon-menu"></i></span>';
        _html+='    </div>';
        _html+='    <div class="search-select-container-list">';
        _html+='        <div class="search-select-container-list-item aui-text-center" >加载中...</div>';
        _html+='    </div>';
        _html+='</div>';
        var _body = $('body');
        var _oldScrollTop = _body.scrollTop();
        var _selectPanel = $(_html);
        var _sObj = $('#'+_id);
        var _bst = setInterval(function(){
            _body.scrollTop(0);
        },1);
        if(_sObj.length==0){
            _body.append(_selectPanel);
            //填充数据
            var _item = '';
            for(var i = 0 ; i < data.key.length ; i++){
                var _cur = '';
                if(data.key[i]==data.curKey&&data.curKey.length>0){
                    _cur = 'cur';
                }
                _item+='<div class="search-select-container-list-item '+_cur+'" data-key="'+data.key[i]+'" data-value="'+data.value[i]+'">'+data.value[i]+'</div>';
            }
            _sObj = $('#'+_id);
            var _its;
            var _st = setInterval(function(){
                _its = _sObj.find('.search-select-container-list');
                if(_its.length>0){
                    _its.html(_item);
                    clearInterval(_st);

                    _its.find('.search-select-container-list-item')[CLICK](function(){
                        _body.scrollTop(_oldScrollTop);
                        clearInterval(_bst);
                        //关闭
                        _sObj.remove();
                        callback({
                            key:$(this).attr('data-key'),
                            value:$(this).attr('data-value')
                        });
                    })

                    _sObj.find('.search-select-container-text-left')[CLICK](function(){
                        _body.scrollTop(_oldScrollTop);
                        clearInterval(_bst);
                        //关闭
                        _sObj.remove();
                    })

                    var _oldText = '';
                    var _ist;
                    _sObj.find('.search-select-container-text-center input').bind('focus',function(){
                        var _inp = $(this);
                        var _tx = $.trim(_inp.val());
                        _oldText = _tx;
                        console.log(_tx);
                        _ist = setInterval(function(){
                            _tx = $.trim(_inp.val());
                            if(_tx.length==0){
                                _its.find('.search-select-container-list-item').show();
                                //clearInterval(_ist);
                                return;
                            }
                            if(_oldText != _tx){
                                //clearInterval(_ist);
                                _its.find('.search-select-container-list-item').hide();
                                console.log("sfwefwef"+_inp.val());
                                var _v = _tx.split('');
                                for(var t = 0 ; t < _v.length ; t++){
                                    for(var n = 0 ; n < data.value.length ; n++){
                                        //console.log(_v[t]+':'+data.value[n]+'              '+data.value[n].indexOf(_v[t]));
                                        if(data.value[n].indexOf(_v[t])!=-1){
                                            _its.find('[data-value="'+data.value[n]+'"]').show();
                                        }
                                    }
                                }
                            }
                        },1)
                    })

                    _sObj.find('.search-select-container-text-center input').bind('blur',function(){
                        clearInterval(_ist);
                    })

                    _sObj.find('.search-select-container-text-center .aui-icon-roundclosefill')[CLICK](function(){
                        _sObj.find('.search-select-container-text-center input').val('');
                    })
                }
            },300)
        }else{
            _sObj.show();
        }
    },
    showLoading:function(_data){
    	var _option = {
            title:'加载中，请稍后...',
            clickHide:false,
            delay:0
        }
        _data = $.extend(_option,_data);
        var _html = '<div class="loadingPanel">';
            _html += _data.title;
            _html += '</div>';
        if($('.loadingPanel').length==0){
            var _loading = $(_html);
            $('body').append(_loading);
            _loading.css({
                'padding-top':_loading.height()/2-50
            });
            if(_data.clickHide){
                _loading[CLICK](function(){
                    _loading.hide();
                })
            }
            if(_data.delay!=0){
                setTimeout(function(){
                    _loading.hide();
                },_data.delay)
            }
        }else{
            var _loading = $('.loadingPanel');
            _loading.show();
            _loading.css({
                'padding-top':_loading.height()/2-50
            });
        }
        $(window).resize(function(){
            var _loading = $('.loadingPanel');
            _loading.css({
                'padding-top':_loading.height()/2-50
            });
        })
    },
    hideLoading: function () {
        var _loading = $('.loadingPanel');
        _loading.hide();
    },
    openWin:function(_dom){
        $(_dom).show();
        $('body').addClass('showWin');
    },
    closeWin:function(_dom,_bool){
        $(_dom).remove();
        $('body').removeClass('showWin');
        System.func(_bool);
    }
}