function getContextPath() {
    var pathName = document.location.pathname;
    return pathName.substr(0,pathName.indexOf("/",1));
}
var InterValObj; //timer变量，控制时间
var count = 60; //间隔函数，1秒执行
var curCount;//当前剩余秒数
var code = ""; //验证码
var codeLength = 6;//验证码长度

var _sto = setInterval; 
window.setInterval = function(callback,timeout,param)
{ 
var args = Array.prototype.slice.call(arguments,2); 
var _cb = function()
{ 
callback.apply(null,args); 
}  
_sto(_cb,timeout); 
}


function sendMessage(templateId,buttonId,bizType) {
	curCount = count;
	var jbPhone = $("#phone").val();
	if (jbPhone != "") {
		$("#"+buttonId).attr("disabled","disabled");
		// 向后台发送处理数据
		$.ajax({
			type: "GET", // 用POST方式传输
			dataType: "JSON", // 数据格式:JSON
			url: getContextPath()+"/common/sms/captcha1.jhtml", // 目标地址
			data: {
				phone:jbPhone,
				templateId:templateId,
				bizType:bizType
			},
			success: function (data){
				$("#"+buttonId).removeAttr("disabled");
				if(data.type == 'success'){
					alert(data.content);
					// 设置button效果，开始计时
					$("#"+buttonId).prop('disabled', true);
					$("#"+buttonId).html("重发" + curCount);
					InterValObj = window.setInterval(SetRemainTime, 1000,buttonId); // 启动计时器，1秒执行一次
				}else {
					alert(data.content);
				} 
			}
		});
	}else{
		alert("手机号码不能为空");
	}
}

//timer处理函数
function SetRemainTime(buttonId) {
	if (curCount == 0) {                
		window.clearInterval(InterValObj);// 停止计时器
		$("#"+buttonId).prop('disabled', false);// 启用按钮
		$("#"+buttonId).html("重新");
		code = ""; // 清除验证码。如果不清除，过时间后，输入收到的验证码依然有效
	}else {
		curCount--;
		$("#"+buttonId).html("重发" + curCount);
	}
}
