$(function(){

    getAllProvinces();
    getAllShop(null,null,null);
    $("#province").change(function(){
        var provinceId = $("#province").val();
        $("#provinceId").val($("#province").find("option:selected").val());
        $("#city").empty();
        $("#area").empty();
        $("#shop").empty();
        getAllCitiesByProvinceId(provinceId);

        getAllShop(provinceId,null,null);
    });

    $("#city").change(function(){
        var cityId = $("#city").val();
        $("#cityId").val($("#city").find("option:selected").val());
        $("#area").empty();
        $("#shop").empty();
        getAllAreaByCityId(cityId);
       var provinceId= $("#provinceId").val();
        getAllShop(provinceId,cityId,null);
    });

    $("#area").change(function(){
        $("#areaId").val($("#area").find("option:selected").val());
        $("#shop").empty();
        var cityId = $("#cityId").val();
        var provinceId= $("#provinceId").val();
        var areaId=$("#areaId").val();
        getAllShop(provinceId,cityId,areaId);
    });

    $("#shop").change(function(){
        $("#shopId").val($("#shop").find("option:selected").val());
    });

});


function getAllShop(provinceId,cityId,areaId){
    service.getAllOpenShopsParentIsNull.provinceId=provinceId;
    service.getAllOpenShopsParentIsNull.cityId=cityId;
    service.getAllOpenShopsParentIsNull.areaId=areaId;

    service.doService(service.getAllOpenShopsParentIsNull,function(ret){
        var json = eval('('+ret+')');
        if(json.code == 1000){
            var datas = json.data;
            var temp = $("<option>").val("").text("请选择门店");
            $("#shop").append(temp);
            for (x in datas) {
                var option = $("<option>").val(datas[x].id).text(datas[x].name);
                $("#shop").append(option);
            }
        }
    });
}
// 查询所有省份
function getAllProvinces(){

    service.doService(service.getAllProvinces,function(ret){
        var json = eval('('+ret+')');
        if(json.code == 1000){
            var datas = json.data;
            var temp = $("<option>").val("").text("请选择省份");
            $("#province").append(temp);
            for (x in datas) {
                var option = $("<option>").val(datas[x].id).text(datas[x].name);
                $("#province").append(option);
            }
        }
    });
}

// 根据省份id 查询对应的 城市
function getAllCitiesByProvinceId(id) {
    service.getAllCitiesByProvinceId.id = id;
    service.doService(service.getAllCitiesByProvinceId,function(ret){
        var json = eval('('+ret+')');
        if(json.code == 1000){
            var datas = json.data;
            var temp = $("<option>").val("").text("请选择城市");
            $("#city").append(temp);
            for (x in datas) {
                var option = $("<option>").val(datas[x].id).text(datas[x].name);
                $("#city").append(option);
            }
            $("#cityId").val($("#city").find("option:selected").val());
        }
    })
}

// 根据城市id 查询对应的 区域
function getAllAreaByCityId(id) {
    service.getAllAreaByCityId.id = id;
    service.doService(service.getAllAreaByCityId, function(ret){
        var json = eval('('+ret+')');
        if(json.code == 1000){
            var datas = json.data;
            var temp = $("<option>").val("").text("请选择区域");
            $("#area").append(temp);
            for (x in datas) {
                var option = $("<option>").val(datas[x].id).text(datas[x].name);
                $("#area").append(option);
            }
            $("#areaId").val($("#area").find("option:selected").val());
        }
    });
}
