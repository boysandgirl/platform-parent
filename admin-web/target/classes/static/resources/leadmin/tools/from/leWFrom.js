var LeFrom={
	loading: function (type)
	{
		alert("1");
	},
	loading_mini: function (selector, type){
		alert("2");
	},
	showRequest:function (formData, jqForm, options){  
		console.log("提交");
		if($('#leSubmitBt').attr('dataBef')=='true'){
			if(leFromBefore()){
			   $("button").attr('disabled',true);
			   return true;  //只要不返回false，表单都会提交,在这里可以对表单元素进行验证  
		    }else{
		      return false;
		    }
		}else{
			 $("button").attr('disabled',true);
			  return true;  //只要不返回false，表单都会提交,在这里可以对表单元素进行验证 
		}
	   
	},
	/**
	 * 生成随机数
	 * @param len
	 * @returns
	 */
	randomString:function (len) {
			len = len || 32;
			var $chars = 'ABCDEFGHJKMNPQRSTWXYZabcdefhijkmnprstwxyz2345678';
			var maxPos = $chars.length;
			var pwd = '';
			for (i = 0; i < len; i++) {
			pwd += $chars.charAt(Math.floor(Math.random() * maxPos));
		}
		return pwd;
	},
	showResponse:function (responseText, statusText){
		var leSubmitBtMsg = $('#leSubmitBt').attr('dataMsg');
		if($('#leSubmitBt').attr('dataRes')=='true'){
			leFromRequest(responseText);
		}else{
			if(responseText.type == "success"){
				parent.layer.msg(leSubmitBtMsg+'成功！',{icon: 3,time:1000});
		 	}else if(responseText.type == "error"){
		 		parent.layer.msg(leSubmitBtMsg+'失败！',{icon: 3,time:1000});
		 	} 
		}
		//leFromRequest(responseText);
		$("button").attr('disabled',false);
		
	}
}
/**
 * 表单处理
 */
LeFrom.options = {
		target : '#letype',   
        beforeSubmit:  LeFrom.showRequest,  //提交前处理 
        success:       LeFrom.showResponse,  //处理完成 
        dataType:  'json'  
    };  
