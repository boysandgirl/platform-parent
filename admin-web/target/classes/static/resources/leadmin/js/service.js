﻿var service = {

	/**
	 * 获取省份信息
	 */
	getAllProvinces: {
		ajaxType: 'GET',
		url: '/sys/dictionary/v1/getAllProvinces'
	},

	/**
	 * 根据省份id，获取 城市
	 */
	getAllCitiesByProvinceId: {
		ajaxType: 'GET',
		id:'',
		url: '/sys/dictionary/v1/getAllCitiesByProvinceId'
	},

	/**
	 * 根据城市id，获取 区域
	 */
	getAllAreaByCityId:{
		ajaxType: 'GET',
		id:'',
		url: '/sys/dictionary/v1/getAllAreaByCityId'
	},

    /**
	 * 修改 公寓状态 1 上架  0 下架
     */
    changeStatus:{
        ajaxType: 'POST',
        id:'',
        delStatus:'',
        url: '/admin/biz/apartment/changeStatus'
	},
    /**
     * 修改 户型状态 1 上架  0 下架
     */
    changeHouseTypeStatus:{
        ajaxType: 'POST',
        id:'',
        delStatus:'',
        url: '/admin/biz/apartment/houseType/changeStatus'
    },

    /**
	 * 通过开放城市id，获取公寓下拉列表
     */
    findApartmentByOpenZoneId:{
        ajaxType: 'POST',
        openZoneId:'',
        url: '/admin/biz/apartment/findApartmentByOpenZoneId'
    },

    /**
     * 通过开放城市id，获取合辑下拉列表
     */
    findCollectionByOpenZoneId:{
        ajaxType: 'POST',
        openZoneId:'',
        collectionId:'',
        url: '/admin/biz/collection/findCollectionByOpenZoneId'
    },
    /**
     * 获取开放城市id 和 name
     */
    findOpenZoneForIdAndName:{
        ajaxType: 'GET',
        url: '/admin/sys/zone/findOpenZoneForIdAndName'
    },

    /**
	 *  通过开放城市id 获取 商圈 id 和name
     */
    findBussinessAreaIdAndName:{
        ajaxType: 'POST',
        openZoneId:'',
        url: '/admin/sys/business/findBussinessAreaIdAndName'
    },
    /**
	 *  通过 公寓id 查找 户型下拉列表
     */
    findHouseTypeIdAndName:{
        ajaxType: 'POST',
        apartmentId:'',
        url: '/admin/biz/apartment/houseType/findHouseTypeIdAndName'
    },

/*************************************************************************************************************************************************/

    /**
     * 获取所有的父级字典类型
     */
    findParentDataType:{
        ajaxType: 'POST',
        url: '/admin/sys/data/findParentDataType'
    },

    /**
     * 禁用 启用 数据类型
     */
    forbidDataType:{
        ajaxType: 'POST',
        id:'',
        delStatus:'',
        url: '/admin/sys/data/forbidDataType'
    },
    /**
     * 禁用数据字典
     */
    forbidDataResource:{
        ajaxType: 'POST',
        id:'',
        delStatus:'',
        url: '/admin/sys/data/forbidDataResource'
    },
    /**
     * 删除数据类型
     */
    deleteDataType:{
        ajaxType: 'POST',
        id:'',
        url: '/admin/sys/data/deleteDataType'
    },
    /**
     * 删除数据字典
     */
    deleteDataResource:{
        ajaxType: 'POST',
        id:'',
        url: '/admin/sys/data/deleteDataResource'
    },

    /**********************************************************************************************************/
    /**
     * 禁用 启用 开放城市
     */
    forbidOpenZone:{
        ajaxType: 'POST',
        openZoneId:'',
        delStatus:'',
        url: '/admin/sys/zone/forbidOpenZone'
    },
    //获取所有省份
    findAllProvinces:{
        ajaxType: 'GET',
        url: '/admin/sys/zone/findAllProvinces'
    },
    //获取省份下的城市
    findAllCitiesByProvinceCode:{
        ajaxType: 'POST',
        provinceCode:'',
        url: '/admin/sys/zone/findAllCitiesByProvinceCode'
    },
    //获取城市下的区域
    findAllAreasByCitCode:{
        ajaxType: 'POST',
        cityCode:'',
        url: '/admin/sys/zone/findAllAreasByCitCode'
    },

    findAllAreasByCitCode:{
        ajaxType: 'GET',
        url: '/admin/sys/zone/findAllAreasByCitCode'
    },

    /**
     * 获取所有开放城市
     */
    findOpenZoneList:{
        ajaxType: 'GET',
        url: '/admin/sys/zone/findOpenZoneList'
    },
    /**
     * 获取开放城市下的所有区域
     */
    findAreasByOpenZoneId:{
        ajaxType: 'POST',
        openZoneId:'',
        url: '/admin/sys/zone/findAreasByOpenZoneId'
    },

    /***********************基础数据字典获取********************************/

    findCommDatas:{
        ajaxType: 'POST',
        dataTypeId: '',
        url: '/admin/sys/data/findCommDatas'
    },

    //禁用 启用商圈
    forbidBusiness:{
        ajaxType: 'POST',
        id:'',
        delStatus:'',
        url: '/admin/sys/business/forbidBusiness'
    },


    /****************预约模块*********************/

    /***
     * 主管为用户分配客服
     */
    directorDistribution:{
        ajaxType: 'POST',
        orderId:'',
        customServiceId:'',
        customServiceName:'',
        url: '/admin/biz/order/directorDistribution'
    },


    findRoles:{
        ajaxType: 'POST',
        url: '/admin/sys/role/findRoles'
    },

    /**
     * 禁用 启用 用户
     */
    forbidEmployee:{
        ajaxType: 'POST',
        id:'',
        delStatus:'',
        url: '/admin/sys/employee/forbid'
    },
    /**
     * 禁用 启用 角色
     */
    forbidRole:{
    ajaxType: 'POST',
        id:'',
        delStatus:'',
        url: '/admin/sys/role/forbid'
},
    //获取所有的一级菜单栏目  目前最多
    findAllParentResources:{
        ajaxType: 'POST',
        url: '/admin/sys/resource/findAllParentResources'
    },

    /**
     * 禁用 启用 角色
     */
    forbidResource:{
        ajaxType: 'POST',
        id:'',
        delStatus:'',
        url: '/admin/sys/resource/forbid'
    },
    /**************合辑上下架******************/
    collectionChangeStatus: {
        ajaxType: 'POST',
        id:'',
        delStatus:'',
        url: '/admin/biz/collection/changeStatus'
    },

    findApartmentByOpenZoneIds:{
        ajaxType: 'POST',
        openZoneIds:'',
        url: '/admin/biz/apartment/findApartmentByOpenZoneIds'
    },

    doService: function (_data, _callback) {
		var _option = {
			url: '',
			ajaxType: 'GET',
			data: {},
			callback: _callback || function () {
			}
		}
		_option = $.extend(_option, _data);
		var _newData = {};
		for (var i in _data) {
			if (i != 'ajaxType' && i != 'url' && i != 'undefined') {
				_newData[i] = _data[i];
			}
		}
		$.ajax({
			url: _option.url,
			type: _option.ajaxType,
			dataType: 'JSON',
			data: _newData,
			success: function (ret) {
				var _ret = '';
				if (window.jQuery) {
					_ret = JSON.stringify(ret);
				} else {
					_ret = ret;
				}
				_option.callback(_ret);
			}
		});
	}
};
