var pagNum=1;
var lePagNumber=1;
$(document).ready(function(){
	parent.leIframeName=window.name;
	$('table[leType=leTable]').each(function(){
		var _lePage = $(this).attr('lePageSize');
		var _id = $(this).attr('id')||'leTabel'+Math.floor(Math.random()*100000);
		$(this).attr('id',_id);
		leTableData(1,_lePage,_id);
		$("#leSeachBtn").click(function(){
			loadSearchData(_id);
		});
		$(this).hide();
	});
	$('div[leType=leGrid]').hide();
})
function  loadPageData(pageNumber,_id){
	var _this=$("#"+_id);
	leTableData(pageNumber,_this.attr('lePageSize'),_id);
	console.log(_id);
}

function  loadSearchData(_id){
	var _this=$("#"+_id);
	leTableData(1,_this.attr('lePageSize'),_id);
	console.log(_id);
}
//表格pageNumber 读取页数，pageSize 数据条数  
function leTableData(pageNumber,pageSize,_id){
	parent.leIframeName=window.name;
	var _this=$("#"+_id);
	lePagNumber=pageNumber;
	if(_this.attr('lePage')=="true"){
		var params ='{"pageNumber":'+pageNumber+','+'"pageSize":'+pageSize+'}';
		if(document.getElementById(_id+"SearchFrom")){
		    params ='{"pageNumber":'+pageNumber+','+'"pageSize":'+pageSize+','+serializeForm('leTableSearchFrom')+'}';
		}
		$.post(_this.attr('leUrl'),JSON.parse(params),function(responseText){
			if(responseText != null){
				//当前页
				var lepageNumber=JSON.parse(responseText.pageNumber);
				//当前页数量
				var lepageSize=JSON.parse(responseText.pageSize);
				//总数据数
				var letotal=JSON.parse(responseText.total);
				//总页数
				var letotalPages=JSON.parse(responseText.totalPages);
				
				var pageS=(lepageNumber-1)*(lepageSize)+1;
				var pageE=lepageNumber*lepageSize;
				var pageC=letotal;
				
				if(pageE>pageC){
					pageE=pageC;
				}
				pagNum=pageS;
				var	html='<div class="clearfix" id="'+_id+'pagination">'+
									'<div class="pull-left pagination">'+
										'<span class="pagination-info">'+
										  '显示第'+pageS+'到第 '+pageE+'条记录，总共'+pageC+'条记录'+
										'</span>'+
								    '</div>'+
					                '<div class="pull-right pagination-sm" id="lePageCenter">';
				var pageHtml='<ul class="pagination pagination-outline">';
				if(lepageNumber==1){
					pageHtml=pageHtml+'<li class="page-first disabled"><a href="javascript:void(0)">«</a></li>'+
					'<li class="page-pre disabled"><a href="javascript:void(0)">‹</a></li>';
				}else{
					pageHtml=pageHtml+'<li class="page-first"><a href="javascript:void(0)" onclick="loadPageData('+1+',\''+_id+'\')">«</a></li>'+
					'<li class="page-pre"><a href="javascript:void(0)" onclick="loadPageData('+(lepageNumber-1)+',\''+_id+'\')">‹</a></li>';
				}
				var num=1;
				if(letotalPages>5){
					if(lepageNumber>3){
						num=lepageNumber-2;
					}	
				}
				if(letotalPages>5){
					console.log(letotalPages);
					if(lepageNumber>(letotalPages-2)){
						if(lepageNumber == letotalPages){
							num=num-2;
						}
						if(lepageNumber == (letotalPages-1)){
							num=num-1;
						}
					}
				}
				for (var i=num,j=0;j<5;i++,j++){
					if(i<=letotalPages){
//						console.log(j);
						if(lepageNumber==i){
							pageHtml=pageHtml+'<li class="page-number active" style="background-color:#CCCCCC;border-color: #CCCCCC;" ><a href="javascript:void(0)" onclick="loadPageData('+i+',\''+_id+'\')">'+i+'</a></li>';
						}else{
							pageHtml=pageHtml+'<li class="page-number"><a href="javascript:void(0)" onclick="loadPageData('+i+',\''+_id+'\')">'+i+'</a></li>';
						}
					}
				}
				
				if(lepageNumber==letotalPages){
					pageHtml=pageHtml+
					'<li class="page-next disabled"><a href="javascript:void(0)">›</a></li>'+
					'<li class="page-last disabled"><a href="javascript:void(0)">»</a></li>'+
					'</ul>';
				}else{
					pageHtml=pageHtml+
					'<li class="page-next"><a href="javascript:void(0)" onclick="loadPageData('+(lepageNumber+1)+',\''+_id+'\')">›</a></li>'+
					'<li class="page-last"><a href="javascript:void(0)" onclick="loadPageData('+letotalPages+',\''+_id+'\')">»</a></li>'+
					'</ul>';
				}
				//是否存在分页
				if(pageC<lepageSize){
					pageHtml="";
				}
				html=html+pageHtml+'</div></div>';
				if($("#"+_id+"pagination").length>0){
					$("#"+_id+"pagination").remove();
					_this.bootstrapTable('load',responseText.content);
					$('div[leType=loading]').hide();
					$('div[leType=leGrid]').show();
					_this.show();
					_this.parent().parent().after(html);
				}else{
					if(_this.attr('data-detail-view')){
						
						_this.bootstrapTable({
				            data:responseText.content,
				            search: !1,
				            pagination: !1,
				            showRefresh: !1,
				            showToggle: !1,
				            showColumns: !1,
				            iconSize: "outline",
				            toolbar: "div[leType="+_id+"Toolbar]"
				    	});
					}else{
						_this.bootstrapTable({
				            data:responseText.content,
				            search: !1,
				            pagination: !1,
				            showRefresh: !1,
				            showToggle: !1,
				            showColumns: !1,
				            iconSize: "outline",
				            toolbar: "div[leType="+_id+"Toolbar]",
				            icons: {
				                refresh: "glyphicon-repeat",
				                toggle: "glyphicon-list-alt",
				                columns: "glyphicon-list"
				            }
				    	});
					}
					_this.parent().parent().after(html);
					$('div[leType=loading]').hide();
					$('div[leType=leGrid]').show();
					_this.show();
				}
			}else{
				parent.layer.alert("数据加载失败！",{skin:'layui-layer-molv'});
			}
		});
	}else{
		$("#"+_id+"SearchFrom").hide();
		$.post(_this.attr('leUrl'),{pageNumber:pageNumber,pageSize:pageSize},function(responseText){
			if(responseText != null){
				_this.bootstrapTable({
		            data: responseText,
		            search: !0,
		            pagination: !0,
		            showRefresh: !0,
		            iconSize: "outline",
		            toolbar: "div[leType="+_id+"Toolbar]",
		            icons: {
		                refresh: "glyphicon-repeat",
		                toggle: "glyphicon-list-alt",
		                columns: "glyphicon-list"
		            }
		    	});
				$('div[leType=loading]').hide();
				$('div[leType=leGrid]').show();
				_this.show();
			}else{
				parent.layer.alert("数据加载失败！",{skin:'layui-layer-molv'});
			}
		});
	}
}
function leRefresh(){
	$('table[leType=leTable]').each(function(){
		var _lePage = $(this).attr('lePageSize');
		var _id = $(this).attr('id');
		if($(this).attr('lePage')=="true"){
			loadPageData($("#"+_id+"pagnum").val(),_id)
		}else{
			$.post($('table[leType=leTable]').attr('leUrl'),{},function(responseText){
				if(responseText.type == "success"){
				    var data=JSON.parse(responseText.content);
				    $('table[leType=leTable]').bootstrapTable('load',data);		
				}else if(responseText.type == "error"){
					parent.layer.alert("数据加载失败！",{skin:'layui-layer-molv'});
					} 
			    });
		}
	});
}
function getRowId(row,row,index){
	return index+pagNum;
}
//获取单个input中的【name,value】数组  
function inputSelector(element) {    
  if (element.value!=='')    
     return [element.name, element.value];    
}
//获取单个input中的【name,value】数组  
function inputRadio(element) {    
  if (element.checked)    
     return [element.name, element.value];    
}   
//获取单个input中的【name,value】数组  
function selectSelector(element) {
  if (element.options[element.selectedIndex].value!='')    
     return [element.name, element.options[element.selectedIndex].value];    
}   
      
function input(element) {    
    switch (element.type.toLowerCase()) {    
      case 'submit':    
      case 'hidden':    
      case 'password':
      case 'text':    
        return inputSelector(element);    
      case 'checkbox':    
      case 'radio':    
        return inputRadio(element);  
      case 'select-one':
        return selectSelector(element);  
    }    
    return false;    
}    
  
//组合URL  
function serializeElement(element) {    
    var method = element.tagName.toLowerCase();    
    var parameter = input(element);    
    
    if (parameter) {    
      var key = encodeURIComponent(parameter[0]);    
      if (key.length == 0) return;    
     // alert(key);
      if (parameter[1].constructor != Array)    
        parameter[1] = [parameter[1]];    
          
      var values = parameter[1];    
      var results = [];    
      for (var i=0; i<values.length; i++) {    
        results.push('"'+key + '":"' + (values[i])+'"');    
      }    
      return results.join('&');    
    }    
 }    
  
//调用方法     
function serializeForm(formId) {    
    var elements = getElements(formId);    
    var queryComponents = new Array();    
    for (var i = 0; i < elements.length; i++) {    
      var queryComponent = serializeElement(elements[i]); 
      if (queryComponent)    
        queryComponents.push(queryComponent);    
    }    
    
    return queryComponents.join(',');  
}    
  //获取指定form中的所有的<input>对象    
function getElements(formId) {    
    var form = document.getElementById(formId);    
    var elements = new Array();    
    var tagElements = form.getElementsByTagName('input');    
    for (var j = 0; j < tagElements.length; j++){
         elements.push(tagElements[j]);  
    }  
    var selectElements =form.getElementsByTagName('select');
     for (var j = 0; j < selectElements.length; j++){
         elements.push(selectElements[j]);  
    } 
    return elements;    
}

/**
 * 和PHP一样的时间戳格式化函数
 * @param  {string} format    格式
 * @param  {int}    timestamp 要格式化的时间 默认为当前时间
 * @return {string}           格式化的时间字符串
 */
function date(format, timestamp){ 
	var a, jsdate=((timestamp) ? new Date(timestamp*1000) : new Date());
	var pad = function(n, c){
		if((n = n + "").length < c){
			return new Array(++c - n.length).join("0") + n;
		} else {
			return n;
		}
	};
	var txt_weekdays = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"];
	var txt_ordin = {1:"st", 2:"nd", 3:"rd", 21:"st", 22:"nd", 23:"rd", 31:"st"};
	var txt_months = ["", "January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
var f = {
		d: function(){return pad(f.j(), 2)},
		D: function(){return f.l().substr(0,3)},
		j: function(){return jsdate.getDate()},
		l: function(){return txt_weekdays[f.w()]},
		N: function(){return f.w() + 1},
		S: function(){return txt_ordin[f.j()] ? txt_ordin[f.j()] : 'th'},
		w: function(){return jsdate.getDay()},
		z: function(){return (jsdate - new Date(jsdate.getFullYear() + "/1/1")) / 864e5 >> 0},
		W: function(){
			var a = f.z(), b = 364 + f.L() - a;
			var nd2, nd = (new Date(jsdate.getFullYear() + "/1/1").getDay() || 7) - 1;
			if(b <= 2 && ((jsdate.getDay() || 7) - 1) <= 2 - b){
				return 1;
			} else{
				if(a <= 2 && nd >= 4 && a >= (6 - nd)){
					nd2 = new Date(jsdate.getFullYear() - 1 + "/12/31");
					return date("W", Math.round(nd2.getTime()/1000));
				} else{
					return (1 + (nd <= 3 ? ((a + nd) / 7) : (a - (7 - nd)) / 7) >> 0);
				}
			}
		},
		F: function(){return txt_months[f.n()]},
		m: function(){return pad(f.n(), 2)},
		M: function(){return f.F().substr(0,3)},
		n: function(){return jsdate.getMonth() + 1},
		t: function(){
			var n;
			if( (n = jsdate.getMonth() + 1) == 2 ){
				return 28 + f.L();
			} else{
				if( n & 1 && n < 8 || !(n & 1) && n > 7 ){
					return 31;
				} else{
					return 30;
				}
			}
		},
		L: function(){var y = f.Y();return (!(y & 3) && (y % 1e2 || !(y % 4e2))) ? 1 : 0},
		Y: function(){return jsdate.getFullYear()},
		y: function(){return (jsdate.getFullYear() + "").slice(2)},
		a: function(){return jsdate.getHours() > 11 ? "pm" : "am"},
		A: function(){return f.a().toUpperCase()},
		B: function(){
			var off = (jsdate.getTimezoneOffset() + 60)*60;
			var theSeconds = (jsdate.getHours() * 3600) + (jsdate.getMinutes() * 60) + jsdate.getSeconds() + off;
			var beat = Math.floor(theSeconds/86.4);
			if (beat > 1000) beat -= 1000;
			if (beat < 0) beat += 1000;
			if ((String(beat)).length == 1) beat = "00"+beat;
			if ((String(beat)).length == 2) beat = "0"+beat;
			return beat;
		},
		g: function(){return jsdate.getHours() % 12 || 12},
		G: function(){return jsdate.getHours()},
		h: function(){return pad(f.g(), 2)},
		H: function(){return pad(jsdate.getHours(), 2)},
		i: function(){return pad(jsdate.getMinutes(), 2)},
		s: function(){return pad(jsdate.getSeconds(), 2)},
		O: function(){
			var t = pad(Math.abs(jsdate.getTimezoneOffset()/60*100), 4);
			if (jsdate.getTimezoneOffset() > 0) t = "-" + t; else t = "+" + t;
			return t;
		},
		P: function(){var O = f.O();return (O.substr(0, 3) + ":" + O.substr(3, 2))},
		c: function(){return f.Y() + "-" + f.m() + "-" + f.d() + "T" + f.h() + ":" + f.i() + ":" + f.s() + f.P()},
		U: function(){return Math.round(jsdate.getTime()/1000)}
	};
		
	return format.replace(/[\\]?([a-zA-Z])/g, function(t, s){
		if( t!=s ){
			// escaped
			ret = s;
		} else if( f[s] ){
			// a date function exists
			ret = f[s]();
		} else{
			// nothing special
			ret = s;
		}
		return ret;
	});
//	date('Y-m-d',value);//很方便的将时间戳转换成了2012-10-11 
	//date('Y-m-d H:i:s','1350052653');//得到的结果是2012-10-12 22:37:33
}

function leRefresh(){
	$('table[leType=leTable]').each(function(){
		var _id = $(this).attr('id');
		var _this=$("#"+_id);
		leTableData(lePagNumber,_this.attr('lePageSize'),_id);
	});
}