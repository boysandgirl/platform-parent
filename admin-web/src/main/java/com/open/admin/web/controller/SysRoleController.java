package com.open.admin.web.controller;

import com.baomidou.kisso.annotation.Action;
import com.baomidou.kisso.annotation.Permission;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.open.admin.web.comm.BaseAdminController;
import com.open.admin.web.comm.page.Page;
import com.open.admin.web.comm.page.PageParameter;
import com.open.admin.web.comm.pojo.Message;
import com.open.comm.enumucation.RoleEnum;
import com.open.system.domain.role.SysRoleBean;
import com.open.system.request.RoleAuthorizeRequest;
import com.open.system.service.api.ISysRoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

/**
 * 系统角色 接口
 *
 * @date: 2017-06-13 18:31
 * @author: hlc
 */
@Controller
@RequestMapping("/admin/sys/role")
public class SysRoleController extends BaseAdminController {

	@Autowired
	private ISysRoleService roleService;

	@Permission(action = Action.Skip)
	@RequestMapping(value = "/toList", method = RequestMethod.GET)
	public String toList() {
		return "ftl/role/list";
	}

	@Permission("system:role:query")
	@ResponseBody
	@RequestMapping(value = "/list", method = RequestMethod.POST)
	public Page listData(PageParameter pageable) {
		return transformPage(roleService.selectPage(getMybatisPage(pageable)), pageable);
	}

	@Permission(action = Action.Skip)
	@RequestMapping(value = "/toAdd", method = RequestMethod.GET)
	public String toAdd() {
		return "ftl/role/add";
	}

	@Permission("system:role:add")
	@RequestMapping(value = "/add", method = RequestMethod.POST)
	@ResponseBody
	public Message add(SysRoleBean roleBean) {
		if (StringUtils.isEmpty(roleBean.getName())) {
			return Message.error("角色名称不能为空");
		}
		if (StringUtils.isEmpty(roleBean.getType())) {
			return Message.error("角色类型不能为空");
		}
		if (!RoleEnum.checkRole(roleBean.getType())) {
			return Message.error("角色类型不存在");
		}
		int count = roleService.selectCount(new EntityWrapper<SysRoleBean>().eq("name", roleBean.getName()).eq("type", roleBean.getType()));
		if (count > 0) {
			return Message.error("当前角色+角色类型已经存在");
		}
		roleService.insert(roleBean);
		return Message.success("添加成功");
	}

	@Permission(action = Action.Skip)
	@RequestMapping(value = "/toEdit", method = RequestMethod.GET)
	public String toEdit(Long id, ModelMap modelMap) {
		SysRoleBean roleBean = roleService.selectById(id);
		modelMap.addAttribute("role", roleBean);
		return "ftl/role/edit";
	}

	@Permission("system:role:update")
	@RequestMapping(value = "/edit", method = RequestMethod.POST)
	@ResponseBody
	public Message edit(SysRoleBean roleBean) {
		if (roleBean.getId() == null || roleBean.getId() <= 0) {
			return Message.error("角色id不能为空");
		}
		if (StringUtils.isEmpty(roleBean.getName())) {
			return Message.error("角色名称不能为空");
		}
		if (StringUtils.isEmpty(roleBean.getType())) {
			return Message.error("角色类型不能为空");
		}
		if (!RoleEnum.checkRole(roleBean.getType())) {
			return Message.error("角色类型不存在");
		}
		SysRoleBean old = roleService.selectById(roleBean.getId());
		if (old == null) {
			return Message.error("角色不存在");
		}
		boolean isUpdate = (old.getName().equals(roleBean.getName()) && old.getType().equals(roleBean.getType()));
		if (!isUpdate) {
			int count = roleService.selectCount(new EntityWrapper<SysRoleBean>().eq("name", roleBean.getName()).eq("type", roleBean.getType()));
			if (count > 0) {
				return Message.error("当前角色+角色类型已经存在");
			}
		}
		roleService.updateById(roleBean);
		return Message.success("修改成功");
	}

	//禁用 启用
	@Permission("system:role:update")
	@RequestMapping(value = "/forbid", method = RequestMethod.POST)
	@ResponseBody
	public Message forbid(Long id, Integer delStatus) {
		SysRoleBean role = roleService.selectById(id);
		if (role != null) {
			if (delStatus != null && (delStatus == 1 || delStatus == 0)) {
				role.setDelStatus(delStatus);
				roleService.updateById(role);
				return Message.success("成功");
			} else {
				return Message.error("输入参数有误");
			}
		} else {
			return Message.error("该角色不存在");
		}
	}

	@Permission(action = Action.Skip)
	@ResponseBody
	@RequestMapping(value = "/findRoles", method = RequestMethod.POST)
	public ResponseEntity findRoles() {

		List<SysRoleBean> roleBeanList = roleService.selectList(new EntityWrapper<SysRoleBean>().eq("del_status", 1));

		return ResponseEntity.ok(roleBeanList);
	}


	/**
	 * 角色授权
	 *
	 * @param authorizeRequest
	 * @return
	 */
	@Permission("authorize:role")
	@RequestMapping(value = "/authorizeRole", method = RequestMethod.POST)
	@ResponseBody
	public Message authorizeRole(RoleAuthorizeRequest authorizeRequest) {
		roleService.addResesByRoleId(authorizeRequest.getResIds(), authorizeRequest.getRoleId());
		return Message.success("授权成功");
	}
}
