package com.open.admin.web.request;

import com.open.comm.utils.page.BaseRequest;

import java.io.Serializable;

/**
 * 登入请求参数
 *
 * @date: 2017-06-22 21:58
 * @author: xfz
 */
public class LoginRequest extends BaseRequest implements Serializable {

	private String accountName;

	private String password;

	private String validateCode;

	public String getAccountName() {
		return accountName;
	}

	public void setAccountName(String accountName) {
		this.accountName = accountName;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getValidateCode() {
		return validateCode;
	}

	public void setValidateCode(String validateCode) {
		this.validateCode = validateCode;
	}
}
