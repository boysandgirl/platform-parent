package com.open.admin.web.controller;


import com.baomidou.kisso.annotation.Action;
import com.baomidou.kisso.annotation.Permission;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.mapper.Wrapper;
import com.open.admin.web.comm.BaseAdminController;
import com.open.admin.web.comm.page.PageParameter;
import com.open.admin.web.comm.pojo.Message;
import com.open.comm.utils.page.BasePageRequest;
import com.open.system.domain.zone.SysOpenZoneBean;
import com.open.system.response.OpenZoneResponse;
import com.open.system.response.ZoneResponse;
import com.open.system.service.api.ISysOpenZoneService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import java.util.List;

/**
 * 开放城市和城市字典列表
 *
 * @date: 2017-06-10 21:06
 * @author: xfz
 */

@Controller
@RequestMapping("/admin/sys/zone")
public class ZoneController extends BaseAdminController {

	@Autowired
	private ISysOpenZoneService zoneService;


	@Autowired
	private ISysOpenZoneService openZoneService;

	@Permission(action = Action.Skip)
	@RequestMapping(value = "/findAllProvinces", method = RequestMethod.GET)
	public ResponseEntity<List<ZoneResponse>> findAllProvinces() {
		List<ZoneResponse> zoneResponseList = zoneService.findAllProvinces();
		return new ResponseEntity(zoneResponseList, HttpStatus.OK);
	}

	@Permission(action = Action.Skip)
	@ResponseBody
	@RequestMapping(value = "/findAllCitiesByProvinceCode", method = RequestMethod.POST)
	public ResponseEntity<List<ZoneResponse>> findAllCitiesByProvinceCode(@RequestParam(value = "provinceCode", required = true) String provinceCode) {
		List<ZoneResponse> zoneResponseList = zoneService.findAllCitiesByParentZoneCode(provinceCode);
		return ResponseEntity.ok(zoneResponseList);

	}

	@Permission(action = Action.Skip)
	@ResponseBody
	@RequestMapping(value = "/findAllAreasByCitCode", method = RequestMethod.POST)
	public ResponseEntity<List<ZoneResponse>> findAllAreasByCitCode(@RequestParam("cityCode") String cityCode) {
		List<ZoneResponse> zoneResponseList = zoneService.findAllAreasByParentZoneCode(cityCode);
		return ResponseEntity.ok(zoneResponseList);
	}


	@Permission(action = Action.Skip)
	@RequestMapping(value = "/addOrUpdateOpenZone", method = RequestMethod.POST)
	@ResponseBody
	public Message addOrUpdateOpenZone(SysOpenZoneBean openZoneBean) {

		if (StringUtils.isEmpty(openZoneBean.getProvinceCode())) {
			return Message.error("所属省份不能为空");
		}
		if (StringUtils.isEmpty(openZoneBean.getCityCode())) {
			return Message.error("所属城市不能为空");
		}
		boolean isExist = openZoneService.isExistOpenZone(openZoneBean.getProvinceCode(), openZoneBean.getCityCode());
		if (!isExist) {
			openZoneService.addOpenZone(openZoneBean);
		} else {
			return Message.error("开放城市已经存在");
		}
		return Message.success("成功");
	}

	/**
	 * 查看开放城市 集合
	 *
	 * @return
	 */
	@Permission(action = Action.Skip)
	@RequestMapping(value = "/findOpenZoneList", method = RequestMethod.GET)
	public ResponseEntity findOpenZoneList() {
		return ResponseEntity.ok(openZoneService.findList());
	}

	@Permission(action = Action.Skip)
	@RequestMapping(value = "/toList", method = RequestMethod.GET)
	public String toOpenZoneList() {
		return "ftl/open/list";
	}

	@Permission(action = Action.Skip)
	@RequestMapping(value = "/listOpenZoneData", method = RequestMethod.POST)
	public ResponseEntity listOpenZoneData(PageParameter pageable) {
		BasePageRequest pageRequest = new BasePageRequest();
		pageRequest.setPageSize(pageable.getPageSize());
		pageRequest.setPageNum(pageable.getPageNumber());
		return ResponseEntity.ok(transformPage(openZoneService.findPage(pageRequest), pageable));
	}

	//跳到 新增或修改页面
	@Permission(action = Action.Skip)
	@RequestMapping(value = "/toAddOrUpdateOpenZone", method = RequestMethod.GET)
	public ModelAndView toAddOrUpdateOpenZone(@RequestParam(required = false, name = "openZoneId") Long openZoneId) {
		ModelAndView mv = new ModelAndView();
		if (openZoneId != null) {
			OpenZoneResponse response = openZoneService.findByOpenZoneId(openZoneId);
			if (response != null) {
				mv.addObject("openZone", response);
				mv.setViewName("ftl/open/edit");
				return mv;
			}
		}
		mv.setViewName("ftl/open/add");
		return mv;
	}

	//禁用 启用 对应的 开放城市
	@Permission(action = Action.Skip)
	@RequestMapping(value = "/forbidOpenZone", method = RequestMethod.POST)
	@ResponseBody
	public Message forbidOpenZone(Long openZoneId, Integer delStatus) {
		SysOpenZoneBean response = openZoneService.getById(openZoneId);
		if (response != null) {
			if (delStatus != null && (delStatus == 1 || delStatus == 0)) {
				response.setDelStatus(delStatus);
				openZoneService.updateOpenZone(response);
				return Message.success("成功");
			} else {
				return Message.error("输入参数有误");
			}
		} else {
			return Message.error("该开放城市不存在");
		}

	}


	@Permission(action = Action.Skip)
	@RequestMapping(value = "/findAreasByOpenZoneId", method = RequestMethod.POST)
	public ResponseEntity findAreasByOpenZoneId(Long openZoneId) {
		OpenZoneResponse openZoneBean = openZoneService.findByOpenZoneId(openZoneId);
		if (openZoneBean != null) {
			List<ZoneResponse> zoneResponseList = zoneService.findAllAreasByParentZoneCode(openZoneBean.getCityCode());
			return ResponseEntity.ok(zoneResponseList);
		}
		return ResponseEntity.ok().build();
	}


	/**
	 * 获取下拉值
	 *
	 * @return
	 */
	@Permission(action = Action.Skip)
	@RequestMapping(value = "/findOpenZoneForIdAndName", method = RequestMethod.GET)
	public ResponseEntity findOpenZoneForIdAndName() {
		Wrapper<SysOpenZoneBean> wapper = new EntityWrapper<SysOpenZoneBean>();
		// delStatus = 1
		wapper.setSqlSelect("id, title as name");
		wapper.where("del_status=1");
		return ResponseEntity.ok(openZoneService.selectMaps(wapper));
	}

}