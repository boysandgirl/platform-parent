package com.open.admin.web.comm.config;

import com.baomidou.kisso.SSOConfig;
import com.baomidou.kisso.exception.KissoException;

import javax.servlet.ServletContext;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.Properties;
import java.util.logging.Logger;

public class WebKissoConfigurer extends SSOConfig {
    protected static final Logger logger = Logger.getLogger("com.abc.admin.web.comm.config.WebKissoConfigurer");
    public static final String CONFIG_LOCATION_PARAM = "kissoConfigLocation";
    private String ssoPropPath = "sso.properties";


    private String ssoSecretkey;


    private String ssoCookieName;


    private String ssoCookieDomain;


    private String ssoLoginUrl;


    public WebKissoConfigurer() {
    }

    public WebKissoConfigurer(String ssoPropPath) {
        this.ssoPropPath = ssoPropPath;
    }

    public void initKisso(ServletContext servletContext) {
        String location = servletContext.getInitParameter("kissoConfigLocation");
        if(location != null) {
            if(location.indexOf("classpath") >= 0) {
                String[] cfg = location.split(":");
                if(cfg.length == 2) {
                    this.initProperties(this.getInputStream(cfg[1]));
                }
            } else {
                File file = new File(location);
                if(!file.isFile()) {
                    throw new KissoException(location);
                }

                try {
                    this.initProperties(this.getInputStream((InputStream)(new FileInputStream(file))));
                } catch (FileNotFoundException var5) {
                    throw new KissoException(location, var5);
                }
            }
        } else {
            servletContext.log("Initializing is not available kissoConfigLocation on the classpath");
        }

    }

    public void initKisso() {
        Properties prop =new Properties();

        prop.setProperty("sso.secretkey",ssoSecretkey);
        prop.setProperty("sso.cookie.name",ssoCookieName);
        prop.setProperty("sso.cookie.domain",ssoCookieDomain);
        prop.setProperty("sso.login.url",ssoLoginUrl);

        if(prop != null) {
            this.initProperties(prop);
        } else {
            logger.severe("Initializing is not available kissoConfigLocation on the classpath");
        }

    }

    public void shutdownKisso() {
        logger.info("Uninstalling Kisso ");
    }

    private Properties getInputStream(String cfg) {
        return this.getInputStream(WebKissoConfigurer.class.getClassLoader().getResourceAsStream(cfg));
    }

    private Properties getInputStream(InputStream in) {
        Properties p = null;

        try {
            p = new Properties();
            p.load(in);
        } catch (Exception var4) {
            logger.severe(" kisso read config file error. \n" + var4.toString());
        }

        return p;
    }

    public String getSsoPropPath() {
        return this.ssoPropPath;
    }


    public String getSsoSecretkey() {
        return ssoSecretkey;
    }

    public void setSsoSecretkey(String ssoSecretkey) {
        this.ssoSecretkey = ssoSecretkey;
    }

    public String getSsoCookieName() {
        return ssoCookieName;
    }

    public void setSsoCookieName(String ssoCookieName) {
        this.ssoCookieName = ssoCookieName;
    }

    public String getSsoCookieDomain() {
        return ssoCookieDomain;
    }

    public void setSsoCookieDomain(String ssoCookieDomain) {
        this.ssoCookieDomain = ssoCookieDomain;
    }

    public String getSsoLoginUrl() {
        return ssoLoginUrl;
    }

    public void setSsoLoginUrl(String ssoLoginUrl) {
        this.ssoLoginUrl = ssoLoginUrl;
    }

    public void setSsoPropPath(String ssoPropPath) {
        this.ssoPropPath = ssoPropPath;
    }
}
