package com.open.admin.web.controller;


import com.baomidou.kisso.annotation.Action;
import com.baomidou.kisso.annotation.Permission;
import com.baomidou.mybatisplus.plugins.Page;
import com.open.admin.web.comm.BaseAdminController;
import com.open.admin.web.comm.page.PageParameter;
import com.open.admin.web.comm.pojo.Message;
import com.open.comm.utils.page.BasePageRequest;
import com.open.system.domain.data.SysDataResourceBean;
import com.open.system.domain.data.SysDataTypeBean;
import com.open.system.response.DataCommResponse;
import com.open.system.service.api.ISysDataService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import java.util.List;


/**
 * 数据字典管理
 *
 * @date: 2017-06-11 1:30
 * @author: xfz
 */
@Controller
@RequestMapping("/admin/sys/data")
public class SysDataController extends BaseAdminController {


	@Autowired
	private ISysDataService sysDataService;

	/**************************************数据类型***********************************************************/

	@Permission(action = Action.Skip)
	@RequestMapping(value = "/toListType", method = RequestMethod.GET)
	public String toListType() {

		return "ftl/data/listType";
	}

	@Permission(action = Action.Skip)
	@RequestMapping(value = "/listType", method = RequestMethod.POST)
	public ResponseEntity listDataType(PageParameter pageable) {
		BasePageRequest pageRequest = new BasePageRequest();
		pageRequest.setPageNum(pageable.getPageNumber());
		pageRequest.setPageSize(pageable.getPageSize());
		Page<SysDataTypeBean> sysDataTypeBeanPage = sysDataService.selectDataTypePage(pageRequest);
		return ResponseEntity.ok(transformPage(sysDataTypeBeanPage, pageable));
	}

	@Permission(action = Action.Skip)
	@RequestMapping(value = "/findParentDataType", method = RequestMethod.POST)
	public ResponseEntity findParentDataType() {

		List<SysDataTypeBean> sysDataTypeBeanPage = sysDataService.findParentDataTypes(null);
		return ResponseEntity.ok(sysDataTypeBeanPage);
	}

	@Permission(action = Action.Skip)
	@RequestMapping(value = "/toAddOrUpdateType", method = RequestMethod.GET)
	public ModelAndView toAddType(@RequestParam(name = "id", required = false) Long id) {
		ModelAndView mv = new ModelAndView();
		if (id != null && id > 0) {
			SysDataTypeBean dataTypeBean = sysDataService.selectDataTypeById(id);
			if (dataTypeBean != null) {
				mv.addObject("dataType", dataTypeBean);

				mv.setViewName("ftl/data/editType");
				return mv;
			}
		}
		mv.setViewName("ftl/data/addType");
		return mv;
	}

	@Permission(action = Action.Skip)
	@RequestMapping(value = "/addOrUpdateDataTyp", method = RequestMethod.POST)
	@ResponseBody
	public Message addOrUpdateDataTyp(SysDataTypeBean dataTypeBean) {
		String message = "";
		try {
			if (dataTypeBean.getId() != null && dataTypeBean.getId() > 0) {
				sysDataService.updateDataType(dataTypeBean);
				message = "修改成功";
			} else {
				sysDataService.addSysDataType(dataTypeBean);
				message = "添加成功";
			}
		} catch (Exception e) {
			message = "失败";
		}
		return Message.success(message);
	}

	@Permission(action = Action.Skip)
	@RequestMapping(value = "/forbidDataType", method = RequestMethod.POST)
	@ResponseBody
	public Message forbidDataType(Long id, Integer delStatus) {

		SysDataTypeBean dataTypeBean = sysDataService.selectDataTypeById(id);
		if (dataTypeBean != null) {
			if (delStatus != null) {
				if (delStatus.compareTo(0) == 0) {
					//禁用
					dataTypeBean.setDelStatus(0);
				} else if (delStatus.compareTo(1) == 0) {
					//启用
					dataTypeBean.setDelStatus(1);
				}
				sysDataService.updateDataType(dataTypeBean);
			}
		} else {
			return Message.error("改数据类型不存在");
		}
		return Message.success("禁用成功");
	}

	@Permission(action = Action.Skip)
	@RequestMapping(value = "/deleteDataType", method = RequestMethod.POST)
	@ResponseBody
	public Message deleteDataType(Long id) {
		sysDataService.deleteDataType(id);
		return Message.success("删除成功");
	}
	/**************************************数据类型***********************************************************/

	/*******************************resource*************************************/
	@Permission(action = Action.Skip)
	@RequestMapping(value = "/toList", method = RequestMethod.GET)
	public String toList() {
		return "ftl/data/list";
	}

	@Permission(action = Action.Skip)
	@ResponseBody
	@RequestMapping(value = "/listData", method = RequestMethod.POST)
	public ResponseEntity listData(@RequestParam(required = false) Long dataTypes, PageParameter pageable) {
		BasePageRequest pageRequest = new BasePageRequest();
		pageRequest.setPageNum(pageable.getPageNumber());
		pageRequest.setPageSize(pageable.getPageSize());
		Page sysDataTypeBeanPage = sysDataService.selectDataResourcePage(pageRequest, dataTypes);

		return ResponseEntity.ok(transformPage(sysDataTypeBeanPage, pageable));

	}

	@Permission(action = Action.Skip)
	@RequestMapping(value = "/toAdd", method = RequestMethod.GET)
	public String toAdd() {
		return "ftl/data/add";
	}

	@Permission(action = Action.Skip)
	@RequestMapping(value = "/toEdit", method = RequestMethod.GET)
	public ModelAndView toEdit(@RequestParam(name = "id", required = true) Long id) {
		ModelAndView mv = new ModelAndView();
		SysDataResourceBean resourceBean = sysDataService.selectDataResourceById(id);
		mv.addObject("dataResource", resourceBean);
		mv.setViewName("ftl/data/edit");
		return mv;
	}

	@Permission(action = Action.Skip)
	@RequestMapping(value = "/addOrUpdateDataResource", method = RequestMethod.POST)
	@ResponseBody
	public Message addOrUpdateDataResource(SysDataResourceBean resourceBean) {

		if (resourceBean.getDataTypeId() == null) {
			return Message.error("字典类型不能为空");
		}

		if (StringUtils.isEmpty(resourceBean.getDataCode())) {
			return Message.error("字典唯一标识不能为空");
		}
		String message = "";
		try {
			if (resourceBean.getId() != null && resourceBean.getId() > 0) {

				sysDataService.updateDataResource(resourceBean);
				message = "修改成功";
			} else {
				boolean isExist = sysDataService.isExistDataResourceCode(resourceBean.getDataTypeId(), resourceBean.getDataCode());
				if (isExist) {
					return Message.error("字典唯一标识已经存在,请重新输入");
				}
				sysDataService.addSysDataResource(resourceBean);
				message = "添加成功";

			}
		} catch (Exception e) {
			message = "失败";
		}
		return Message.success(message);
	}

	@Permission(action = Action.Skip)
	@RequestMapping(value = "/forbidDataResource", method = RequestMethod.POST)
	@ResponseBody
	public Message forbidDataResource(Long id, Integer delStatus) {

		SysDataResourceBean dataResourceBean = sysDataService.selectDataResourceById(id);
		if (dataResourceBean != null) {
			if (delStatus != null) {
				if (delStatus.compareTo(0) == 0) {
					//禁用
					dataResourceBean.setDelStatus(0);
				} else if (delStatus.compareTo(1) == 0) {
					//启用
					dataResourceBean.setDelStatus(1);
				}
				sysDataService.updateDataResource(dataResourceBean);
			}
		} else {
			return Message.error("改数据字典不存在");
		}
		return Message.success("禁用成功");
	}

	@Permission(action = Action.Skip)
	@RequestMapping(value = "/deleteDataResource", method = RequestMethod.POST)
	@ResponseBody
	public Message deleteDataResource(Long id) {
		sysDataService.deleteDataResource(id);
		return Message.success("删除成功");
	}

	/************************************* 获取普通字典接口**************************************************************************************************************/
	@Permission(action = Action.Skip)
	@RequestMapping(value = "/findCommDatas", method = RequestMethod.POST)
	public ResponseEntity<List<DataCommResponse>> findCommDatas(Long dataTypeId) {
		List<DataCommResponse> commResponses = null;
		commResponses = sysDataService.findCommDatas(dataTypeId);
		return ResponseEntity.ok(commResponses);
	}
}
