package com.open.admin.web.comm.config;

import com.xxl.job.core.executor.XxlJobExecutor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;


@Configuration
@PropertySource(value = "classpath:/xxl-job-executor.properties")
public class XxlJobConfig {
	@Value("${xxl.job.executor.ip}")
	private String ip;
	@Value("${xxl.job.executor.port}")
	private int port;
	@Value("${xxl.job.admin.addresses}")
	private String adminAddresses;
	@Value("${xxl.job.executor.appname}")
	private String appName;
	@Value("${xxl.job.executor.logpath}")
	private String logPath;


	@Bean(initMethod = "start", destroyMethod = "destroy")
	public XxlJobExecutor xxlJobExecutor() {
		XxlJobExecutor executor = new XxlJobExecutor();
		executor.setIp(ip);
		executor.setPort(port);
		executor.setAdminAddresses(adminAddresses);
		executor.setAppName(appName);
		executor.setLogPath(logPath);
		return executor;
	}
}