package com.open.admin.web.controller;


import com.baomidou.kisso.SSOConfig;
import com.baomidou.kisso.SSOHelper;
import com.baomidou.kisso.SSOToken;
import com.baomidou.kisso.annotation.Action;
import com.baomidou.kisso.annotation.Login;
import com.baomidou.kisso.web.waf.request.WafRequestWrapper;
import com.open.admin.web.comm.BaseAdminController;
import com.open.admin.web.request.LoginRequest;
import com.open.admin.web.service.SysUserServiceApi;
import com.open.admin.web.utils.CaptchaUtil;
import com.open.comm.utils.exception.LcException;
import com.open.system.domain.user.SysEmployeeBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.io.IOException;

/**
 * Created by xfz on 2017-4-9.
 */

@Controller
public class SSOController extends BaseAdminController {


	@Autowired
	private SysUserServiceApi userServiceApi;

	/**
	 * 登录 （注解跳过权限验证）
	 */
	@Login(action = Action.Skip)
	@RequestMapping("/login")
	public String login() {
		SSOToken st = SSOHelper.getToken(getRequest());
		if (st != null) {
			return redirectTo("/admin/main/toIndex");
		}
		return "/login";
	}

	/**
	 * 登录 （注解跳过权限验证）
	 */
	@Login(action = Action.Skip)
	@RequestMapping("/loginpost")
	public String loginpost(ModelMap modelMap) {
		/**
		 * 生产环境需要过滤sql注入
		 */
		WafRequestWrapper req = new WafRequestWrapper(getRequest());
		String username = req.getParameter("username");
		String password = req.getParameter("password");
		String vcode = req.getParameter("vcode");

		LoginRequest loginRequest = new LoginRequest();

		loginRequest.setAccountName(username);
		loginRequest.setPassword(password);
		SysEmployeeBean employeeBean = null;
		try {
			if (StringUtils.isEmpty(vcode)) {
				throw new LcException("验证码不能为空");
			}
			String validateCode = (String) getRequest().getSession().getAttribute("code");
			if (!validateCode.equalsIgnoreCase(vcode)) {
				throw new LcException("验证码错误");
			}
			employeeBean = userServiceApi.login(loginRequest);
			SSOToken st = new SSOToken(getRequest());
			st.setId(employeeBean.getId());
			st.setUid(employeeBean.getId() + "");
			st.setType(1);

			//记住密码，设置 cookie 时长 1 周 = 604800 秒 【动态设置 maxAge 实现记住密码功能】
			String rememberMe = req.getParameter("rememberMe");
			if ("on".equals(rememberMe)) {
				getRequest().setAttribute(SSOConfig.SSO_COOKIE_MAXAGE, 604800);
			}
			SSOHelper.setSSOCookie(getRequest(), getResponse(), st, true);

			/*
			 * 登录需要跳转登录前页面，自己处理 ReturnURL 使用
			 * HttpUtil.decodeURL(xx) 解码后重定向
			 */
			return redirectTo("/admin/main/toIndex");
		} catch (LcException e) {
			modelMap.addAttribute("errorMessage", e.getMessage());
		} catch (Exception e) {
			modelMap.addAttribute("errorMessage", "系统异常,稍后重试");
			logger.info(e.getMessage());
		}
		return "/login";
	}

	/**
	 * 退出登录
	 */
	@RequestMapping("/logout")
	public String logout() {
		/**
		 * <p>
		 * SSO 退出，清空退出状态即可
		 * </p>
		 *
		 * <p>
		 * 子系统退出 SSOHelper.logout(request, response); 注意 sso.properties 包含 退出到
		 * SSO 的地址 ， 属性 sso.logout.url 的配置
		 * </p>
		 */
		SSOHelper.clearLogin(getRequest(), getResponse());
		return redirectTo("/login");
	}

	/**
	 * 验证码 （注解跳过权限验证）
	 */
	@Login(action = Action.Skip)
	@ResponseBody
	@RequestMapping("/verify")
	public void verify() {
		try {
			String verifyCode = CaptchaUtil.outputImage(getResponse().getOutputStream());
			getRequest().getSession().setAttribute("code", verifyCode);
			System.out.println("验证码:" + verifyCode);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * 异常 404 提示页
	 */
	@RequestMapping("/404")
	public String error_404() {
		return "ftl/404";
	}
}
