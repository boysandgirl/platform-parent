/**
 * Copyright (c) 2011-2014, hubin (243194995@qq.com).
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package com.open.admin.web.aop;


import com.baomidou.kisso.SSOAuthorization;
import com.baomidou.kisso.Token;
import com.open.system.domain.role.SysRoleBean;
import com.open.system.domain.user.SysEmployeeBean;
import com.open.system.response.ResourceInfoResponse;
import com.open.system.service.api.ISysEmployeeService;
import com.open.system.service.api.ISysResourcesService;
import com.open.system.service.api.ISysRoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

/**
 * 系统权限授权实现类
 */
@Component
public class SysAuthorization implements SSOAuthorization {

	@Autowired
	private ISysEmployeeService employeeService;
	@Autowired
	private ISysRoleService roleService;
	@Autowired
	private ISysResourcesService resourcesService;

	/**
	 * 开启配置 sso.permission.uri=true 支持、先验证 url 地址，后验证注解。
	 */
	public boolean isPermitted(Token token, String permission) {
		/**
		 * 循环判断权限编码是否合法，token 获取登录用户ID信息、判断相应权限也可作为缓存主键使用。
		 */
		Long userId = token.getId();
		SysEmployeeBean employeeBean = employeeService.selectById(userId);
		if (employeeBean != null) {
			if (employeeBean.getDelStatus() == 0) {
				//倍禁用
				return false;
			}
			List<ResourceInfoResponse> allResesList = new ArrayList<ResourceInfoResponse>();
			List<ResourceInfoResponse> responseList = null;
			List<SysRoleBean> roleBeanList = roleService.findRoleByUserId(employeeBean.getId());
			if (roleBeanList != null && roleBeanList.size() > 0) {
				responseList = new ArrayList<ResourceInfoResponse>();
				for (SysRoleBean roleBean : roleBeanList) {
					if (roleBean.getDelStatus() == 1) {
						responseList = resourcesService.findResesByRoleId(roleBean.getId());
						allResesList.addAll(responseList);

					}
				}
			}
			boolean isPermission = false;
			if (allResesList != null && allResesList.size() > 0) {
				for (ResourceInfoResponse response : allResesList) {
					if (response.getPermission().equals(permission)) {
						isPermission = true;
						break;
					}
				}
			}
			if (isPermission) {
				return true;
			}
		}
		return false;
	}

}
