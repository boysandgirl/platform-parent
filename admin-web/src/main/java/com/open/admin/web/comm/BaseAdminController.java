package com.open.admin.web.comm;


import com.baomidou.kisso.SSOHelper;
import com.baomidou.kisso.Token;
import com.open.admin.web.comm.page.Page;
import com.open.admin.web.comm.page.PageParameter;
import com.open.comm.utils.page.BasePageRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;
import java.util.Map;

/**
 * 后台管理基础controller
 *
 * @date: 2017-06-11 2:08
 * @author: hlc
 */
public class BaseAdminController {

	protected Logger logger = LoggerFactory.getLogger(this.getClass());

	protected HttpServletRequest getRequest() {
		return ((ServletRequestAttributes) RequestContextHolder
				.getRequestAttributes()).getRequest();
	}

	protected HttpServletResponse getResponse() {
		HttpServletResponse response = ((ServletRequestAttributes) RequestContextHolder
				.getRequestAttributes()).getResponse();
		return response;
	}

	public com.baomidou.mybatisplus.plugins.Page getMybatisPage(PageParameter pageable) {
		com.baomidou.mybatisplus.plugins.Page pa = new com.baomidou.mybatisplus.plugins.Page();

		pa.setCurrent(pageable.getPageNumber());
		pa.setSize(pageable.getPageSize());

		return pa;
	}

	public Page transformPage(com.baomidou.mybatisplus.plugins.Page k, PageParameter pageable) {

		List<Map> content = k.getRecords();
		Page page = new Page(content, k.getTotal(), pageable);

		return page;
	}

	public BasePageRequest getBasePage(PageParameter pageParameter) {
		BasePageRequest basePageRequest = new BasePageRequest();
		basePageRequest.setPageSize(pageParameter.getPageSize());
		basePageRequest.setPageNum(pageParameter.getPageNumber());
		return basePageRequest;
	}


	protected String redirectTo(String url) {
		StringBuffer rto = new StringBuffer("redirect:");
		rto.append(url);
		return rto.toString();
	}


	protected Token getToken() {
		return SSOHelper.getToken(getRequest());
	}
}
