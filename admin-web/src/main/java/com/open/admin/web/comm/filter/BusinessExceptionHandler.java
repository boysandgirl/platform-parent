package com.open.admin.web.comm.filter;

import com.open.comm.utils.exception.LcException;
import com.open.comm.utils.exception.ParameterException;
import com.open.comm.utils.exception.UnPermissionException;
import org.omg.CORBA.SystemException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import javax.servlet.http.HttpServletResponse;

//@ControllerAdvice
public class BusinessExceptionHandler extends HandlerInterceptorAdapter {

	Logger logger = LoggerFactory.getLogger(getClass());

	public void setHttpHeaders(HttpServletResponse response){
		response.setHeader("Access-Control-Allow-Origin", "*");
	}



	@ExceptionHandler(Exception.class)
	public ResponseEntity HandleException(HttpServletResponse response, Exception exception) {

		setHttpHeaders(response);
		logger.debug("ParameterException, message = " + exception.getMessage(), exception);

		return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(exception.getMessage());
	}


	@ExceptionHandler(LcException.class)
	public ResponseEntity HandleException(HttpServletResponse response, LcException exception) {
		setHttpHeaders(response);
		logger.debug("ParameterException, message = " + exception.getMessage(), exception);
		return  ResponseEntity.status(HttpStatus.SEE_OTHER).body(exception.getMessage());
	}

	@ExceptionHandler(ParameterException.class)
	public ResponseEntity HandleException(HttpServletResponse response, ParameterException exception) {
		setHttpHeaders(response);
		logger.debug("ParameterException, message = " + exception.getMessage(), exception);

		return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(exception.getMessage());
	}

	@ExceptionHandler(SystemException.class)
	public ResponseEntity HandleException(HttpServletResponse response, SystemException exception) {
		setHttpHeaders(response);
		logger.debug("SystemException, message = " + exception.getMessage(), exception);
		return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(exception.getMessage());
	}

	@ExceptionHandler(LcException.class)
	public ResponseEntity HandleException(HttpServletResponse response, UnPermissionException exception) {
		setHttpHeaders(response);
		logger.debug("UnPermissionException, message = " + exception.getMessage(), exception);
		return  ResponseEntity.status(HttpStatus.UNAUTHORIZED).body(exception.getMessage());
	}

}
