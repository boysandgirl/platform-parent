package com.open.admin.web.controller;

import com.baomidou.kisso.annotation.Action;
import com.baomidou.kisso.annotation.Permission;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * 后台总入口
 *
 * @date: 2017-06-10 21:36
 * @author: xfz
 */
@Controller
@RequestMapping("/admin/main")
public class MainController {
    @Permission(action = Action.Skip)
    @RequestMapping(value = "/toIndex", method = RequestMethod.GET)
    public String toIndex() {
        return "ftl/main/index";
    }
}
