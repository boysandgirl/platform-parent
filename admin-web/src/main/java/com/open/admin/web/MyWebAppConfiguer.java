package com.open.admin.web;

import com.baomidou.kisso.SSOAuthorization;
import com.baomidou.kisso.web.interceptor.SSOPermissionInterceptor;
import com.baomidou.kisso.web.interceptor.SSOSpringInterceptor;
import com.open.admin.web.comm.config.WebKissoConfigurer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

@Configuration
public class MyWebAppConfiguer extends WebMvcConfigurerAdapter {

	//--------------Kisso start--------------------------
	@Autowired
	private ApplicationContext context;

	@Value("${sso.secretkey}")
	private String ssoSecretkey;

	@Value("${sso.cookie.name}")
	private String ssoCookieName;

	@Value("${sso.cookie.domain}")
	private String ssoCookieDomain;

	@Value("${sso.login.url}")
	private String ssoLoginUrl;


	@Bean(initMethod = "initKisso")
	public WebKissoConfigurer kissoInit() {
		WebKissoConfigurer webKissoConfigurer = new WebKissoConfigurer();
		webKissoConfigurer.setSsoSecretkey(ssoSecretkey);
		webKissoConfigurer.setSsoCookieDomain(ssoCookieDomain);
		webKissoConfigurer.setSsoCookieName(ssoCookieName);
		webKissoConfigurer.setSsoLoginUrl(ssoLoginUrl);
		return webKissoConfigurer;
	}

	@Bean
	public SSOPermissionInterceptor getSSOPermissionInterceptor(SSOAuthorization authorization) {
		SSOPermissionInterceptor ssoPermissionInterceptor = new SSOPermissionInterceptor();
		ssoPermissionInterceptor.setIllegalUrl("/ftl/401");
		ssoPermissionInterceptor.setAuthorization(authorization);
		return ssoPermissionInterceptor;
	}
	//--------------Kisso end--------------------------


	@Override
	public void addInterceptors(InterceptorRegistry registry) {
		registry.addInterceptor(new SSOSpringInterceptor()).addPathPatterns("/**");
		registry.addInterceptor(context.getBean(SSOPermissionInterceptor.class)).addPathPatterns("/admin/**");
		super.addInterceptors(registry);
	}

	/**
	 * 静态资源过滤
	 */
	@Override
	public void addResourceHandlers(ResourceHandlerRegistry registry) {
		registry.addResourceHandler("/static/**").addResourceLocations("classpath:/static/");
	}

}