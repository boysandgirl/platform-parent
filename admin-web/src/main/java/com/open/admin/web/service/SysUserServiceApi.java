package com.open.admin.web.service;


import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.open.admin.web.request.LoginRequest;
import com.open.comm.utils.exception.LcException;
import com.open.system.domain.user.SysEmployeeBean;
import com.open.system.service.api.ISysEmployeeService;
import com.open.system.service.api.ISysRoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 系统用户服务模块
 *
 * @date: 2017-06-22 21:55
 * @author: xfz
 */
@Service
public class SysUserServiceApi {

	@Autowired
	private ISysEmployeeService sysEmployeeService;

	@Autowired
	private ISysRoleService sysRoleService;


	public SysEmployeeBean login(LoginRequest loginRequest) {

		List<SysEmployeeBean> employeeBeanList = sysEmployeeService.selectList(new EntityWrapper<SysEmployeeBean>().eq("account_name", loginRequest.getAccountName()));
		if (employeeBeanList != null && employeeBeanList.size() > 0) {
			SysEmployeeBean employeeBean = employeeBeanList.get(0);
			if (employeeBean.getDelStatus() == 0) {
				throw new LcException("用户被禁用,请联系管理员");
			}
			if (!employeeBean.getPassword().equals(loginRequest.getPassword())) {
				throw new LcException("账号或密码错误");
			}
			return employeeBean;

		} else {
			throw new LcException("账号或密码错误");
		}

	}
}
