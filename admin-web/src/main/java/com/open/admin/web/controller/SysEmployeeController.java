package com.open.admin.web.controller;


import com.baomidou.kisso.annotation.Action;
import com.baomidou.kisso.annotation.Permission;
import com.open.admin.web.comm.BaseAdminController;
import com.open.admin.web.comm.page.Page;
import com.open.admin.web.comm.page.PageParameter;
import com.open.admin.web.comm.pojo.Message;
import com.open.system.domain.role.SysRoleBean;
import com.open.system.domain.user.SysEmployeeBean;
import com.open.system.request.AuthorizeOpenZoneRequest;
import com.open.system.request.EmployeeRequest;
import com.open.system.service.api.ISysEmployeeService;
import com.open.system.service.api.ISysRoleService;
import org.apache.commons.beanutils.PropertyUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import java.util.List;

/**
 * 系统员工 接口
 *
 * @date: 2017-06-13 18:31
 * @author: hlc
 */
@Controller
@RequestMapping("/admin/sys/employee")
public class SysEmployeeController extends BaseAdminController {

	@Autowired
	private ISysEmployeeService sysEmployeeService;

	@Autowired
	private ISysRoleService sysRoleService;

	@Permission(action = Action.Skip)
	@RequestMapping(value = "/toList", method = RequestMethod.GET)
	public String toList() {
		return "ftl/employee/list";
	}

	@Permission("system:user:query")
	@ResponseBody
	@RequestMapping(value = "/list", method = RequestMethod.POST)
	public Page listData(PageParameter pageable) {

		return transformPage(sysEmployeeService.findPageEmployees(getBasePage(pageable)), pageable);
	}

	@Permission(action = Action.Skip)
	@RequestMapping(value = "/toAdd", method = RequestMethod.GET)
	public String toAdd() {
		return "ftl/employee/add";
	}

	@Permission("system:user:add")
	@RequestMapping(value = "/add", method = RequestMethod.POST)
	@ResponseBody
	public Message add(EmployeeRequest employeeRequest) {
		// TODO

		try {
			sysEmployeeService.addUser(employeeRequest);
		} catch (Exception e) {
			return Message.error("添加失败：" + e.getMessage());
		}
		return Message.success("添加成功");
	}

	@Permission(action = Action.Skip)
	@RequestMapping(value = "/toEdit", method = RequestMethod.GET)
	public String toEdit(Long id, ModelMap modelMap) {
		SysEmployeeBean em = sysEmployeeService.selectById(id);
		EmployeeRequest employeeRequest = null;
		if (em != null) {
			employeeRequest = new EmployeeRequest();
			try {
				PropertyUtils.copyProperties(employeeRequest, em);
			} catch (Exception e) {
				logger.error(e.getMessage(), e);
			}
			List<SysRoleBean> roleBeanList = sysRoleService.findRoleByUserId(id);
			StringBuilder sb = new StringBuilder();
			if (roleBeanList != null && roleBeanList.size() > 0) {
				for (SysRoleBean roleBean : roleBeanList) {

					sb.append(roleBean.getId() + "").append(",");
				}
				employeeRequest.setRoleIds(sb.toString().substring(0, sb.toString().length() - 1));
			}

		}
		modelMap.put("employee", employeeRequest);
		return "ftl/employee/edit";
	}

	@Permission("system:user:update")
	@RequestMapping(value = "/edit", method = RequestMethod.POST)
	@ResponseBody
	public Message edit(EmployeeRequest employeeRequest) {
		// TODO

		sysEmployeeService.updateUser(employeeRequest);
		return Message.success("修改成功");
	}

	//禁用 启用 对应的 开放城市
	@Permission("system:user:update")
	@RequestMapping(value = "/forbid", method = RequestMethod.POST)
	@ResponseBody
	public Message forbid(Long id, Integer delStatus) {
		SysEmployeeBean employeeBean = sysEmployeeService.selectById(id);
		if (employeeBean != null) {
			if (delStatus != null && (delStatus == 1 || delStatus == 0)) {
				employeeBean.setDelStatus(delStatus);
				sysEmployeeService.updateById(employeeBean);
				return Message.success("成功");
			} else {
				return Message.error("输入参数有误");
			}
		} else {
			return Message.error("该用户不存在");
		}

	}

	@Permission(action = Action.Skip)
	@RequestMapping(value = "/toAuthorizeOpenZone", method = RequestMethod.GET)
	public ModelAndView toAuthorizeOpenZone(@RequestParam(required = true) Long userId) {

		ModelAndView mv = new ModelAndView();
		mv.addObject("userId", userId);

		String openZoneIDS = sysEmployeeService.findOpenZonesByEmployeeId(userId);
		mv.addObject("openZoneIDS", openZoneIDS);

		mv.setViewName("ftl/employee/authorize");
		return mv;
	}

	@Permission("authorize:openzone")
	@RequestMapping(value = "/addAuthorizeOpenZone", method = RequestMethod.POST)
	@ResponseBody
	public Message addAuthorizeOpenZone(AuthorizeOpenZoneRequest openZoneRequest) {

		try {
			sysEmployeeService.addAuthorizeOpenZone(openZoneRequest);
		} catch (Exception e) {
			return Message.error(e.getMessage());
		}
		return Message.success("添加成功");
	}

}
