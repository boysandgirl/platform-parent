package com.open.admin.web.comm.pojo;

import java.io.BufferedOutputStream;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.UUID;

import org.springframework.web.multipart.MultipartFile;

import sun.misc.BASE64Decoder;

public class BASE64DecodedMultipartFile implements MultipartFile {
	private final byte[] imgContent;
	private final String originalFilename;
	private final String contentType;

	public BASE64DecodedMultipartFile(String imageData) throws IOException {
		BASE64Decoder base64Decoder = new BASE64Decoder();
		this.imgContent = base64Decoder.decodeBuffer(imageData.substring(imageData.indexOf(",") + 1));
		this.contentType = imageData.substring(imageData.indexOf(":") + 1, imageData.indexOf(";"));
		this.originalFilename = UUID.randomUUID() + "." + contentType.split("/")[1];
	}

	@Override
	public String getName() {
		// TODO - implementation depends on your requirements
		return null;
	}

	@Override
	public String getOriginalFilename() {
		return this.originalFilename;
	}

	@Override
	public String getContentType() {
		return this.contentType;
	}

	@Override
	public boolean isEmpty() {
		return imgContent == null || imgContent.length == 0;
	}

	@Override
	public long getSize() {
		return imgContent.length;
	}

	@Override
	public byte[] getBytes() throws IOException {
		return imgContent;
	}

	@Override
	public InputStream getInputStream() throws IOException {
		return new ByteArrayInputStream(imgContent);
	}

	@Override
	public void transferTo(File dest) throws IOException, IllegalStateException {
		BufferedOutputStream bos = new BufferedOutputStream(new FileOutputStream(dest));
		bos.write(imgContent);
		bos.flush();
		bos.close();
	}
}
