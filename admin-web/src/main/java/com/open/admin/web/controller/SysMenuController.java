package com.open.admin.web.controller;

import com.baomidou.kisso.annotation.Action;
import com.baomidou.kisso.annotation.Permission;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.open.admin.web.comm.BaseAdminController;
import com.open.admin.web.comm.page.Page;
import com.open.admin.web.comm.page.PageParameter;
import com.open.admin.web.comm.pojo.Message;
import com.open.system.domain.resource.SysResourcesBean;
import com.open.system.response.ResourceInfoResponse;
import com.open.system.service.api.ISysResourcesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import java.util.List;

/**
 * 系统资源 接口
 *
 * @date: 2017-06-13 18:31
 * @author: xfz
 */
@Controller
@RequestMapping("/admin/sys/resource")
public class SysMenuController extends BaseAdminController {

	@Autowired
	private ISysResourcesService resourcesService;

	@Permission(action = Action.Skip)
	@RequestMapping(value = "/toList", method = RequestMethod.GET)
	public String toList() {
		return "ftl/resource/list";
	}

	@Permission("system:resource:query")
	@ResponseBody
	@RequestMapping(value = "/list", method = RequestMethod.POST)
	public Page listData(PageParameter pageable) {

		return transformPage(resourcesService.selectPage(getMybatisPage(pageable)), pageable);
	}

	@Permission(action = Action.Skip)
	@RequestMapping(value = "/toAdd", method = RequestMethod.GET)
	public String toAdd() {
		return "ftl/resource/add";
	}

	@Permission("system:resource:add")
	@RequestMapping(value = "/add", method = RequestMethod.POST)
	@ResponseBody
	public Message add(SysResourcesBean resourcesBean) {

		if (resourcesBean.getType().compareTo(0) == 0) {//类型为目录
			if (resourcesBean.getParentId() != null && resourcesBean.getParentId() > 0) {
				return Message.error("为目录类型的节点不能有父级节点存在");
			}
		} else if (resourcesBean.getType().compareTo(1) == 0) {//类型为菜单
			if (resourcesBean.getParentId() != null && resourcesBean.getParentId() > 0) {
				//判断当前父节点是否为目录级别的节点 不是给予提示
				int isParentRoot = resourcesService.selectCount(new EntityWrapper<SysResourcesBean>().eq("id", resourcesBean.getParentId()).eq("type", 0));
				if (isParentRoot <= 0) {
					return Message.error("所选的父节点不是目录级别节点");
				}

			} else {
				//必须选择对应的 父节点目录
				return Message.error("	请选择对应的目录节点");
			}
		} else if (resourcesBean.getType().compareTo(2) == 0) {// 类型为按钮
			if (resourcesBean.getParentId() != null && resourcesBean.getParentId() > 0) {
				//上级节点必须为目菜单
				int isParentMeum = resourcesService.selectCount(new EntityWrapper<SysResourcesBean>().eq("id", resourcesBean.getParentId()).eq("type", 1));
				if (isParentMeum <= 0) {
					return Message.error("所选的父节点不是菜单级别节点");
				}
			} else {
				//必须选择对应的 父节点菜单
				return Message.error("	请选择对应的菜单节点");
			}
		} else {
			return Message.error("请选择对应的类型");
		}

		if (StringUtils.isEmpty(resourcesBean.getPermission())) {
			return Message.error("资源唯一标识不能为空");
		}

		int count = resourcesService.selectCount(new EntityWrapper<SysResourcesBean>().eq("permission", resourcesBean.getPermission()));
		if (count > 0) {
			return Message.error("当前资源唯一标识已经存在");
		}


		resourcesService.insert(resourcesBean);
		return Message.success("添加成功");
	}

	@Permission(action = Action.Skip)
	@RequestMapping(value = "/toEdit", method = RequestMethod.GET)
	public String toEdit(Long id, ModelMap modelMap) {

		SysResourcesBean resourcesBean = resourcesService.selectById(id);
		modelMap.addAttribute("resource", resourcesBean);
		return "ftl/resource/edit";
	}

	@Permission("system:resource:update")
	@RequestMapping(value = "/edit", method = RequestMethod.POST)
	@ResponseBody
	public Message edit(SysResourcesBean resourcesBean) {

		SysResourcesBean sysResourcesBean = resourcesService.selectById(resourcesBean.getId());
		if (sysResourcesBean == null) {
			return Message.error("资源不存在");
		}

		if (StringUtils.isEmpty(resourcesBean.getPermission())) {
			return Message.error("资源唯一标识不能为空");
		}

		if (resourcesBean.getParentId() != null && sysResourcesBean.getParentId().compareTo(resourcesBean.getId()) == 0) {
			return Message.error("上级菜单不能等于当前资源");
		}
		if (!sysResourcesBean.getPermission().equals(resourcesBean.getPermission())) {
			int count = resourcesService.selectCount(new EntityWrapper<SysResourcesBean>().eq("permission", resourcesBean.getPermission()));
			if (count > 0) {
				return Message.error("当前资源唯一标识已经存在");
			}
		}
		//存在的情况  如果是根节点  已经有子节点 无法修改成 功能

		if (sysResourcesBean.getParentId().compareTo(0l) == 0) {

			List<SysResourcesBean> childResources = resourcesService.selectList(new EntityWrapper<SysResourcesBean>().eq("parent_id", sysResourcesBean.getId()));
			if (childResources != null && childResources.size() > 0) {
				if (resourcesBean.getType() == 2 || resourcesBean.getType() == 1) {//当前节点被修改为 目录 或功能
					return Message.error("当前资源已经有子节点,不能修改当前类型为功能或目录");
				}
			}
		} else {
			//原先的为功能 则 现在的为功能类型 父节点不能为空

			if (resourcesBean.getParentId() == null) {
				return Message.error("当前父节点不能为空");
			}
		}

		resourcesService.updateById(resourcesBean);
		return Message.success("修改成功");
	}

	@Permission("system:resource:update")
	//禁用 启用
	@RequestMapping(value = "/forbid", method = RequestMethod.POST)
	@ResponseBody
	public Message forbid(Long id, Integer delStatus) {

		SysResourcesBean resourcesBean = resourcesService.selectById(id);
		if (resourcesBean != null) {
			if (delStatus != null && (delStatus == 1 || delStatus == 0)) {
				resourcesBean.setDelStatus(delStatus);
				resourcesService.updateById(resourcesBean);
				return Message.success("成功");
			} else {
				return Message.error("输入参数有误");
			}
		} else {
			return Message.error("该资源不存在");
		}
	}

	//获取所有的菜单
	@Permission(action = Action.Skip)
	@RequestMapping(value = "/findAllParentResources", method = RequestMethod.POST)
	@ResponseBody
	public ResponseEntity findAllParentResources() {
		List<SysResourcesBean> resourcesBeanList = resourcesService.selectList(new EntityWrapper<SysResourcesBean>().eq("del_status", 1));
		return ResponseEntity.ok(resourcesBeanList);
	}

	@Permission(action = Action.Skip)
	@RequestMapping(value = "/toAuthorize", method = RequestMethod.GET)
	public ModelAndView toAuthorize(@RequestParam(required = true) Long roleId) {

		ModelAndView mv = new ModelAndView();

		//角色对应的权限
		List<ResourceInfoResponse> resourceInfoResponseList = resourcesService.findResesByRoleId(roleId);
		//所有的权限
		List<ResourceInfoResponse> allResesList = resourcesService.findAllResources();
		if (allResesList != null && allResesList.size() > 0) {

			if (resourceInfoResponseList != null && resourceInfoResponseList.size() > 0) {
				int i = 0;
				for (ResourceInfoResponse res : allResesList) {
					boolean isHave = false;
					for (ResourceInfoResponse roleRes : resourceInfoResponseList) {

						if (res.getId().compareTo(roleRes.getId()) == 0) {
							isHave = true;
							break;
						}
					}
					if (isHave) {
						res.setbCheck(true);
					} else {
						res.setbCheck(false);
					}
					allResesList.set(i, res);
					i++;
				}
			}
		}
		mv.addObject("resourceList", allResesList);

		mv.addObject("roleId", roleId);
		mv.setViewName("ftl/resource/authorize");
		return mv;
	}


}
