package com.open.admin.web.controller;


import com.baomidou.kisso.annotation.Action;
import com.baomidou.kisso.annotation.Permission;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.mapper.Wrapper;
import com.open.admin.web.comm.BaseAdminController;
import com.open.admin.web.comm.page.Page;
import com.open.admin.web.comm.page.PageParameter;
import com.open.admin.web.comm.pojo.Message;
import com.open.system.domain.data.SysBusinessAreaBean;
import com.open.system.domain.zone.SysOpenZoneBean;
import com.open.system.service.api.ISysBusinessAreaService;
import com.open.system.service.api.ISysOpenZoneService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;



/**
 * 开发商圈接口
 *
 * @date: 2017-06-15 1:08
 * @author: xfz
 */
@Controller
@RequestMapping("/admin/sys/business")
public class SysBusinessController extends BaseAdminController {

    @Autowired
    private ISysBusinessAreaService businessAreaService;

    @Autowired
    private ISysOpenZoneService openZoneService;

    @Permission(action = Action.Skip)
    @RequestMapping(value = "/toList", method = RequestMethod.GET)
    public String toList() {
        return "ftl/business/list";
    }

    @Permission(action = Action.Skip)
    @ResponseBody
    @RequestMapping(value = "/list", method = RequestMethod.POST)
    public Page listData(Long openZoneId, PageParameter pageable) {
        com.baomidou.mybatisplus.plugins.Page page=getMybatisPage(pageable);
        if(openZoneId!=null&&openZoneId>0){
            Map<String, Object> condition = new ConcurrentHashMap();
            condition.put("open_zone_id",openZoneId);

            page.setCondition(condition);

        }
        return transformPage(businessAreaService.selectPage(page), pageable);
    }
    @Permission(action = Action.Skip)
    @RequestMapping(value = "/toAdd", method = RequestMethod.GET)
    public String toAdd() {
        return "ftl/business/add";
    }

    @Permission(action = Action.Skip)
    @RequestMapping(value = "/add", method = RequestMethod.POST)
    @ResponseBody
    public Message add(SysBusinessAreaBean areaBean) {

        SysOpenZoneBean openZoneBean= openZoneService.selectById(areaBean.getOpenZoneId());
        if(openZoneBean==null){
            return   Message.error("所选开放城市不能为空");
        }
        areaBean.setProvinceCode(openZoneBean.getProvinceCode());
        areaBean.setProvinceName(openZoneBean.getProvinceName());
        areaBean.setCityCode(openZoneBean.getCityCode());
        areaBean.setCityName(openZoneBean.getCityName());
        // TODO
        businessAreaService.insert(areaBean);
        return Message.success("添加成功");
    }

    @Permission(action = Action.Skip)
    @RequestMapping(value = "/toEdit", method = RequestMethod.GET)
    public String toEdit(Long id, ModelMap modelMap) {
        modelMap.put("areaBean", businessAreaService.selectById(id));
        return "ftl/business/edit";
    }
    @Permission(action = Action.Skip)
    @RequestMapping(value = "/edit", method = RequestMethod.POST)
    @ResponseBody
    public Message edit(SysBusinessAreaBean areaBean) {
        // TODO

        businessAreaService.updateById(areaBean);
        return Message.success("修改成功");
    }

    //禁用 启用 对应的 开放城市
    @Permission(action = Action.Skip)
    @RequestMapping(value = "/forbidBusiness", method = RequestMethod.POST)
    @ResponseBody
    public Message forbidBusiness(Long id,Integer delStatus) {
        SysBusinessAreaBean  response = businessAreaService.selectById(id);
        if (response != null) {
            if (delStatus != null && (delStatus == 1 || delStatus == 0)) {
                response.setDelStatus(delStatus);
                businessAreaService.updateById(response);
                return Message.success("成功");
            } else {
                return Message.error("输入参数有误");
            }
        } else {
            return Message.error("该商圈不存在");
        }

    }

    /**
     * 获取下拉值
     * 通过开放城市id  获取 商圈
     * @return
     */
    @Permission(action = Action.Skip)
    @RequestMapping(value = "/findBussinessAreaIdAndName", method = RequestMethod.POST)
    public ResponseEntity findBussinessAreaIdAndName(Long openZoneId){
        Wrapper<SysBusinessAreaBean> wapper = new EntityWrapper<SysBusinessAreaBean>();
        // delStatus = 1
        wapper.setSqlSelect("id, name");
        wapper.where("open_zone_id={0}", openZoneId);
        wapper.where("del_status=1");
        return ResponseEntity.ok(businessAreaService.selectMaps(wapper));
    }
}
