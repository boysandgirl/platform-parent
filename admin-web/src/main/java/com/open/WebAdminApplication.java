package com.open;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.Banner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.ServletComponentScan;
import org.springframework.boot.web.support.SpringBootServletInitializer;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * The type Web admin application.
 *
 * @author xfz
 */

@ServletComponentScan//servlet 扫描 主要是 DruidStatViewServlet
@SpringBootApplication
@EnableTransactionManagement//开启事物
public class WebAdminApplication extends SpringBootServletInitializer {

	private static final Logger LOGGER = LoggerFactory.getLogger(WebAdminApplication.class);

	@Override
	protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
		return application.sources(WebAdminApplication.class);
	}


	@RequestMapping
	@ResponseBody
	public String hello() {
		return "Hello World!";
	}


	public static void main(String[] args) {
		SpringApplication application = new SpringApplication(WebAdminApplication.class);
		application.setBannerMode(Banner.Mode.OFF);
		application.run(args);
		LOGGER.info("Web admin started!!!");
	}
}
