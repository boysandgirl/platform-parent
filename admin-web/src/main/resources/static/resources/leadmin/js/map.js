// 弹出选择经纬度
function selectJingWeiDu(){
    var location = $("#openZoneId").find("option:selected").text();
    layer.open({
        type: 2,
        title: '获取经纬度',
        shadeClose: false,
        scrollbar: false,
        shift:1,
        shade: 0.8,
        area: ['100%', '100%'],
        content: '/map.html?location=' + location //iframe的url
    });

}
// longitude: 经度  // latitude: 纬度
function setJingWeiDu (longitude, latitude) {
    $("#longitude").val(longitude);
    $("#latitude").val(latitude);

    $.ajax({
        url: '/admin/biz/apartment/getAddress?longitude='+latitude+'&latitude='+longitude,
        type: 'POST',
        dataType: 'JSON',
        success: function (ret) {
            if (ret.type == 'success') {
                $("#address").val(ret.content);
            }
        }
    });
}