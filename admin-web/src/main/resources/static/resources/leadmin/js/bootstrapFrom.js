var options={
	    target:'#letype',    // 把服务器返回的内容放入id为output的元素中
	    beforeSubmit:showRequest,    // 提交前的回调函数
	    success:showResponse,        // 提交后的回调函数
	    dataType:'json',
	    clearForm:true,
	    resetForm:true
	}
$(document).ready(function(){
	$(".i-checks").iCheck({checkboxClass:"icheckbox_square-green",radioClass:"iradio_square-green",})
	$(".iCheck-helper").click(function(){
		$("#"+$(this).parent().find("input[type='radio']").attr("name")+"-error").remove();
	});
	$(".radio.i-checks label").click(function(){
		$("#"+$(this).find("input[type='radio']").attr("name")+"-error").remove();
	});
});
function leClose(){
	var index = parent.layer.getFrameIndex(window.name); //先得到当前iframe层的索引
	parent.layer.close(index); //再执行关闭
}
function showRequest(formData, jqForm, options){
	if($('button[leType=leSubmitBt]').attr('dataBef')=='true'){
		   return true;  //只要不返回false，表单都会提交,在这里可以对表单元素进行验证
	}else{
		  return true;  //只要不返回false，表单都会提交,在这里可以对表单元素进行验证
	}
}
function showResponse(responseText, statusText){
	if(responseText.type=="success"){
			var form = parent.$("#demoForm").serialize();
			parent.leRefresh();
		    parent.layer.msg(responseText.content,{icon:1,time:1500},function(){
				leClose();
		    });
	}else if(responseText.type == "error"){
	   parent.layer.msg(responseText.content,{skin:'layui-layer-molv'});
	}
}
