/*
 * Copyright 2010-2015 fjlonge.com. All rights reserved.
 * Support: http://www.fjlonge.com
 * License: http://www.fjlonge.com/license
 * 
 * JavaScript - Common
 * Version: 1.0
 */

var leAdminGlobal = {
	base: "/leWebsite",
	locale: "zh_CN"
};


var setting = {
	priceScale: "2",
	priceRoundType: "roundHalfUp",
	currencySign: "￥",
	currencyUnit: "元",
	uploadImageExtension: "jpg,jpeg,bmp,gif,png",
	uploadFlashExtension: "swf,flv",
	uploadMediaExtension: "swf,flv,mp3,wav,avi,rm,rmvb",
	uploadFileExtension: "zip,rar,7z,doc,docx,xls,xlsx,ppt,pptx"
};

var messages = {
	"admin.message.success": "操作成功",
	"admin.message.error": "操作错误",
	"admin.dialog.ok": "确&nbsp;&nbsp;定",
	"admin.dialog.cancel": "取&nbsp;&nbsp;消",
	"admin.dialog.deleteConfirm": "您确定要删除吗？",
	"admin.dialog.clearConfirm": "您确定要清空吗？",
	"admin.browser.title": "选择文件",
	"admin.browser.upload": "本地上传",
	"admin.browser.parent": "上级目录",
	"admin.browser.orderType": "排序方式",
	"admin.browser.name": "名称",
	"admin.browser.size": "大小",
	"admin.browser.type": "类型",
	"admin.browser.select": "选择文件",
	"admin.upload.sizeInvalid": "上传文件大小超出限制",
	"admin.upload.typeInvalid": "上传文件格式不正确",
	"admin.upload.invalid": "上传文件格式或大小不正确",
	"admin.validate.required": "必填",
	"admin.validate.email": "E-mail格式错误",
	"admin.validate.url": "网址格式错误",
	"admin.validate.date": "日期格式错误",
	"admin.validate.dateISO": "日期格式错误",
	"admin.validate.pointcard": "信用卡格式错误",
	"admin.validate.number": "只允许输入数字",
	"admin.validate.digits": "只允许输入零或正整数",
	"admin.validate.minlength": "长度不允许小于{0}",
	"admin.validate.maxlength": "长度不允许大于{0}",
	"admin.validate.rangelength": "长度必须在{0}-{1}之间",
	"admin.validate.min": "不允许小于{0}",
	"admin.validate.max": "不允许大于{0}",
	"admin.validate.range": "必须在{0}-{1}之间",
	"admin.validate.accept": "输入后缀错误",
	"admin.validate.equalTo": "两次输入不一致",
	"admin.validate.remote": "输入错误",
	"admin.validate.integer": "只允许输入整数",
	"admin.validate.positive": "只允许输入正数",
	"admin.validate.negative": "只允许输入负数",
	"admin.validate.decimal": "数值超出了允许范围",
	"admin.validate.pattern": "格式错误",
	"admin.validate.extension": "文件格式错误"
};
			
// 多语言
function message(code) {
	if (code != null) {
		var content = messages[code] != null ? messages[code] : code;
		if (arguments.length == 1) {
			return content;
		} else {
			if ($.isArray(arguments[1])) {
				$.each(arguments[1], function(i, n) {
					content = content.replace(new RegExp("\\{" + i + "\\}", "g"), n);
				});
				return content;
			} else {
				$.each(Array.prototype.slice.apply(arguments).slice(1), function(i, n) {
					content = content.replace(new RegExp("\\{" + i + "\\}", "g"), n);
				});
				return content;
			}
		}
	}
}




//文件浏览
$.fn.extend({
	browser: function(options) {
		var settings = {
			type: "image",
			isUpload: true,
			browserUrl: leAdminGlobal.base +"/admin/file/browser.jhtml",
			uploadUrl: leAdminGlobal.base +"/admin/file/upload.jhtml",
			callback: null
		};
		$.extend(settings, options);
		
		//var token = getCookie("token");
		var cache = {};
		return this.each(function() {
			var $browserButton = $(this);
			$browserButton.click(function() {
				
				var imageBrowserHtml = Hogan.compile(LE_TEMPLATE.imageBrowser).render({
		        	'base': leAdminGlobal.base
		        });
				var imageBrowser = layer.open({
				    type: 1,
				    title: '选择文件',
				    area: ['940px', '490px'],
				    content: imageBrowserHtml
				});
				
				
				var $browserUploadInput = $('#browserUploadInput');
				var $browserForm =$('#browserForm');
				var $browserOrderType = $("#orderType");
				var $browserList = $("#browserList");
				var $browserParentButton = $("#browserParentButton");
				var $browserFrame = $('#browserFrame');
				
				

				browserList("/");
				var loadingLoad;
				function browserList(path) {
					var key = settings.type + "_" + path + "_" + $browserOrderType.val();
					if (cache[key] == null) {
						$.ajax({
							url: settings.browserUrl,
							type: "GET",
							data: {fileType: settings.type, orderType: $browserOrderType.val(), path: path},
							dataType: "json",
							cache: false,
							beforeSend: function() {
								loadingLoad = layer.load();
							},
							success: function(data) {
								createBrowserList(path, data);
								cache[key] = data;
							},
							complete: function() {
								layer.close(loadingLoad);
							}
						});
					} else {
						createBrowserList(path, cache[key]);
					}
				};
				
				
				function createBrowserList(path, data) {
					var browserListHtml = "";
					$.each(data, function(i, fileInfo) {
						var iconUrl;
						var title;
						if (fileInfo.isDirectory) {
							iconUrl = leAdminGlobal.base + "/resources/leadmin/img/folder_icon.gif";
							title = fileInfo.name;
						} else if (new RegExp("^\\S.*\\.(jpg|jpeg|bmp|gif|png)$", "i").test(fileInfo.name)) {
							iconUrl = fileInfo.url;
							title = fileInfo.name + " (" + Math.ceil(fileInfo.size / 1024) + "KB, " + new Date(fileInfo.lastModified).toLocaleString() + ")";
						} else {
							iconUrl = leAdminGlobal.base + "/resources/leadmin/img/file_icon.gif";
							title = fileInfo.name + " (" + Math.ceil(fileInfo.size / 1024) + "KB, " + new Date(fileInfo.lastModified).toLocaleString() + ")";
						}
						browserListHtml += Hogan.compile(LE_TEMPLATE.imageHtml).render({
				        	'iconUrl': iconUrl,
				        	'title': title,
				        	'url': fileInfo.url,
				        	'isDirectory': fileInfo.isDirectory,
				        	'fileName': fileInfo.name,
				        });
					});
					
					$browserList.html(browserListHtml);
					
					$browserList.find("img").bind("click", function() {
						var $this = $(this);
						var isDirectory = $this.attr("isDirectory");
						if (isDirectory == "true") {
							var name = $this.attr("name");
							browserList(path + name + "/");
						} else {
							var url = $this.attr("url");
							if (settings.input != null) {
								settings.input.val(url);
							} else {
								$browserButton.prev(":text").val(url);
							}
							
							if (settings.callback != null && typeof settings.callback == "function") {
								settings.callback(url);
							}
							
							layer.close(imageBrowser);
						}
					});
					
					if (path == "/") {
						$browserParentButton.unbind("click");
					} else {
						var parentPath = path.substr(0, path.replace(/\/$/, "").lastIndexOf("/") + 1);
						$browserParentButton.unbind("click").bind("click", function() {
							browserList(parentPath);
						});
					}
					$browserOrderType.unbind("change").bind("change", function() {
						browserList(path);
					});
				};
				
				$browserUploadInput.change(function() {
					var allowedUploadExtensions;
					if (settings.type == "flash") {
						allowedUploadExtensions = setting.uploadFlashExtension;
					} else if (settings.type == "media") {
						allowedUploadExtensions = setting.uploadMediaExtension;
					} else if (settings.type == "file") {
						allowedUploadExtensions = setting.uploadFileExtension;
					} else {
						allowedUploadExtensions = setting.uploadImageExtension;
					}
					if ($.trim(allowedUploadExtensions) == "" || !new RegExp("^\\S.*\\.(" + allowedUploadExtensions.replace(/,/g, "|") + ")$", "i").test($browserUploadInput.val())) {
						layer.msg(message("admin.upload.typeInvalid"), 1, 3);
						return false;
					}
					loadingLoad = layer.load();
					$browserForm.submit();
				});
				
				
				$browserFrame.load(function() {
					var text;
					var io = document.getElementById("browserFrame");
					if(io.contentWindow) {
						text = io.contentWindow.document.body ? io.contentWindow.document.body.innerHTML : null;
					} else if(io.contentDocument) {
						text = io.contentDocument.document.body ? io.contentDocument.document.body.innerHTML : null;
					}
					if ($.trim(text) != "") {
						layer.close(loadingLoad);
						var data = $.parseJSON(text);
						if (data.message.type == "success") {
							if (settings.input != null) {
								settings.input.val(url);
							} else {
								$browserButton.prev(":text").val(data.url);
							}
							
							if (settings.callback != null && typeof settings.callback == "function") {
								settings.callback(data.url);
							}
							
							cache = {};
						} else {
							layer.msg(data.message, 1, 3);
						}
					}
					
					layer.close(imageBrowser);
				});
			});
			
		});
	}
});


function fatherFunction(obj){
	var f = window[obj.name];
	var ff = function(){}
	if(f){
		ff = f;
		return ff(obj.content);
	}else{
		var _frames = document.getElementsByTagName('iframe');
		for(var f = 0 ; f < _frames.length ; f++){
			var _n = _frames[f].getAttribute('name');
			try{
				window.frames[_n].fatherFunction(obj);
			}catch(e){}
		}
	}
}