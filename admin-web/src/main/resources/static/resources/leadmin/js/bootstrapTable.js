var pagNum=1;
$(function(){
	$('table[leType=leTable]').each(function(){
		var _lePage = $(this).attr('lePageSize');
		var _id = $(this).attr('id')||'leTabel'+Math.floor(Math.random()*100000);
		$(this).attr('id',_id);
		leTableData(1,_lePage,_id);
	});
})
function  loadPageData(pageNumber,_id){
	var _this=$("#"+_id);
	leTablePageData(pageNumber,_this.attr('lePageSize'),_id);
}

function leTableData(pageNumber,pageSize,_id){
	var _this=$("#"+_id);
//	alert(pageNumber+":"+pageSize);
	parent.leIframeName=window.name;
	$('div[leType=leGrid]').hide();
	_this.hide();
	$.post(_this.attr('leUrl'),{pageNumber:pageNumber,pageSize:pageSize},function(responseText){
		if(responseText.type == "success"){
			if(_this.attr('lePage')=="true"){
				var pageS=(JSON.parse(responseText.content).page.number-1)*(JSON.parse(responseText.content).page.size)+1;
				var pageE=JSON.parse(responseText.content).page.number*JSON.parse(responseText.content).page.size;
				var pageC=JSON.parse(responseText.content).page.total;
				if(pageE>pageC){
					pageE=pageC;
				}
				var	htmlleft='<div class="clearfix" id="'+_id+'pagination"><div class="pull-left pagination"><span class="pagination-info">';	
				htmlleft=htmlleft+"显示第"+pageS+" 到第 "+pageE+"条记录，总共 "+pageC+" 条记录"+'</span></div>';
				var htmlright='<div class="pull-right pagination-sm" id="lePageCenter">';
				
				var pageHtml='<ul class="pagination pagination-outline">';
				if(JSON.parse(responseText.content).page.number==1){
					pageHtml=pageHtml+'<li class="page-first disabled"><a href="javascript:void(0)">«</a></li>'+
					'<li class="page-pre disabled"><a href="javascript:void(0)">‹</a></li>';
				}else{
					pageHtml=pageHtml+'<li class="page-first"><a href="javascript:void(0)" onclick="loadPageData('+1+',\''+_id+'\')">«</a></li>'+
					'<li class="page-pre"><a href="javascript:void(0)" onclick="loadPageData('+(JSON.parse(responseText.content).page.number-1)+',\''+_id+'\')">‹</a></li>';
				}
				var num=1;
				if(JSON.parse(responseText.content).page.pages>5){
					if(JSON.parse(responseText.content).page.number>3){
						num=JSON.parse(responseText.content).page.number-2;
					}
				}
				for (var i=num;i<=5;i++){
					if(i<=JSON.parse(responseText.content).page.pages){
						if(JSON.parse(responseText.content).page.number==i){
							pageHtml=pageHtml+'<li class="page-number active"><a href="javascript:void(0)" onclick="loadPageData('+i+',\''+_id+'\')">'+i+'</a></li>';
						}else{
							pageHtml=pageHtml+'<li class="page-number"><a href="javascript:void(0)" onclick="loadPageData('+i+',\''+_id+'\')">'+i+'</a></li>';
						}
					}
				}
				
				if((JSON.parse(responseText.content).page.pages)>5){
					if(JSON.parse(responseText.content).page.number+2<JSON.parse(responseText.content).page.pages){
						num=JSON.parse(responseText.content).page.number+2;
					}
				}
				
				
				if(JSON.parse(responseText.content).page.number==JSON.parse(responseText.content).page.pages){
					pageHtml=pageHtml+
					'<li class="page-next disabled"><a href="javascript:void(0)">›</a></li>'+
					'<li class="page-last disabled"><a href="javascript:void(0)">»</a></li>'+
					'</ul>';
				}else{
					pageHtml=pageHtml+
					'<li class="page-next"><a href="javascript:void(0)" onclick="loadPageData('+(JSON.parse(responseText.content).page.number+1)+',\''+_id+'\')">›</a></li>'+
					'<li class="page-last"><a href="javascript:void(0)" onclick="loadPageData('+JSON.parse(responseText.content).page.pages+',\''+_id+'\')">»</a></li>'+
					'</ul>';
				}
				
				if(pageC<JSON.parse(responseText.content).page.size){
					pageHtml="";
				}
				
				htmlright=htmlright+pageHtml+'</div></div>';
				if($("#"+_id+"pagination").length>0){
					$("#"+_id+"pagination").remove();
				}
				console.log(JSON.parse(responseText.content).content);
				_this.bootstrapTable({
		            data: JSON.parse(responseText.content).content,
		            search: !0,
		            pagination: !1,
		            showRefresh: !0,
		            iconSize: "outline",
		            toolbar: "div[leType="+_id+"Toolbar]",
		            icons: {
		                refresh: "glyphicon-repeat",
		                toggle: "glyphicon-list-alt",
		                columns: "glyphicon-list"
		            }
		    	});		
				_this.parent().parent().after(htmlleft+htmlright);
			}else{
				_this.bootstrapTable({
		            data: JSON.parse(responseText.content),
		            search: !0,
		            pagination: !0,
		            showRefresh: !0,
		            iconSize: "outline",
		            toolbar: "div[leType="+_id+"Toolbar]",
		            icons: {
		                refresh: "glyphicon-repeat",
		                toggle: "glyphicon-list-alt",
		                columns: "glyphicon-list"
		            }
		    	});		
			}
			$('div[leType=loading]').hide();
			$('div[leType=leGrid]').show();
			_this.show();
		}else if(responseText.type == "error"){
			parent.layer.alert("数据加载失败！",{skin:'layui-layer-molv'});
			} 
		});
}

function leTablePageData(pageNumber,pageSize,_id){
	var _this=$("#"+_id);
//	alert(pageNumber+":"+pageSize);
	parent.leIframeName=window.name;
	$('div[leType=leGrid]').hide();
	_this.hide();
	$.post(_this.attr('leUrl'),{pageNumber:pageNumber,pageSize:pageSize},function(responseText){
		if(responseText.type == "success"){
			   
				var pageS=(JSON.parse(responseText.content).page.number-1)*(JSON.parse(responseText.content).page.size)+1;
				var pageE=JSON.parse(responseText.content).page.number*JSON.parse(responseText.content).page.size;
				var pageC=JSON.parse(responseText.content).page.total;
				if(pageE>pageC){
					pageE=pageC;
				}
				 pagNum=pageS;
				var	htmlleft='<div class="clearfix" id="'+_id+'pagination"><input type="hidden" id="'+_id+'pagnum" value="'+pageNumber+'"/><div class="pull-left pagination"><span class="pagination-info">';	
				htmlleft=htmlleft+"显示第"+pageS+" 到第 "+pageE+"条记录，总共 "+pageC+" 条记录"+'</span></div>';
				var htmlright='<div class="pull-right pagination-sm" id="lePageCenter">';
				
				var pageHtml='<ul class="pagination pagination-outline">';
				if(JSON.parse(responseText.content).page.number==1){
					pageHtml=pageHtml+'<li class="page-first disabled"><a href="javascript:void(0)">«</a></li>'+
					'<li class="page-pre disabled"><a href="javascript:void(0)">‹</a></li>';
				}else{
					pageHtml=pageHtml+'<li class="page-first"><a href="javascript:void(0)" onclick="loadPageData('+1+',\''+_id+'\')">«</a></li>'+
					'<li class="page-pre"><a href="javascript:void(0)" onclick="loadPageData('+(JSON.parse(responseText.content).page.number-1)+',\''+_id+'\')">‹</a></li>';
				}
				var sum=1;
				if(JSON.parse(responseText.content).page.number>3){
					sum=JSON.parse(responseText.content).page.number-2;
				}
				if((JSON.parse(responseText.content).page.number+2)>JSON.parse(responseText.content).page.pages){
					if((JSON.parse(responseText.content).page.pages-4)<1){
						sum=1;
					}else{
						sum=JSON.parse(responseText.content).page.pages-4;
					}
				}
				for (var i=sum,j=1;j<=5;i++,j++){
					if(i<=JSON.parse(responseText.content).page.pages){
						//alert(i);
						if(JSON.parse(responseText.content).page.number==i){
							pageHtml=pageHtml+'<li class="page-number active"><a href="javascript:void(0)" onclick="loadPageData('+i+',\''+_id+'\')">'+i+'</a></li>';
						}else{
							pageHtml=pageHtml+'<li class="page-number"><a href="javascript:void(0)" onclick="loadPageData('+i+',\''+_id+'\')">'+i+'</a></li>';
						}
					}
				}
				if(JSON.parse(responseText.content).page.number==JSON.parse(responseText.content).page.pages){
					pageHtml=pageHtml+
					'<li class="page-next disabled"><a href="javascript:void(0)">›</a></li>'+
					'<li class="page-last disabled"><a href="javascript:void(0)">»</a></li>'+
					'</ul>';
				}else{
					pageHtml=pageHtml+
					'<li class="page-next"><a href="javascript:void(0)" onclick="loadPageData('+(JSON.parse(responseText.content).page.number+1)+',\''+_id+'\')">›</a></li>'+
					'<li class="page-last"><a href="javascript:void(0)" onclick="loadPageData('+JSON.parse(responseText.content).page.pages+',\''+_id+'\')">»</a></li>'+
					'</ul>';
				}
				
				htmlright=htmlright+pageHtml+'</div></div>';
				if($("#"+_id+"pagination").length>0){
					$("#"+_id+"pagination").remove();
				}
				console.log(JSON.parse(responseText.content).content);
				_this.bootstrapTable('load', JSON.parse(responseText.content).content);
				_this.parent().parent().after(htmlleft+htmlright);
			$('div[leType=loading]').hide();
			$('div[leType=leGrid]').show();
			_this.show();
		}else if(responseText.type == "error"){
			parent.layer.alert("数据加载失败！",{skin:'layui-layer-molv'});
			} 
		});
}


function leRefresh(){
	$('table[leType=leTable]').each(function(){
		var _lePage = $(this).attr('lePageSize');
		var _id = $(this).attr('id');
		if($(this).attr('lePage')=="true"){
			loadPageData($("#"+_id+"pagnum").val(),_id)
		}else{
			$.post($('table[leType=leTable]').attr('leUrl'),{},function(responseText){
				if(responseText.type == "success"){
				    var data=JSON.parse(responseText.content);
				    $('table[leType=leTable]').bootstrapTable('load',data);		
				}else if(responseText.type == "error"){
					parent.layer.alert("数据加载失败！",{skin:'layui-layer-molv'});
					} 
			    });
		}
	});
}
function getRowId(row,row,index){
	return index+pagNum;
}
window.operateEvents = {
    'click #edit': function (e, value, row) {
    	editInfo(row);
    },
    'click #delete': function (e, value, row) {
        leDel(row);
    }
};
//操作栏
function operateFormatter(value, row, index) {
    return [
        '<div class="pull-ce">',
        '<a id="edit" href="javascript:void(0)" title="edit">',
        '<i class="fa fa-cog"></i>',
        '</a>&nbsp;&nbsp;&nbsp;',
        '<a id="delete"  href="javascript:void(0)" title="delete">',
        '<i class="fa fa-ban" style="color:#EF5352;"></i>',
        '</a>',
        '</div>'
    ].join('');
}