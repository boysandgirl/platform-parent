var LeTree={
	/**
	 * ztree初始化
	 * @param {Object} id
	 */
		treeInit:function(id){
		$(document).on("click", function (){
			//对document绑定一个影藏Div方法
			LeTree.hideZtreeMenu(id);
		});
//		console.log(document)
		event.stopPropagation();//阻止事件向上冒泡
		$(id).click(function (event){
			event.stopPropagation();//阻止事件向上冒泡
		});	
	},
	/**
	 * ztree下拉隐藏
	 * @param {Object} id
	 */
	hideZtreeMenu: function (id){
		$(id).fadeOut("fast");
	},
	/**
	 * ztree选中之前事件
	 * @param {Object} treeId
	 * @param {Object} treeNode
	 */
	beforeZtreeClick:function (treeId, treeNode) {
		var check = (treeNode && !treeNode.isParent);
		if (!check) 
		layer.msg('只能选择城市！', 1, 3);
		//alert("只能选择城市");
		var id=treeId.substring(0, treeId.length-9);
		$("#"+id+"MenuContent").slideDown("fast");
		return check;
	},
		/**
		 * ztree 单选选中事件
		 * @param {Object} e
		 * @param {Object} treeId
		 * @param {Object} treeNode
		 */
	onZtreeClick:function (e, treeId, treeNode) {
		var zTree = $.fn.zTree.getZTreeObj(treeId),
		nodes = zTree.getSelectedNodes(),
		v = "",ids="";
		nodes.sort(function compare(a,b){return a.id-b.id;});
		for (var i=0, l=nodes.length; i<l; i++) {
			v += nodes[i].name + ",";
			if(ids==""){
				ids += nodes[i].id ;
			}else{
				ids += ","+nodes[i].id;	
			}
		}
		if (v.length > 0 ) v = v.substring(0, v.length-1);
		
		var id=treeId.substring(0, treeId.length-9);
		var cityObj = $("#"+id+"ZtreeInput");
		var idObj = $("#"+id);
		cityObj.attr("value", v);
		idObj.attr("dataText", v);
		idObj.attr("value", ids);
		if(idObj.attr("leZtreeClick")=="true"){
			//leZtreeClick(ids,v);
			LeTree.doCallback(eval(id+"Click"),[ids,v]); 
		};
		
	},
	/**
	 * ztree选中多选事件
	 * @param {Object} e
	 * @param {Object} treeId
	 * @param {Object} treeNode
	 */
	onZtreeCheck:function (e, treeId, treeNode) {
		var zTree = $.fn.zTree.getZTreeObj(treeId),
		nodes = zTree.getCheckedNodes(true),
		v = "",ids="";
		for (var i=0, l=nodes.length; i<l; i++) {
			v += nodes[i].name + ",";
			if(ids==""){
				ids += nodes[i].id ;
			}else{
				ids += ","+nodes[i].id;	
			}
			
		}
		if (v.length > 0 ) v = v.substring(0, v.length-1);
		
		var id=treeId.substring(0, treeId.length-9);
		var cityObj = $("#"+id+"ZtreeInput");
		var idObj = $("#"+id);
		cityObj.attr("value", v);
		idObj.attr("dataText", v);
		idObj.attr("value", ids);
		
		if(idObj.attr("leZtreeClick")=="true"){
			//leZtreeClick(ids,v);
			LeTree.doCallback(eval(id+"Click"),[ids,v]); 
		};
	},
	/**
	 * ztree下拉显示事件
	 * @param {Object} id
	 */
	showZtreeMenu:function (id) {
		//var cityObj =$("input[name='selectTree']");
		var cityObj = $("#"+id+"ZtreeInput");
		var cityOffset = $("#"+id+"ZtreeInput").offset();
		LeTree.treeInit("#"+id+"MenuContent");
		$("#"+id+"MenuContent").slideDown("fast");
	},
	/**
	 * 设置下拉框
	 * @param a
	 * @returns {String}
	 */
	selectTreeDom:function (a){
		var id=$(a).attr('id');
		var name=$(a).attr('name');
		var leZtreeClick=$(a).attr('leZtreeClick');
		var htmlDom= '<input id='+id+' name='+name+' type="hidden" dataText="" leZtreeClick='+leZtreeClick+' value=""   />'+
		               '<input id='+id+'ZtreeInput type="text" treeType="selectTree" class="form-control" readonly  value="" onclick="LeTree.showZtreeMenu(\''+id+'\'); return false;"/>'+
						 '<div id='+id+'MenuContent class="menuContent" treeType="menuContent" style="display:none; position: absolute;z-index:1000000 ;background-color:#CFCFCF;">'+
						 	'<ul id='+id+'ZtreeList class="ztree input" style="margin-top:0; width:220px;" ></ul>'+
						 '</div>';
		return htmlDom;
	 },
	/**
	 * 设置下拉框
	 * @param a
	 * @returns {String}
	 */
	listTreeDom:function (a){
		var id=$(a).attr('id');
		var leZtreeClick=$(a).attr('leZtreeClick');
		var htmlDom= '<input id='+id+' type="hidden" dataText="" leZtreeClick='+leZtreeClick+' value="" />'+
//		             '<ul id='+id+'root class="ztree" style="margin-top:0; width:200px;">'+
//		             	'<li id="listTreeZtreeList_1" class="level0" tabindex="0" hidefocus="true" treenode="">'+
//							'<span id="listTreeZtreeList_1_switch" title="" class="button level0 switch" treenode_switch=""></span>'+
//							'<a id="listTreeZtreeList_1_a" class="level0" treenode_a="" onclick="" target="_blank" style="" title="河北省">'+
//								'<span id="listTreeZtreeList_1_ico" title="" treenode_ico="" class="button ico_close" style=""></span>'+
//								'<span id="listTreeZtreeList_1_span">河北省</span>'+
//							'</a>'+
//						'</li>'+	
//		             '</ul>'+
		             '<ul id='+id+'ZtreeList class="ztree" style="margin-top:0; width:200px;"></ul>';
		return htmlDom;
	 },
	 initSelectTree:function(id,type,zNodes){
		 try{
			
			 $("input[treeType='leSelectTree']").replaceWith(function(){
					return LeTree.selectTreeDom(this);
				});
				$("input[treeType='selectTree']").click(function(){
					$("input[treeType='selectTree']").not(this).each(function() {
//						$("div[treeType='menuContent']").fadeOut("fast");
						//防止冒泡时间
//						 event.stopPropagation();
			        });
					var id=$("input[treeType='selectTree']").attr("id");
					LeTree.showZtreeMenu(id);
				});
			 $.fn.zTree.init($("#"+id+"ZtreeList"), type, zNodes);
			LeTree.getChackNodes(id);
			}catch(e){
				var zNodesDefault =[{id:0, pId:0, name:"没有节点！"}];
				 $.fn.zTree.init($("#"+id+"ZtreeList"), LeTree.settingDefault, zNodesDefault);
//				console.log(e.name+":"+e.message);
			} 
			
			
			
	 },
	 initListTree:function(id,type,zNodes){
		 var zNodesDefault =[{id:0, pId:0, name:"没有节点！"}];
		 try{
			 if(zNodes){
				 $("input[treeType='leListTree']").replaceWith(function(){
						return LeTree.listTreeDom(this);
					});
				 $.fn.zTree.init($("#"+id+"ZtreeList"), type, zNodes);
			 }else{
				 $("input[treeType='leListTree']").replaceWith(function(){
						return LeTree.listTreeDom(this);
					});
				 $.fn.zTree.init($("#"+id+"ZtreeList"), LeTree.settingDefault, zNodesDefault);
			 }
			
			}catch(e){
				 $.fn.zTree.init($("#"+id+"ZtreeList"), LeTree.settingDefault, zNodesDefault);
//			     console.log(e.name+":"+e.message);
			} 
	 },
	 //选中单个选项
	 selectNode:function(treeName,id){  
		   var ztree=$.fn.zTree.getZTreeObj(treeName+"ZtreeList");
		  // alert(ztree);
		   try{
			   ztree.selectNode(ztree.getNodeByParam("id",id, null));
				LeTree.expandLevel(ztree,ztree.getNodeByParam("id",id, null),1);
				$("#"+treeName).attr("datatext",ztree.getNodeByParam("id",id, null).name);
				$("#"+treeName).val(id);
				var ztreeInput=$("#"+treeName+"ZtreeInput");
				if(ztreeInput){
					ztreeInput.attr("value", ztree.getNodeByParam("id",id, null).name);
				}
			}catch(e){
			 try{
				  var nodes = ztree.getNodes();
			      if (nodes.length>0) 
			      {     
			            var node = ztree.selectNode(nodes[0]);
			            $("#"+treeName).val(nodes[0].id);
			            $("#"+treeName).attr("datatext", nodes[0].name);
						var ztreeInput=$("#"+treeName+"ZtreeInput");
						if(ztreeInput){
							ztreeInput.attr("value", nodes[0].name);
						}
			      }
				}catch(e){
//					console.log(e.name  +   " :  "   +  e.message);
				}
//					 console.log(e.name  +   " :  "   +  e.message);
			} 
	 },
	 //选中多个选项
	 selectNodes:function(treeName,ids){
		   //var arr=ids.split(',');
		 	  $.each(ids,function(n,value) {   
		 		 //alert(value);
		 		   var ztree=$.fn.zTree.getZTreeObj(treeName+"ZtreeList"); 
				   ztree.selectNode(ztree.getNodeByParam("id",value, null));       
	           });  
	 },
	 //展开父节点
	 expandLevel:function (treeObj,node){
		    treeObj.expandNode(node,true,false);
	},
	//获取选择节点值
	getChackNodes:function(treeId){
		//alert("11");
		var zTree = $.fn.zTree.getZTreeObj(treeId+"ZtreeList"),
		nodes=zTree.getCheckedNodes(true),
        v="",ids="";
        for(var i=0;i<nodes.length;i++){
        	if(v==""){
				v += nodes[i].name ;
			}else{
				v += ","+nodes[i].name;	
			}
			if(ids==""){
				ids += nodes[i].id ;
			}else{
				ids += ","+nodes[i].id;	
			}
        }
        var cityObj = $("#"+treeId+"ZtreeInput");
		var idObj = $("#"+treeId);
		cityObj.attr("value", v);
		idObj.attr("dataText", v);
		idObj.attr("value", ids);
	},//这个方法做了一些操作、然后调用回调函数    
	doCallback:function(fn,args)    
	{    
	    fn.apply(this, args);  
	},//展开所有节点
	openNodes:function(treeId){
		var treeObj = $.fn.zTree.getZTreeObj(treeId+"ZtreeList");
		treeObj.expandAll(true); 
	}   
	
	 
	 
	 
	 
}

/**
 * zTree下拉框事件
 */
//单选
LeTree.settingDefault = {
	view: {
		dblClickExpand: false
	},
	data: {
		simpleData: {
			enable: true
		}
	},
	callback: {
		//选中前事件
		//beforeClick: beforeZtreeClick,
		//选中事件
		onClick: LeTree.onZtreeClick
	}
};
//多选
LeTree.settingCheck = {
	view: {
		selectedMulti: false
	},
	check: {
		enable: true
	},
	data: {
		simpleData: {
			enable: true
		}
	},
	callback: {
		onCheck: LeTree.onZtreeCheck
	}
};

//jQuery扩展
(function ($){
	$("input[name='selectTree']").click(function(){
		alert($(this).Attr("id"));
		LeTree.showZtreeMenu();
	});
	
	$("input[name='selectTree']").blur(function(){
		//$("div[treeType='menuContent']").fadeOut("fast");
		alert($(this).Attr("id"));
	});
	
	
	
	
})(jQuery);

