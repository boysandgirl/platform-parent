package com.open.system.dao.mapper;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.open.system.dao.response.EmployeeInfoResponse;
import com.open.system.dao.sql.SysEmployeeBeanSqlProvider;
import com.open.system.domain.user.SysEmployeeBean;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.Results;
import org.apache.ibatis.annotations.SelectProvider;

import java.util.List;
import java.util.Map;

/**
 * 系统 用户dao
 *
 * @date: 2017-06-13 17:33
 * @author: hlc
 */
@Mapper
public interface SysEmployeeBeanMapper extends BaseMapper<SysEmployeeBean> {

	/**
	 * @param map
	 * @description: 分页查看员工系统员工列表
	 * @date: 2017/6/20 21:25
	 * @author: xfz
	 */

	@SelectProvider(type = SysEmployeeBeanSqlProvider.class, method = "findPageEmployees")
	@Results({
			@Result(property = "id", column = "id", id = true),
			@Result(property = "name", column = "name"),
			@Result(property = "delStatus", column = "del_status"),
			@Result(property = "nickname", column = "nickname"),
			@Result(property = "phone", column = "phone"),
			@Result(property = "sex", column = "sex"),
			@Result(property = "roleNames", column = "role_names"),
			@Result(property = "description", column = "description")
	})
	public List<EmployeeInfoResponse> findEmployees(Map<String, Object> map);
}
