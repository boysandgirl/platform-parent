package com.open.system.dao.mapper;


import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.open.system.domain.zone.SysZoneBean;
import org.apache.ibatis.annotations.*;

import java.util.List;

@Mapper
public interface SysZoneBeanMapper extends BaseMapper<SysZoneBean> {

	@Select("select " +
			"id, " +
			"zone_code," +
			"zone_name," +
			"zone_type," +
			"parent_zone_code, " +
			"del_status," +
			" create_time, " +
			"update_time" +
			" from " +
			" " + SysZoneBean.TABLE_NAME + " " +
			" where 1=1 " +
			" and  parent_zone_code=#{parentZoneCode} " +
			" and del_status = 1 ")
	@Results({
			@Result(property = "id", column = "id", id = true),
			@Result(property = "zoneCode", column = "zone_code"),
			@Result(property = "zoneName", column = "zone_name"),
			@Result(property = "parentZoneCode", column = "parent_zone_code"),
			@Result(property = "zoneType", column = "zone_type"),
			@Result(property = "delStatus", column = "del_status"),
			@Result(property = "createTime", column = "create_time"),
			@Result(property = "updateTime", column = "update_time")
	})
	List<SysZoneBean> findZonesByParentZoneCode(@Param("parentZoneCode") String parentZoneCode);


	@Select("select count(*)" +
			" from " + SysZoneBean.TABLE_NAME + " where parent_zone_code=#{parentZoneCode} and zone_code=#{zoneCode} and del_status = 1 ")
	int isExist(@Param("parentZoneCode") String parentZoneCode, @Param("zoneCode") String zoneCode);


}
