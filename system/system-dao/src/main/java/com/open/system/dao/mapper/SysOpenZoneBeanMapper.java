package com.open.system.dao.mapper;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.open.system.dao.sql.SysOpenZoneBeanSqlMapper;
import com.open.system.domain.zone.SysOpenZoneBean;
import org.apache.ibatis.annotations.*;

import java.util.List;
import java.util.Map;

/**
 * abc开发区域dao
 *
 * @date: 2017-06-09 0:54
 * @author: xfz
 */
@Mapper
public interface SysOpenZoneBeanMapper extends BaseMapper<SysOpenZoneBean> {


	/**
	 * @param provinceCode 省份code
	 * @param cityCode     城市code
	 * @param areaCode     区域code
	 * @description: 用于统计 省市区是否重复用
	 * @date: 2017/6/9 1:00
	 * @author: xfz
	 */
	@Select("select count(*) " +
			" from " + SysOpenZoneBean.TABLE_NAME + " " +
			"where " +
			" 1=1 " +
			" and  province_code=#{provinceCode}" +
			" and city_code=#{cityCode}" +
			" and area_code=#{areaCode}"
	)
	public int count(String provinceCode, String cityCode, String areaCode);

	@UpdateProvider(type = SysOpenZoneBeanSqlMapper.class, method = "findByParams")
	@Results({
			@Result(property = "id", column = "id", id = true),
			@Result(property = "title", column = "title"),
			@Result(property = "areaName", column = "area_name"),
			@Result(property = "areaCode", column = "area_code"),
			@Result(property = "cityName", column = "city_name"),
			@Result(property = "cityCode", column = "city_code"),
			@Result(property = "provinceName", column = "province_name"),
			@Result(property = "provinceCode", column = "province_code"),
			@Result(property = "delStatus", column = "del_status"),
			@Result(property = "createTime", column = "create_time"),
			@Result(property = "updateTime", column = "update_time")
	})
	public List<SysOpenZoneBean> findByParams(Map params);
}
