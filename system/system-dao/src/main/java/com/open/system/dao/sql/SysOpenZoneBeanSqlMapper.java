package com.open.system.dao.sql;


import com.open.system.domain.zone.SysOpenZoneBean;
import com.open.system.domain.zone.SysZoneBean;
import org.springframework.util.StringUtils;

import java.util.Map;

public class SysOpenZoneBeanSqlMapper {

	public String update(SysOpenZoneBean openZoneBean) {

		StringBuilder sql = new StringBuilder();
		sql.append("update " + SysOpenZoneBean.TABLE_NAME + " set");
		sql.append(" update_time = now()");

		if (!StringUtils.isEmpty(openZoneBean.getTitle())) {
			sql.append(", title = #{title}");
		}
		if (!StringUtils.isEmpty(openZoneBean.getProvinceCode())) {
			sql.append(", province_code = #{provinceCode}");
		}
		if (!StringUtils.isEmpty(openZoneBean.getProvinceName())) {
			sql.append(", province_name = #{provinceName}");
		}
		if (!StringUtils.isEmpty(openZoneBean.getCityCode())) {
			sql.append(", city_code = #{cityCode}");
		}
		if (!StringUtils.isEmpty(openZoneBean.getCityName())) {
			sql.append(", city_name = #{cityName}");
		}
		if (!StringUtils.isEmpty(openZoneBean.getAreaCode())) {
			sql.append(", area_code = #{areaCode}");
		}
		if (!StringUtils.isEmpty(openZoneBean.getAreaName())) {
			sql.append(",area_name = #{areaName}");
		}
		if (null != openZoneBean.getSort()) {
			sql.append(", sort = #{sort}");
		}
		if (null != openZoneBean.getDelStatus()) {
			sql.append(", del_status = #{delStatus}");
		}
		sql.append("" +
				" where " +
				" 1=1 " +
				" and id = #{id}");
		return sql.toString();
	}


	public String findByParams(Map map){
		StringBuilder sb=new StringBuilder();
		sb.append("select  ");

		sb.append(" * ");

		sb.append(" from " +
						" " + SysOpenZoneBean.TABLE_NAME + " " +
				" where " +
				" 1=1 ");

		if(map.containsKey("provinceCode")) {
			sb.append(" and province_code=#{provinceCode}");
		}
		if(map.containsKey("cityCode")) {
			sb.append(" and city_code=#{cityCode}");
		}
		if(map.containsKey("areaCode")) {
			sb.append(" and area_code=#{areaCode}");
		}

		sb.append("select id, zone_code,zone_name,zone_type,parent_zone_code, is_deleted, create_time, update_time" +
				" from " + SysZoneBean.TABLE_NAME + " where parent_zone_code=#{parentZoneCode} and is_deleted = 0 ");
		return sb.toString();
	}
}
