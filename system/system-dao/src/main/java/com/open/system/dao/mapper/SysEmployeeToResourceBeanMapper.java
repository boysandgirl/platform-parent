package com.open.system.dao.mapper;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.open.system.domain.user.SysEmployeeToResourceBean;
import org.apache.ibatis.annotations.Mapper;

/**
 * 系统用户对应的资源权限dao
 *
 * @date: 2017-06-19 23:17
 * @author: xfz
 */
@Mapper
public interface SysEmployeeToResourceBeanMapper extends BaseMapper<SysEmployeeToResourceBean> {
}
