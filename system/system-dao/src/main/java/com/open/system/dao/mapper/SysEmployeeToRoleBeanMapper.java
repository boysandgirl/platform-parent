package com.open.system.dao.mapper;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.open.system.domain.role.SysEmployeeToRoleBean;
import org.apache.ibatis.annotations.Mapper;

/**
 * 系统用户对应的角色dao
 *
 * @date: 2017-06-19 23:19
 * @author: xfz
 */
@Mapper
public interface SysEmployeeToRoleBeanMapper extends BaseMapper<SysEmployeeToRoleBean> {
}
