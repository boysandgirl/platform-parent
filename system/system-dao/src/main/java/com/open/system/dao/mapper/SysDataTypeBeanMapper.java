package com.open.system.dao.mapper;


import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.open.system.domain.data.SysDataTypeBean;
import org.apache.ibatis.annotations.Mapper;

/**
 * 字典类型dao
 *
 * @Date 2017-06-13
 * @Time 17:53
 * @Author 徐福周
 */
@Mapper
public interface SysDataTypeBeanMapper extends BaseMapper<SysDataTypeBean> {
}
