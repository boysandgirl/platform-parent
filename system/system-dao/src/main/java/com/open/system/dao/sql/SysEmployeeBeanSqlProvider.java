package com.open.system.dao.sql;


import com.open.system.domain.role.SysEmployeeToRoleBean;
import com.open.system.domain.role.SysRoleBean;
import com.open.system.domain.user.SysEmployeeBean;

import java.io.Serializable;
import java.util.Map;

/**
 * 系统员工sql
 *
 * @date: 2017-06-20 21:20
 * @author: xfz
 */
public class SysEmployeeBeanSqlProvider implements Serializable {

    public String findPageEmployees(Map<String,Object> map){
        StringBuilder sb=new StringBuilder();

        String returnDatas=" e.id , " +
                " GROUP_CONCAT(r.`name` SEPARATOR ',') as role_names , " +
                " e.del_status ," +
                " e.name, " +
                " e.nickname , " +
                " e.age,  " +
                " e.phone ," +
                " e.sex, " +
                " e.description ";

        sb.append(" select ");

        sb.append(returnDatas);
        sb.append(" from "+ SysEmployeeBean.TABLE_NAME+" as e ");

        sb.append(" left join "+ SysEmployeeToRoleBean.TABLE_NAME+" as  etr ");
        sb.append(" on(e.id=etr.employee_id) ");
        sb.append(" left join "+ SysRoleBean.TABLE_NAME+" as  r ");
        sb.append(" on(etr.role_id =r.id) ");
        sb.append(" group by e.id ");

        sb.append(" limit #{pageNum},#{pageSize} ");
        return sb.toString();
    }
}
