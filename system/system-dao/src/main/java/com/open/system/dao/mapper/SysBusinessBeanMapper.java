package com.open.system.dao.mapper;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.open.system.domain.data.SysBusinessAreaBean;
import org.apache.ibatis.annotations.Mapper;

/**
 *  商圈dao
 *
 * @Date 2017-06-09
 * @Time 18:03
 * @Author 徐福周
 */
@Mapper
public interface SysBusinessBeanMapper extends BaseMapper<SysBusinessAreaBean> {
}
