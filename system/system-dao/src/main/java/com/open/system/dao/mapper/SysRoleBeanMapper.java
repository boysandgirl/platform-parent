package com.open.system.dao.mapper;


import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.open.system.domain.role.SysRoleBean;
import org.apache.ibatis.annotations.Mapper;

/**
 * 系统角色dao
 *
 * @date: 2017-06-19 23:12
 * @author: xfz
 */
@Mapper
public interface SysRoleBeanMapper extends BaseMapper<SysRoleBean> {
}
