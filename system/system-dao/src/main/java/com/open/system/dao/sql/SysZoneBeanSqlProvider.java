package com.open.system.dao.sql;


import com.open.system.domain.zone.SysZoneBean;
import org.springframework.util.StringUtils;

public class SysZoneBeanSqlProvider {

	public String update(SysZoneBean sysZoneBean) {

		StringBuilder sql = new StringBuilder();
		sql.append("update " + SysZoneBean.TABLE_NAME + "" +
				" set ");
		sql.append(" update_time = now()");

		if (!StringUtils.isEmpty(sysZoneBean.getZoneCode())) {
			sql.append(", zone_code = #{zoneCode}");
		}
		if (!StringUtils.isEmpty(sysZoneBean.getZoneName())) {
			sql.append(", zone_name = #{zoneName}");
		}
		if (!StringUtils.isEmpty(sysZoneBean.getParentZoneCode())) {
			sql.append(", parent_zone_code = #{parentZoneCode}");
		}
		if (null != sysZoneBean.getZoneType()) {
			sql.append(", zone_type = #{zoneType}");
		}

		if (null != sysZoneBean.getDelStatus()) {
			sql.append(", delStatus = #{delStatus}");
		}
		sql.append(" where del_status = 1 and id = #{id}");
		return sql.toString();
	}

}
