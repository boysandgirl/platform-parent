package com.open.system.dao.mapper;


import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.open.system.domain.user.SysEmployeeToOpenZoneBean;
import org.apache.ibatis.annotations.Mapper;

/**
 * 系统用户对应的可操作的开放城市dao
 *
 * @date: 2017-06-19 23:17
 * @author: xfz
 */
@Mapper
public interface SysEmployeeToOpenZoneBeanMapper extends BaseMapper<SysEmployeeToOpenZoneBean> {
}
