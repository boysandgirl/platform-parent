package com.open.system.dao.sql;

import com.open.system.domain.resource.SysResourcesBean;
import org.springframework.util.StringUtils;

/**
 * 资源sql复杂处理
 *
 * @Date 2017-06-05
 * @Time 18:14
 * @Author 徐福周
 */

public class SysResourcesSqlProvider {

	public String update(SysResourcesBean resourcesBean) {

		StringBuilder sql = new StringBuilder();

		sql.append("update " + SysResourcesBean.TABLE_NAME + " set update_time = now()");

		if (!StringUtils.isEmpty(resourcesBean.getName())) {
			sql.append(", name = #{name}");
		}
		sql.append(" where is_deleted = 0 and id = #{id}");
		return sql.toString();
	}
}
