package com.open.system.dao.mapper;


import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.open.system.domain.resource.SysResourcesBean;
import com.open.system.domain.role.SysRoleToResourceBean;
import org.apache.ibatis.annotations.*;

import java.util.List;

/**
 * 系统资源菜单dao
 *
 * @Date 2017-06-05
 * @Time 18:12
 * @Author 徐福周
 */
@Mapper
public interface SysResourcesBeanMapper extends BaseMapper<SysResourcesBean> {

//	public void save(SysResourcesBean resourcesBean);
//
//	public void update(SysResourcesBean resourcesBean);
//
//	public SysResourcesBean getByResourceId(Long resourceId);


	@Select("select  " +
			"distinct r.id,r.parent_id,r.name,r.permission" +
			"" +
			" from " + SysResourcesBean.TABLE_NAME + " r inner join " + SysRoleToResourceBean.TABLE_NAME + " rs on (rs.resource_id = r.id )" +
			"" +
			" where " +
			"" +
			" 1=1 " +
			" and r.del_status = 1 " +
			" and r.have_show = 1 " +
			" and rs.role_id = #{roleId} ")
	@Results({
			@Result(property = "id", column = "id", id = true),
			@Result(property = "name", column = "name"),
			@Result(property = "delStatus", column = "del_status"),
			@Result(property = "parentId", column = "parent_id"),
			@Result(property = "permission", column = "permission")
	})
	public List<SysResourcesBean> findResourceByRoleId(@Param("roleId") Long roleId);


}
