package com.open.system.dao.mapper;


import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.open.system.domain.role.SysRoleToResourceBean;
import org.apache.ibatis.annotations.Mapper;

/**
 * 系统角色对应的资源dao
 *
 * @date: 2017-06-19 23:14
 * @author: xfz
 */
@Mapper
public interface SysRoleToResourceBeanMapper extends BaseMapper<SysRoleToResourceBean> {


}
