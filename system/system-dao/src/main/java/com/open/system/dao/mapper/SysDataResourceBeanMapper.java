package com.open.system.dao.mapper;


import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.open.system.domain.data.SysDataResourceBean;
import org.apache.ibatis.annotations.Mapper;

/**
 * 系统数据资源dao
 *
 * @Date 2017-06-13
 * @Time 17:54
 * @Author 徐福周
 */

@Mapper
public interface SysDataResourceBeanMapper extends BaseMapper<SysDataResourceBean> {
}
