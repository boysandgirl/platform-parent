package com.open.system.dao.response;

import java.io.Serializable;

/**
 * 系统员工对象
 *
 * @date: 2017-06-20 20:43
 * @author: xfz
 */
public class EmployeeInfoResponse implements Serializable {

    //用户id
    private Long id;

    //是否可用 1可用 0不可用
    private Integer delStatus;

    //姓名
    private String name;

    //昵称
    private String nickname;

    //电话
    private String phone;

    //1男 2 女 0 未知
    private Integer sex;

    //角色
    private String roleNames;

    //描述
    private String description;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getDelStatus() {
        return delStatus;
    }

    public void setDelStatus(Integer delStatus) {
        this.delStatus = delStatus;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public Integer getSex() {
        return sex;
    }

    public void setSex(Integer sex) {
        this.sex = sex;
    }

    public String getRoleNames() {
        return roleNames;
    }

    public void setRoleNames(String roleNames) {
        this.roleNames = roleNames;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
