package com.open.system.dao.mapper;


import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.open.system.domain.role.SysRoleToOpenZoneBean;
import org.apache.ibatis.annotations.Mapper;

/**
 * 系统角色对应的开放城市dao
 *
 * @date: 2017-06-19 23:27
 * @author: xfz
 */
@Mapper
public interface SysRoleToOpenZoneBeanMapper extends BaseMapper<SysRoleToOpenZoneBean> {
}
