package com.open.system.dao.mapper;


import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.open.system.domain.data.SysBusinessAreaBean;
import org.apache.ibatis.annotations.Mapper;

/**
 * SysBusinessAreaBeanMapper
 *
 * @date: 2017-06-15 18:15
 * @author: hlc
 */
@Mapper
public interface SysBusinessAreaBeanMapper extends BaseMapper<SysBusinessAreaBean> {
}
