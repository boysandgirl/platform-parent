package com.open.system.domain.zone;

import com.open.comm.bean.DataEntity;

import java.io.Serializable;

/**
 * 所有的省市区列表
 *
 * @date: 2017-06-08 22:40
 * @author: xfz
 */

public class SysZoneBean extends DataEntity<SysZoneBean> {

	public static final String TABLE_NAME = "sys_zone_bean";

	@Override
	protected Serializable pkVal() {
		return super.getId();
	}

	public static Byte TYPE_NATION = 1;

	public static Byte TYPE_PROVINCE = 2;

	public static Byte TYPE_CITY = 3;

	public static Byte TYPE_AREA = 4;

	/**
	 * 地区编码
	 */
	private String zoneCode;

	/**
	 * 地区名称
	 */
	private String zoneName;

	private String parentZoneCode;


	private Byte zoneType;//地区类型(1国家2省3市4区)

	public String getZoneCode() {
		return zoneCode;
	}

	public void setZoneCode(String zoneCode) {
		this.zoneCode = zoneCode;
	}

	public String getZoneName() {
		return zoneName;
	}

	public void setZoneName(String zoneName) {
		this.zoneName = zoneName;
	}

	public String getParentZoneCode() {
		return parentZoneCode;
	}

	public void setParentZoneCode(String parentZoneCode) {
		this.parentZoneCode = parentZoneCode;
	}

	public Byte getZoneType() {
		return zoneType;
	}

	public void setZoneType(Byte zoneType) {
		this.zoneType = zoneType;
	}

	public static int getTye(Byte zoneType) {
		int type = 0;
		if (zoneType.compareTo(TYPE_NATION) == 0) {
			type = 1;
		} else if (zoneType.compareTo(TYPE_PROVINCE) == 0) {
			type = 2;
		} else if (zoneType.compareTo(TYPE_CITY) == 0) {
			type = 3;
		} else if (zoneType.compareTo(TYPE_AREA) == 0) {
			type = 4;
		}
		return type;
	}
}
