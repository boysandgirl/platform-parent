package com.open.system.domain.role;

import com.open.comm.bean.DataEntity;
import com.open.comm.enumucation.RoleEnum;

import java.io.Serializable;

/**
 * 系统角色表
 *
 * @date: 2017-06-19 22:44
 * @author: xfz
 */
public class SysRoleBean extends DataEntity<SysRoleBean> {
	public static final String TABLE_NAME = "sys_role_bean";

	@Override
	protected Serializable pkVal() {
		return super.getId();
	}

	//角色名称
	private String name;

//    //是否可用  默认1 可用 0 不可用 有delStatus
//    private int enable;

	//类型  默认系统有  客服主管 客服员  接待员  带看员   超级管理员 管理员  跟名称相对应  不能删除和修改 除了超级管理员可以  默认其他类型
	private String type = RoleEnum.OTHER.getName();

	//角色描述
	private String description;


	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

//    public int getEnable() {
//        return enable;
//    }
//
//    public void setEnable(int enable) {
//        this.enable = enable;
//    }

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
}
