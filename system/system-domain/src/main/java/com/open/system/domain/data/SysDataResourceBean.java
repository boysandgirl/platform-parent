package com.open.system.domain.data;


import com.open.comm.bean.DataEntity;

import java.io.Serializable;

/**
 * 数据字典集合
 *
 * @date: 2017-06-11 17:43
 * @author: xfz
 */

public class SysDataResourceBean extends DataEntity<SysDataResourceBean> {
	public static final String TABLE_NAME = "sys_data_resource_bean";

	@Override
	protected Serializable pkVal() {
		return super.getId();
	}


	private String name;


	private String dataCode;

	//多级字典 父级id

	private Long parentId;

	/**
	 * 必填 为系统现在有什么类型的字典  录入的依据  也是查询的依据 【各个字典的区分】
	 */

	private Long dataTypeId;

	//如面积使用的字典---防止是 金额等 全部用string

	private String minData;

	//如面积使用的字典  价格字典等

	private String maxData;


	/***************************************************/

	/**
	 * 所属开放城市   如字典为商圈的时候
	 */

	private Long openZoneId;

	//省份编码

	private String provinceCode;
	//省份名称

	private String provinceName;
	//区域编码

	private String cityCode;
	//城市名称

	private String cityName;
	//区域编码

	private String areaCode;
	//区域名称

	private String areaName;

	/*******************************************/

	private String description;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDataCode() {
		return dataCode;
	}

	public void setDataCode(String dataCode) {
		this.dataCode = dataCode;
	}

	public Long getParentId() {
		return parentId;
	}

	public void setParentId(Long parentId) {
		this.parentId = parentId;
	}

	public Long getDataTypeId() {
		return dataTypeId;
	}

	public void setDataTypeId(Long dataTypeId) {
		this.dataTypeId = dataTypeId;
	}

	public String getMinData() {
		return minData;
	}

	public void setMinData(String minData) {
		this.minData = minData;
	}

	public String getMaxData() {
		return maxData;
	}

	public void setMaxData(String maxData) {
		this.maxData = maxData;
	}

	public Long getOpenZoneId() {
		return openZoneId;
	}

	public void setOpenZoneId(Long openZoneId) {
		this.openZoneId = openZoneId;
	}

	public String getProvinceCode() {
		return provinceCode;
	}

	public void setProvinceCode(String provinceCode) {
		this.provinceCode = provinceCode;
	}

	public String getProvinceName() {
		return provinceName;
	}

	public void setProvinceName(String provinceName) {
		this.provinceName = provinceName;
	}

	public String getCityCode() {
		return cityCode;
	}

	public void setCityCode(String cityCode) {
		this.cityCode = cityCode;
	}

	public String getCityName() {
		return cityName;
	}

	public void setCityName(String cityName) {
		this.cityName = cityName;
	}

	public String getAreaCode() {
		return areaCode;
	}

	public void setAreaCode(String areaCode) {
		this.areaCode = areaCode;
	}

	public String getAreaName() {
		return areaName;
	}

	public void setAreaName(String areaName) {
		this.areaName = areaName;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
}
