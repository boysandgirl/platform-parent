package com.open.system.domain.data;

import com.open.comm.bean.DataEntity;

import java.io.Serializable;

/**
 * 省市区 区下商圈 表
 *
 * @date: 2017-06-08 22:59
 * @author: xfz
 */

public class SysBusinessAreaBean extends DataEntity<SysBusinessAreaBean> {

	public static final String TABLE_NAME = "sys_business_area__bean";

	@Override
	protected Serializable pkVal() {
		return super.getId();
	}
	//商圈名称

	private String name;

	private String businessAreaCode;

	/**
	 * 所属开放城市
	 */

	private Long openZoneId;

	//省份编码

	private String provinceCode;
	//省份名称

	private String provinceName;
	//区域编码

	private String cityCode;
	//城市名称

	private String cityName;
	//区域编码

	private String areaCode;
	//区域名称

	private String areaName;


	private String description;

	public Long getOpenZoneId() {
		return openZoneId;
	}

	public void setOpenZoneId(Long openZoneId) {
		this.openZoneId = openZoneId;
	}


	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getBusinessAreaCode() {
		return businessAreaCode;
	}

	public void setBusinessAreaCode(String businessAreaCode) {
		this.businessAreaCode = businessAreaCode;
	}

	public String getProvinceCode() {
		return provinceCode;
	}

	public void setProvinceCode(String provinceCode) {
		this.provinceCode = provinceCode;
	}

	public String getProvinceName() {
		return provinceName;
	}

	public void setProvinceName(String provinceName) {
		this.provinceName = provinceName;
	}

	public String getCityCode() {
		return cityCode;
	}

	public void setCityCode(String cityCode) {
		this.cityCode = cityCode;
	}

	public String getCityName() {
		return cityName;
	}

	public void setCityName(String cityName) {
		this.cityName = cityName;
	}

	public String getAreaCode() {
		return areaCode;
	}

	public void setAreaCode(String areaCode) {
		this.areaCode = areaCode;
	}

	public String getAreaName() {
		return areaName;
	}

	public void setAreaName(String areaName) {
		this.areaName = areaName;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
}
