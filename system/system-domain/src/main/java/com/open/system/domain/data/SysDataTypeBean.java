package com.open.system.domain.data;


import com.open.comm.bean.DataEntity;

import java.io.Serializable;

/**
 * 字典类型描述
 *
 * @date: 2017-06-11 18:03
 * @author: xfz
 */

public class SysDataTypeBean extends DataEntity<SysDataTypeBean> {

	public static final String TABLE_NAME = "sys_data_type_bean";

	@Override
	protected Serializable pkVal() {
		return super.getId();
	}

	/**
	 * 所属字典类型描述
	 */

	private String name;

	/**
	 * 默认没有下一级 类型  =0
	 */

	private Integer haveChild;

	//父级id字典描述 如： 品牌下有多个项目
	private Long parentId;


	private String description;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Integer getHaveChild() {
		return haveChild;
	}

	public void setHaveChild(Integer haveChild) {
		this.haveChild = haveChild;
	}

	public Long getParentId() {
		return parentId;
	}

	public void setParentId(Long parentId) {
		this.parentId = parentId;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
}
