package com.open.system.domain.role;


import com.open.comm.bean.DataEntity;

import java.io.Serializable;

/**
 * 系统用户对应的角色表
 *
 * @date: 2017-06-19 23:04
 * @author: xfz
 */
public class SysEmployeeToRoleBean extends DataEntity<SysEmployeeToRoleBean> {

	public static final String TABLE_NAME = "sys_employee_to_role_bean";

	@Override
	protected Serializable pkVal() {
		return super.getId();
	}

	private Long roleId;

	private Long employeeId;

	public Long getRoleId() {
		return roleId;
	}

	public void setRoleId(Long roleId) {
		this.roleId = roleId;
	}

	public Long getEmployeeId() {
		return employeeId;
	}

	public void setEmployeeId(Long employeeId) {
		this.employeeId = employeeId;
	}
}
