package com.open.system.domain.user;


import com.open.comm.bean.DataEntity;

import java.io.Serializable;

/**
 * 员工对应可查阅的开放城市表
 * <p>
 * 所有的用户对应的
 *
 * @date: 2017-06-19 22:51
 * @author: xfz
 */
public class SysEmployeeToOpenZoneBean extends DataEntity<SysEmployeeToOpenZoneBean> {

	public static final String TABLE_NAME = "sys_employee_to_open_zone_bean";

	@Override
	protected Serializable pkVal() {
		return super.getId();
	}

	//对应 开放城市
	private Long openZoneId;

	//对应系统员工上
	private Long employeeId;


	private String areaCode;

	private String areaName;


	public Long getOpenZoneId() {
		return openZoneId;
	}

	public void setOpenZoneId(Long openZoneId) {
		this.openZoneId = openZoneId;
	}

	public Long getEmployeeId() {
		return employeeId;
	}

	public void setEmployeeId(Long employeeId) {
		this.employeeId = employeeId;
	}

	public String getAreaCode() {
		return areaCode;
	}

	public void setAreaCode(String areaCode) {
		this.areaCode = areaCode;
	}

	public String getAreaName() {
		return areaName;
	}

	public void setAreaName(String areaName) {
		this.areaName = areaName;
	}
}
