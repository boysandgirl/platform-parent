package com.open.system.domain.resource;


import com.open.comm.bean.DataEntity;

import java.io.Serializable;

/**
 *
 */

public class SysResourcesBean extends DataEntity<SysResourcesBean> {

	/**
	 * 数据库表名
	 */
	public static final String TABLE_NAME = "sys_resources_bean";

	@Override
	protected Serializable pkVal() {
		return super.getId();
	}

	private static final long serialVersionUID = 1L;

	/**
	 * 父级编号//
	 */

	private Long parentId;
	/**
	 * 所有父级编号
	 */

	private String parentIds;
	/**
	 * 名称
	 */

	private String name;
	/**
	 * 链接
	 */

	private String href;
	/**
	 * 图标
	 */

	private String icon;

	//类型 菜单 功能  默认没选为 0 顶级目录0   菜单 1 功能为 2
	private Integer type = 0;

	/**
	 * 是否在菜单中显示（1：显示；0：不显示）
	 */
	private int haveShow;
	/**
	 * 权限标识[ 唯一标识] ----接口唯一标识哈哈哈  用数字搞定
	 */

	private String permission;
	/**
	 * 备注
	 */

	private String description;

	public Integer getType() {
		return type;
	}

	public void setType(Integer type) {
		this.type = type;
	}

	public SysResourcesBean() {
		super();
	}


	public Long getParentId() {
		return parentId;
	}

	public void setParentId(Long parentId) {
		this.parentId = parentId;
	}

	public String getParentIds() {
		return parentIds;
	}

	public void setParentIds(String parentIds) {
		this.parentIds = parentIds;
	}

	//    @Length(min = 1, max = 100)
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	//    @Length(min = 0, max = 2000)
	public String getHref() {
		return href;
	}

	public void setHref(String href) {
		this.href = href;
	}

	//    @Length(min = 0, max = 100)
	public String getIcon() {
		return icon;
	}

	public void setIcon(String icon) {
		this.icon = icon;
	}

	public int getHaveShow() {
		return haveShow;
	}

	public void setHaveShow(int haveShow) {
		this.haveShow = haveShow;
	}

	//    @Length(min = 0, max = 200)
	public String getPermission() {
		return permission;
	}

	public void setPermission(String permission) {
		this.permission = permission;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}


}