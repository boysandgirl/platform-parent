package com.open.system.domain.zone;

import com.open.comm.bean.DataEntity;

import java.io.Serializable;


/**
 * abc公寓字典开放区域
 *
 * @date: 2017-06-08 22:40
 * @author: xfz
 */

public class SysOpenZoneBean extends DataEntity<SysOpenZoneBean> {

	public static final String TABLE_NAME = "sys_open_zone_bean";

	@Override
	protected Serializable pkVal() {
		return super.getId();
	}
	//标题

	private String title;
	//省份编码

	private String provinceCode;
	//省份名称

	private String provinceName;
	//区域编码

	private String cityCode;
	//城市名称

	private String cityName;
	//区域编码

	private String areaCode;
	//区域名称

	private String areaName;

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getProvinceCode() {
		return provinceCode;
	}

	public void setProvinceCode(String provinceCode) {
		this.provinceCode = provinceCode;
	}

	public String getProvinceName() {
		return provinceName;
	}

	public void setProvinceName(String provinceName) {
		this.provinceName = provinceName;
	}

	public String getCityCode() {
		return cityCode;
	}

	public void setCityCode(String cityCode) {
		this.cityCode = cityCode;
	}

	public String getCityName() {
		return cityName;
	}

	public void setCityName(String cityName) {
		this.cityName = cityName;
	}

	public String getAreaCode() {
		return areaCode;
	}

	public void setAreaCode(String areaCode) {
		this.areaCode = areaCode;
	}

	public String getAreaName() {
		return areaName;
	}

	public void setAreaName(String areaName) {
		this.areaName = areaName;
	}
}
