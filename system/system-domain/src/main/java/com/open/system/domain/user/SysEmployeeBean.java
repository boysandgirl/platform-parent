package com.open.system.domain.user;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.open.comm.bean.DataEntity;

import java.io.Serializable;

/**
 * 员工  运营端  管理
 *
 * @date: 2017-06-09 16:08
 * @author: hlc
 */

public class SysEmployeeBean extends DataEntity<SysEmployeeBean> {

	public static final String TABLE_NAME = "sys_employee_bean";

	@Override
	protected Serializable pkVal() {
		return super.getId();
	}


	//登入名
	private String accountName;

	//密码
	private String password;

	//年龄 默认为0
	private Integer age;

	//1为男  2为女  0 未知   默认未知0
	private Integer sex;

	//手机号
	private String phone;


	// 姓名
	@JsonProperty
	private String name;
	// 微信opendid

	private String opendId;

	// 微信昵称

	private String nickname;

	// 岗位
	@JsonProperty
	private Integer post;

	//描述
	private String description;

	/**
	 * 用户所处开放城市
	 */
	private Long openZoneId;

	private String provinceName;

	private String cityName;

	private String areaName;

	private String provinceCode;

	private String cityCode;

	private String areaCode;


	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getOpendId() {
		return opendId;
	}

	public void setOpendId(String opendId) {
		this.opendId = opendId;
	}

	public String getNickname() {
		return nickname;
	}

	public void setNickname(String nickname) {
		this.nickname = nickname;
	}

	public Integer getPost() {
		return post;
	}

	public void setPost(Integer post) {
		this.post = post;
	}

	public String getAccountName() {
		return accountName;
	}

	public void setAccountName(String accountName) {
		this.accountName = accountName;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public Integer getAge() {
		return age;
	}

	public void setAge(Integer age) {
		this.age = age;
	}

	public Integer getSex() {
		return sex;
	}

	public void setSex(Integer sex) {
		this.sex = sex;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Long getOpenZoneId() {
		return openZoneId;
	}

	public void setOpenZoneId(Long openZoneId) {
		this.openZoneId = openZoneId;
	}

	public String getProvinceName() {
		return provinceName;
	}

	public void setProvinceName(String provinceName) {
		this.provinceName = provinceName;
	}

	public String getCityName() {
		return cityName;
	}

	public void setCityName(String cityName) {
		this.cityName = cityName;
	}

	public String getAreaName() {
		return areaName;
	}

	public void setAreaName(String areaName) {
		this.areaName = areaName;
	}

	public String getProvinceCode() {
		return provinceCode;
	}

	public void setProvinceCode(String provinceCode) {
		this.provinceCode = provinceCode;
	}

	public String getCityCode() {
		return cityCode;
	}

	public void setCityCode(String cityCode) {
		this.cityCode = cityCode;
	}

	public String getAreaCode() {
		return areaCode;
	}

	public void setAreaCode(String areaCode) {
		this.areaCode = areaCode;
	}
}
