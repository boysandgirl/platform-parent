package com.open.system.domain.user;


import com.open.comm.bean.DataEntity;

import java.io.Serializable;

/**
 * 系统用户对应的资源权限表
 *
 * @date: 2017-06-19 23:07
 * @author: xfz
 */
public class SysEmployeeToResourceBean extends DataEntity<SysEmployeeToResourceBean> {

	public static final String TABLE_NAME = "sys_employee_to_resource_bean";

	@Override
	protected Serializable pkVal() {
		return super.getId();
	}

	private Long employeeId;

	private Long resourceId;

	public Long getEmployeeId() {
		return employeeId;
	}

	public void setEmployeeId(Long employeeId) {
		this.employeeId = employeeId;
	}

	public Long getResourceId() {
		return resourceId;
	}

	public void setResourceId(Long resourceId) {
		this.resourceId = resourceId;
	}
}
