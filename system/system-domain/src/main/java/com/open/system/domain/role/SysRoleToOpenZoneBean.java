package com.open.system.domain.role;


import com.open.comm.bean.DataEntity;

import java.io.Serializable;

/**
 * 系统角色对应的开放城市表
 *
 * @date: 2017-06-19 23:25
 * @author: xfz
 */
public class SysRoleToOpenZoneBean  extends DataEntity<SysRoleToOpenZoneBean> {

    public static final String TABLE_NAME = "sys_role_to_open_zone_bean";

    @Override
    protected Serializable pkVal() {
        return super.getId();
    }

    //对应 开放城市
    private Long openZoneId;

    //对应系统角色上
    private Long roleId;

    /**
     * 开饭城市下的区域
     */
    private String areaCode;

    private String areaName;

    public Long getOpenZoneId() {
        return openZoneId;
    }

    public void setOpenZoneId(Long openZoneId) {
        this.openZoneId = openZoneId;
    }

    public Long getRoleId() {
        return roleId;
    }

    public void setRoleId(Long roleId) {
        this.roleId = roleId;
    }

    public String getAreaCode() {
        return areaCode;
    }

    public void setAreaCode(String areaCode) {
        this.areaCode = areaCode;
    }

    public String getAreaName() {
        return areaName;
    }

    public void setAreaName(String areaName) {
        this.areaName = areaName;
    }
}
