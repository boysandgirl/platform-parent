package com.open.system.domain.data;

import com.open.comm.bean.DataEntity;

import java.io.Serializable;

/**
 * 交通配套表---所属开放城市下
 *
 * @date: 2017-06-08 23:57
 * @author: xfz
 */

public class SysTrafficMatchingBean extends DataEntity<SysTrafficMatchingBean> {

	public static final String TABLE_NAME = "sys_traffic_matching_bean";

	@Override
	protected Serializable pkVal() {
		return super.getId();
	}
	//交通配套名称

	private String name;

	//配套设置编码唯一  插入新的要判断类型是否为空  删除的只能做软删除 切 code不能删除

	private String trafficMatchingCode;


	private String description;

	//省份编码

	private String provinceCode;
	//省份名称

	private String provinceName;
	//区域编码

	private String cityCode;
	//城市名称

	private String cityName;
	//区域编码

	private String areaCode;
	//区域名称

	private String areaName;

	/**
	 * 所属开放区域
	 */
	private Long openZoneId;

	public String getProvinceCode() {
		return provinceCode;
	}

	public void setProvinceCode(String provinceCode) {
		this.provinceCode = provinceCode;
	}

	public String getProvinceName() {
		return provinceName;
	}

	public void setProvinceName(String provinceName) {
		this.provinceName = provinceName;
	}

	public String getCityCode() {
		return cityCode;
	}

	public void setCityCode(String cityCode) {
		this.cityCode = cityCode;
	}

	public String getCityName() {
		return cityName;
	}

	public void setCityName(String cityName) {
		this.cityName = cityName;
	}

	public String getAreaCode() {
		return areaCode;
	}

	public void setAreaCode(String areaCode) {
		this.areaCode = areaCode;
	}

	public String getAreaName() {
		return areaName;
	}

	public void setAreaName(String areaName) {
		this.areaName = areaName;
	}

	public Long getOpenZoneId() {
		return openZoneId;
	}

	public void setOpenZoneId(Long openZoneId) {
		this.openZoneId = openZoneId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getTrafficMatchingCode() {
		return trafficMatchingCode;
	}

	public void setTrafficMatchingCode(String trafficMatchingCode) {
		this.trafficMatchingCode = trafficMatchingCode;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
}
