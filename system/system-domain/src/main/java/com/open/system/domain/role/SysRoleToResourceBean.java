package com.open.system.domain.role;

import com.open.comm.bean.DataEntity;

import java.io.Serializable;

/**
 * 角色对应的系统资源表
 *
 * @date: 2017-06-19 23:06
 * @author: xfz
 */
public class SysRoleToResourceBean extends DataEntity<SysRoleToResourceBean> {

	public static final String TABLE_NAME = "sys_role_to_resource_bean";

	@Override
	protected Serializable pkVal() {
		return super.getId();
	}

	private Long roleId;

	private Long resourceId;

	public Long getRoleId() {
		return roleId;
	}

	public void setRoleId(Long roleId) {
		this.roleId = roleId;
	}

	public Long getResourceId() {
		return resourceId;
	}

	public void setResourceId(Long resourceId) {
		this.resourceId = resourceId;
	}
}
