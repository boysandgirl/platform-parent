package com.open.system.response;

import java.io.Serializable;

/**
 * 基础的数据字典返回【针对对应的字典类型】
 *
 * @date: 2017-06-17 22:37
 * @author: xfz
 */
public class DataCommResponse implements Serializable {
    private Long id;

    private String name;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
