package com.open.system.service;


import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.open.comm.service.BaseServiceImpl;
import com.open.comm.utils.exception.SystemException;
import com.open.comm.utils.page.BasePageRequest;
import com.open.system.dao.mapper.SysOpenZoneBeanMapper;
import com.open.system.dao.mapper.SysZoneBeanMapper;
import com.open.system.domain.zone.SysOpenZoneBean;
import com.open.system.domain.zone.SysZoneBean;
import com.open.system.response.OpenZoneResponse;
import com.open.system.response.ZoneResponse;
import com.open.system.service.api.ISysOpenZoneService;
import org.apache.commons.beanutils.PropertyUtils;
import org.apache.ibatis.session.RowBounds;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * 开放城市实现服务
 *
 * @date: 2017-06-15 0:06
 * @author: xfz
 */
@Service
public class SysOpenZoneServiceImpl extends BaseServiceImpl<SysOpenZoneBeanMapper, SysOpenZoneBean> implements ISysOpenZoneService {

	@Autowired
	private SysOpenZoneBeanMapper openZoneBeanMapper;


	@Autowired
	private SysZoneBeanMapper zoneBeanMapper;

	@Override
	public void addOpenZone(SysOpenZoneBean openZoneBean) {
		openZoneBeanMapper.insert(openZoneBean);
	}

	@Override
	public void updateOpenZone(SysOpenZoneBean openZoneBean) {
		openZoneBeanMapper.updateById(openZoneBean);
	}

	@Override
	public List<OpenZoneResponse> findList() {
		List<OpenZoneResponse> openZoneResponses = null;
		List<SysOpenZoneBean> openZoneBeanList = openZoneBeanMapper.selectList(new EntityWrapper<SysOpenZoneBean>().eq("del_status", 1));
		if (openZoneBeanList != null && openZoneBeanList.size() > 0) {
			openZoneResponses = new ArrayList<OpenZoneResponse>();
			for (SysOpenZoneBean openZoneBean : openZoneBeanList) {
				OpenZoneResponse zoneResponse = new OpenZoneResponse();
				try {
					PropertyUtils.copyProperties(zoneResponse, openZoneBean);
					zoneResponse.setOpenZoneId(openZoneBean.getId());
				} catch (Exception e) {
					throw new SystemException("system copy error");
				}
				openZoneResponses.add(zoneResponse);
			}
		}
		return openZoneResponses;
	}

	@Override
	public OpenZoneResponse findByOpenZoneId(Long openZoneId) {
		OpenZoneResponse response = null;
		SysOpenZoneBean openZoneBean = openZoneBeanMapper.selectById(openZoneId);
		if (openZoneBean != null) {
			response = new OpenZoneResponse();
			try {
				PropertyUtils.copyProperties(response, openZoneBean);
				response.setOpenZoneId(openZoneBean.getId());
			} catch (Exception e) {
				throw new SystemException("system copy error");
			}
		}
		return response;
	}

	@Override
	public SysOpenZoneBean getById(Long id) {

		return openZoneBeanMapper.selectById(id);
	}

	@Override
	public Page<SysOpenZoneBean> findPage(BasePageRequest pageRequest) {

		Page<SysOpenZoneBean> page = new Page<SysOpenZoneBean>(pageRequest.getPageNum(), pageRequest.getPageSize());
		int offset = 0;
		int lastTotalCount = 0;
		if (!StringUtils.isEmpty(pageRequest.getBoundValues())) {
			lastTotalCount = Integer.parseInt(pageRequest.getBoundValues());
		}
		int currentTotal = openZoneBeanMapper.selectCount(null);

		if (currentTotal > 0) {
			if (lastTotalCount != 0 && lastTotalCount > 0) {
				offset = currentTotal - lastTotalCount;
			}
			offset = pageRequest.getPageStartIndex() + offset;
			RowBounds rowBounds = new RowBounds(offset, pageRequest.getPageSize());
			List<SysOpenZoneBean> sysDataTypeBeanList = openZoneBeanMapper.selectPage(rowBounds, null);
			page.setRecords(sysDataTypeBeanList);
			page.setTotal(currentTotal);
		}
		return page;
	}

	@Override
	public boolean isExistOpenZone(String provinceCode, String cityCode) {
		int count = openZoneBeanMapper.selectCount(new EntityWrapper<SysOpenZoneBean>().eq("province_code", provinceCode).eq("city_code", cityCode));
		if (count > 0) {
			return true;
		}
		return false;
	}

	@Override
	public SysOpenZoneBean getOpenZoneByName(String provinceName, String cityName) {
		List<SysOpenZoneBean> openZoneBeanList = openZoneBeanMapper.selectList(new EntityWrapper<SysOpenZoneBean>().eq("province_name", provinceName).eq("city_name", cityName));
		if (openZoneBeanList != null && openZoneBeanList.size() > 0) {

			return openZoneBeanList.get(0);
		}
		return null;
	}


	//	@Transactional
	public void saveZone(ZoneResponse zoneResponse) {
		SysZoneBean zoneBean = new SysZoneBean();
		zoneBean.setParentZoneCode(zoneResponse.getParentZoneCode());
		zoneBean.setZoneCode(zoneResponse.getZoneCode());
		zoneBean.setZoneName(zoneResponse.getZoneName());
		zoneBean.setZoneType(zoneResponse.getType(zoneResponse.getZoneType()));
		zoneBean.setDelStatus(1);
		zoneBean.preInsert();
		zoneBeanMapper.insert(zoneBean);
	}

	private List<ZoneResponse> findAllZones(String parentZoneCode) {

		List<ZoneResponse> zoneResponseList = null;

		List<SysZoneBean> zoneBeanList = zoneBeanMapper.selectList(new EntityWrapper<SysZoneBean>().eq("parent_zone_code", parentZoneCode));
		if (zoneBeanList != null && zoneBeanList.size() > 0) {
			zoneResponseList = new ArrayList<ZoneResponse>();
			for (SysZoneBean zone : zoneBeanList) {
				ZoneResponse zoneResponse = new ZoneResponse();
				zoneResponse.setZoneCode(zone.getZoneCode());
				zoneResponse.setZoneName(zone.getZoneName());
				zoneResponse.setZoneType(SysZoneBean.getTye(zone.getZoneType()));
				zoneResponse.setParentZoneCode(parentZoneCode);
				zoneResponse.setId(zone.getId());
				zoneResponseList.add(zoneResponse);
			}
		}

		return zoneResponseList;
	}


	public List<ZoneResponse> findAllNations() {
		String parentZoneCode = null;
		return findAllZones(parentZoneCode);
	}

	public List<ZoneResponse> findAllProvinces() {
		String parentZoneCode = "1";
		return findAllZones(parentZoneCode);
	}

	public List<ZoneResponse> findAllCitiesByParentZoneCode(String parentZoneCode) {
		if (StringUtils.isEmpty(parentZoneCode)) {
//			throw new LcException("地区编号不能为空");
		}
		return findAllZones(parentZoneCode);
	}

	public List<ZoneResponse> findAllAreasByParentZoneCode(String parentZoneCode) {
		if (StringUtils.isEmpty(parentZoneCode)) {
//			throw new LcException("地区编号不能为空");
		}
		return findAllZones(parentZoneCode);
	}
}
