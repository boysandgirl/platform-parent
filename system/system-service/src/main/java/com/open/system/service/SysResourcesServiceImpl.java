package com.open.system.service;


import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.open.comm.service.BaseServiceImpl;
import com.open.system.dao.mapper.SysResourcesBeanMapper;
import com.open.system.dao.mapper.SysRoleToResourceBeanMapper;
import com.open.system.domain.resource.SysResourcesBean;
import com.open.system.response.ResourceInfoResponse;
import com.open.system.service.api.ISysResourcesService;
import org.apache.commons.beanutils.PropertyUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * 系统资源具体实现业务
 *
 * @Date 2017-06-05
 * @Time 18:19
 * @Author 徐福周
 */
@Service
public class SysResourcesServiceImpl extends BaseServiceImpl<SysResourcesBeanMapper, SysResourcesBean> implements ISysResourcesService {


	@Autowired
	private SysRoleToResourceBeanMapper roleToResourceBeanMapper;

	@Autowired
	private SysResourcesBeanMapper resourcesBeanMapper;

	@Override
	public List<SysResourcesBean> findResourcesByParentId(Long parentId) {
		return null;
	}

	@Override
	public List<ResourceInfoResponse> findResesByRoleId(Long roleId) {

		List<ResourceInfoResponse> targetReses = null;
		List<SysResourcesBean> resourcesBeanList = resourcesBeanMapper.findResourceByRoleId(roleId);
		if (resourcesBeanList != null && resourcesBeanList.size() > 0) {
			targetReses = new ArrayList<ResourceInfoResponse>();
			for (SysResourcesBean res : resourcesBeanList
					) {
				ResourceInfoResponse dest = new ResourceInfoResponse();
				try {
					PropertyUtils.copyProperties(dest, res);
					dest.setbCheck(true);
					if (dest.getParentId() == null || dest.getParentId() == 0) {
						dest.setbParent(true);
					} else {
						dest.setbParent(false);
					}
				} catch (Exception e) {
					e.printStackTrace();
				}

				targetReses.add(dest);
			}
		}
		return targetReses;
	}

	@Override
	public List<ResourceInfoResponse> findAllResources() {
		List<ResourceInfoResponse> allReses = null;
		EntityWrapper<SysResourcesBean> wrapper = new EntityWrapper<SysResourcesBean>();
		wrapper.setSqlSelect(" id , name ,parent_id ");
		wrapper.eq(" del_status", 1);
		wrapper.eq("have_show", 1);
		List<SysResourcesBean> resourcesBeanList = resourcesBeanMapper.selectList(wrapper);
		if (resourcesBeanList != null && resourcesBeanList.size() > 0) {
			allReses = new ArrayList<ResourceInfoResponse>();
			for (SysResourcesBean res : resourcesBeanList
					) {
				ResourceInfoResponse dest = new ResourceInfoResponse();
				try {
					PropertyUtils.copyProperties(dest, res);
					dest.setbCheck(false);
					if (dest.getParentId() == null || dest.getParentId() == 0) {
						dest.setbParent(true);
					} else {
						dest.setbParent(false);
					}
				} catch (Exception e) {
					e.printStackTrace();
				}

				allReses.add(dest);
			}
		}
		return allReses;
	}
}
