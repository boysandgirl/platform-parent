package com.open.system.request;

import java.io.Serializable;

/**
 * 员工添加请求
 *
 * @date: 2017-06-21 23:03
 * @author: xfz
 */
public class EmployeeRequest implements Serializable {

    private Long id;

    //登入名
    private String accountName;


    private Integer delStatus;

    //密码
    private String password;

    //年龄 默认为0
    private Integer age;

    //1为男  2为女  0 未知   默认未知0
    private Integer sex;

    //手机号
    private String phone;

    // 姓名

    private String name;

    // 岗位

    private String  roleIds;

    //描述
    private String description;

    /**
     * 用户所处开放城市
     */
    private Long openZoneId;

    private String provinceName;

    private String cityName;

    private String areaName;

    private String provinceCode;

    private String cityCode;

    private String areaCode;

    /**
     * 可查看的所有开放城市  多个城市 逗号隔开
     */
    private String canLookOpenZoneIds;


    public String getCanLookOpenZoneIds() {
        return canLookOpenZoneIds;
    }

    public void setCanLookOpenZoneIds(String canLookOpenZoneIds) {
        this.canLookOpenZoneIds = canLookOpenZoneIds;
    }

    public Long getOpenZoneId() {
        return openZoneId;
    }

    public void setOpenZoneId(Long openZoneId) {
        this.openZoneId = openZoneId;
    }

    public String getProvinceName() {
        return provinceName;
    }

    public void setProvinceName(String provinceName) {
        this.provinceName = provinceName;
    }

    public String getCityName() {
        return cityName;
    }

    public void setCityName(String cityName) {
        this.cityName = cityName;
    }

    public String getAreaName() {
        return areaName;
    }

    public void setAreaName(String areaName) {
        this.areaName = areaName;
    }

    public String getProvinceCode() {
        return provinceCode;
    }

    public void setProvinceCode(String provinceCode) {
        this.provinceCode = provinceCode;
    }

    public String getCityCode() {
        return cityCode;
    }

    public void setCityCode(String cityCode) {
        this.cityCode = cityCode;
    }

    public String getAreaCode() {
        return areaCode;
    }

    public void setAreaCode(String areaCode) {
        this.areaCode = areaCode;
    }

    public Integer getDelStatus() {
        return delStatus;
    }

    public void setDelStatus(Integer delStatus) {
        this.delStatus = delStatus;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getAccountName() {
        return accountName;
    }

    public void setAccountName(String accountName) {
        this.accountName = accountName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public Integer getSex() {
        return sex;
    }

    public void setSex(Integer sex) {
        this.sex = sex;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getRoleIds() {
        return roleIds;
    }

    public void setRoleIds(String roleIds) {
        this.roleIds = roleIds;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
