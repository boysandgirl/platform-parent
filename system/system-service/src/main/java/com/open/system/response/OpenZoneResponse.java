package com.open.system.response;

import java.io.Serializable;

/**
 * 开放城市 返回参数
 *
 * @date: 2017-06-11 20:49
 * @author: xfz
 */

public class OpenZoneResponse implements Serializable {

    //开放区域id

    private Long openZoneId;
    //开放所属省份编码

    private String provinceCode;
    //开放所属省份名称

    private String provinceName;
    //开放所属城市code

    private String cityCode;
    //开放所属城市名称

    private String cityName;

    public Long getOpenZoneId() {
        return openZoneId;
    }

    public void setOpenZoneId(Long openZoneId) {
        this.openZoneId = openZoneId;
    }

    public String getProvinceCode() {
        return provinceCode;
    }

    public void setProvinceCode(String provinceCode) {
        this.provinceCode = provinceCode;
    }

    public String getProvinceName() {
        return provinceName;
    }

    public void setProvinceName(String provinceName) {
        this.provinceName = provinceName;
    }

    public String getCityCode() {
        return cityCode;
    }

    public void setCityCode(String cityCode) {
        this.cityCode = cityCode;
    }

    public String getCityName() {
        return cityName;
    }

    public void setCityName(String cityName) {
        this.cityName = cityName;
    }
}
