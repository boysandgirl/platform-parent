package com.open.system.response;


import com.open.comm.utils.poi.MapperCell;

import java.io.Serializable;

/**
 * @Description 省市区 返回参数
 * <p>
 * Created by 徐福周 on 2017/6/1.
 */
public class ZoneResponse implements Serializable {

	private Long id;
	/**
	 * 地区编号
	 */
	@MapperCell(cellName = "areaCode", order = 0)
	private String zoneCode;

	/**
	 * 地区名称
	 */
	@MapperCell(cellName = "areaName", order = 0)
	private String zoneName;

	/**
	 * 上级的地区编号
	 */
	@MapperCell(cellName = "areaParentId", order = 0)
	private String parentZoneCode;
	/**
	 * 地区类型
	 */
	@MapperCell(cellName = "areaType", order = 0)
	private int zoneType;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getZoneCode() {
		return zoneCode;
	}

	public void setZoneCode(String zoneCode) {
		this.zoneCode = zoneCode;
	}

	public String getZoneName() {
		return zoneName;
	}

	public void setZoneName(String zoneName) {
		this.zoneName = zoneName;
	}

	public int getZoneType() {
		return zoneType;
	}

	public void setZoneType(int zoneType) {
		this.zoneType = zoneType;
	}

	public String getParentZoneCode() {
		return parentZoneCode;
	}

	public void setParentZoneCode(String parentZoneCode) {
		this.parentZoneCode = parentZoneCode;
	}


	public Byte getType(int zoneType) {
		Byte type = 0;
		if (zoneType == 1) {
			type = 1;
		} else if (zoneType == 2) {
			type = 2;
		} else if (zoneType == 3) {
			type = 3;
		} else if (zoneType == 4) {
			type = 4;
		}

		return type;
	}

	@Override
	public String toString() {
		return "ZoneResponse{" +
				"zoneCode='" + zoneCode + '\'' +
				", zoneName='" + zoneName + '\'' +
				", parentZoneCode='" + parentZoneCode + '\'' +
				", zoneType=" + zoneType +
				'}';
	}
}
