package com.open.system.service.api;


import com.open.comm.service.IBaseService;
import com.open.system.domain.resource.SysResourcesBean;
import com.open.system.response.ResourceInfoResponse;

import java.util.List;

/**
 *
 */
public interface ISysResourcesService extends IBaseService<SysResourcesBean> {


	/**
	 * 根据父级id 获取下一级的菜单
	 *
	 * @param parentId
	 * @return
	 */
	public List<SysResourcesBean> findResourcesByParentId(Long parentId);


	public List<ResourceInfoResponse> findResesByRoleId(Long roleId);

	public List<ResourceInfoResponse> findAllResources();
}
