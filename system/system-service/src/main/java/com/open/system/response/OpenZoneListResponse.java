package com.open.system.response;

import java.util.List;

/**
 * 开放区域集合
 *
 * @date: 2017-06-11 20:55
 * @author: xfz
 */

public class OpenZoneListResponse  {


    private Long openZoneId;

    private  String provinceCode;

    private  String provinceName;


    private  String currentCityCode;

    private  String currentCityName;


    private boolean haveOpen=true;


    private List<OpenZoneResponse> zoneResponses;


    public String getProvinceCode() {
        return provinceCode;
    }

    public void setProvinceCode(String provinceCode) {
        this.provinceCode = provinceCode;
    }

    public String getProvinceName() {
        return provinceName;
    }

    public void setProvinceName(String provinceName) {
        this.provinceName = provinceName;
    }

    public String getCurrentCityCode() {
        return currentCityCode;
    }

    public void setCurrentCityCode(String currentCityCode) {
        this.currentCityCode = currentCityCode;
    }

    public String getCurrentCityName() {
        return currentCityName;
    }

    public void setCurrentCityName(String currentCityName) {
        this.currentCityName = currentCityName;
    }

    public boolean isHaveOpen() {
        return haveOpen;
    }
    public void setHaveOpen(boolean haveOpen) {
        this.haveOpen = haveOpen;
    }

    public List<OpenZoneResponse> getZoneResponses() {
        return zoneResponses;
    }

    public void setZoneResponses(List<OpenZoneResponse> zoneResponses) {
        this.zoneResponses = zoneResponses;
    }

    public Long getOpenZoneId() {
        return openZoneId;
    }

    public void setOpenZoneId(Long openZoneId) {
        this.openZoneId = openZoneId;
    }
}
