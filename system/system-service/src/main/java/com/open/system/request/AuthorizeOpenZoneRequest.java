package com.open.system.request;

import java.io.Serializable;

/**
 * 为用户授权可查看的城市
 *
 * @date: 2017-06-25 12:39
 * @author: xfz
 */
public class AuthorizeOpenZoneRequest implements Serializable{
    private Long userId;

    private String  openZoneIds;

    private String departmentIds;
    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getOpenZoneIds() {
        return openZoneIds;
    }

    public void setOpenZoneIds(String openZoneIds) {
        this.openZoneIds = openZoneIds;
    }

    public String getDepartmentIds() {
        return departmentIds;
    }

    public void setDepartmentIds(String departmentIds) {
        this.departmentIds = departmentIds;
    }
}
