package com.open.system.service.api;


import com.baomidou.mybatisplus.plugins.Page;
import com.open.comm.service.IBaseService;
import com.open.comm.utils.page.BasePageRequest;
import com.open.system.dao.response.EmployeeInfoResponse;
import com.open.system.domain.user.SysEmployeeBean;
import com.open.system.request.AuthorizeOpenZoneRequest;
import com.open.system.request.EmployeeRequest;

/**
 * 员工
 *
 * @date: 2017-06-13 17:30
 * @author: hlc
 */
public interface ISysEmployeeService extends IBaseService<SysEmployeeBean> {

	public Page<EmployeeInfoResponse> findPageEmployees(BasePageRequest pageRequest);

	public int isExistAccountName(String accountName);

	public void addUser(EmployeeRequest employeeBean);

	public EmployeeRequest findUserById(Long userId);


	public void updateUser(EmployeeRequest employeeRequest);

	public void addAuthorizeOpenZone(AuthorizeOpenZoneRequest openZoneRequest);

	public String findOpenZonesByEmployeeId(Long employeeId);

}
