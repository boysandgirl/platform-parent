package com.open.system.service;


import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.open.comm.service.BaseServiceImpl;
import com.open.system.dao.mapper.SysEmployeeToRoleBeanMapper;
import com.open.system.dao.mapper.SysRoleBeanMapper;
import com.open.system.dao.mapper.SysRoleToResourceBeanMapper;
import com.open.system.domain.role.SysEmployeeToRoleBean;
import com.open.system.domain.role.SysRoleBean;
import com.open.system.domain.role.SysRoleToResourceBean;
import com.open.system.service.api.ISysRoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * 系统角色服务实现
 *
 * @date: 2017-06-19 23:49
 * @author: xfz
 */
@Service
public class SysRoleServiceImpl extends BaseServiceImpl<SysRoleBeanMapper, SysRoleBean> implements ISysRoleService {

	@Autowired
	private SysEmployeeToRoleBeanMapper employeeToRoleBeanMapper;

	@Autowired
	private SysRoleBeanMapper roleBeanMapper;


	@Autowired
	private SysRoleToResourceBeanMapper roleToResourceBeanMapper;

	@Override
	public List<SysRoleBean> findRoleByUserId(Long employeeId) {
		List<SysRoleBean> roleBeanList = null;
		List<SysEmployeeToRoleBean> employeeToRoleBeans = employeeToRoleBeanMapper.selectList(new EntityWrapper<SysEmployeeToRoleBean>().eq("employee_id", employeeId)
				.eq("del_status", 1));
		if (employeeToRoleBeans != null && employeeToRoleBeans.size() > 0) {
			roleBeanList = new ArrayList<SysRoleBean>();

			for (SysEmployeeToRoleBean employeeToRoleBean : employeeToRoleBeans) {
				roleBeanList.add(selectById(employeeToRoleBean.getRoleId()));
			}

		}
		return roleBeanList;
	}

	@Transactional
	@Override
	public void addResesByRoleId(String resIds, Long roleId) {

		SysRoleBean roleBean = super.selectById(roleId);
		if (roleBean != null) {
			if (!StringUtils.isEmpty(resIds)) {
				if (resIds.length() > 0) {
					String resIdsStr[] = resIds.split(",");
					roleToResourceBeanMapper.delete(new EntityWrapper<SysRoleToResourceBean>().eq("role_id", roleBean.getId()));
					for (String resIdStr : resIdsStr) {
						Long resId = Long.parseLong(resIdStr);

						SysRoleToResourceBean rs = new SysRoleToResourceBean();
						rs.setResourceId(resId);
						rs.setRoleId(roleId);
						rs.preInsert();
						roleToResourceBeanMapper.insert(rs);
					}

				}
			}
		}
	}
}
