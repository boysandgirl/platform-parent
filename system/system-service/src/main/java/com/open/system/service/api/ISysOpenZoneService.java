package com.open.system.service.api;


import com.baomidou.mybatisplus.plugins.Page;
import com.open.comm.service.IBaseService;
import com.open.comm.utils.page.BasePageRequest;
import com.open.system.domain.zone.SysOpenZoneBean;
import com.open.system.response.OpenZoneResponse;
import com.open.system.response.ZoneResponse;

import java.util.List;


/**
 * 字典开放城市【目前做到所对应的市区】如厦门市
 *
 * @date: 2017-06-15 0:01
 * @author: xfz
 */
public interface ISysOpenZoneService extends IBaseService<SysOpenZoneBean> {

	public void addOpenZone(SysOpenZoneBean openZoneBean);

	public void updateOpenZone(SysOpenZoneBean openZoneBean);

	//获取所有的开放城市
	public List<OpenZoneResponse> findList();

	public OpenZoneResponse findByOpenZoneId(Long openZoneId);

	public SysOpenZoneBean getById(Long id);

	public Page<SysOpenZoneBean> findPage(BasePageRequest pageRequest);


	public boolean isExistOpenZone(String provinceCode, String cityCode);

	public SysOpenZoneBean getOpenZoneByName(String provinceName, String cityName);


	public void saveZone(ZoneResponse zoneResponse);

	public List<ZoneResponse> findAllNations();

	public List<ZoneResponse> findAllProvinces();

	public List<ZoneResponse> findAllCitiesByParentZoneCode(String parentZoneCode);

	public List<ZoneResponse> findAllAreasByParentZoneCode(String parentZoneCode);

}
