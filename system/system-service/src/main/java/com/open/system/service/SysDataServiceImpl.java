package com.open.system.service;


import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.open.comm.utils.exception.LcException;
import com.open.comm.utils.page.BasePageRequest;
import com.open.system.dao.mapper.SysDataResourceBeanMapper;
import com.open.system.dao.mapper.SysDataTypeBeanMapper;
import com.open.system.domain.data.SysDataResourceBean;
import com.open.system.domain.data.SysDataTypeBean;
import com.open.system.response.DataCommResponse;
import com.open.system.service.api.ISysDataService;
import org.apache.commons.beanutils.PropertyUtils;
import org.apache.ibatis.session.RowBounds;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * abc系统数据字典服务实现
 *
 * @Date 2017-06-13
 * @Time 17:50
 * @Author 徐福周
 */
@Component
public class SysDataServiceImpl implements ISysDataService {
	@Autowired
	private SysDataTypeBeanMapper dataTypeBeanMapper;

	@Autowired
	private SysDataResourceBeanMapper dataResourceBeanMapper;

	@Override
	public Integer addSysDataType(SysDataTypeBean var1) {
		return dataTypeBeanMapper.insert(var1);
	}

	@Override
	public Integer addSysDataResource(SysDataResourceBean var1) {
		return dataResourceBeanMapper.insert(var1);
	}

	@Override
	public void deleteDataResource(Long id) {
		dataResourceBeanMapper.deleteById(id);
	}

	@Override
	public void deleteDataType(Long id) {
		dataTypeBeanMapper.deleteById(id);
	}

	@Override
	public Integer updateDataType(SysDataTypeBean var1) {
		return dataTypeBeanMapper.updateById(var1);
	}

	@Override
	public Integer updateDataResource(SysDataResourceBean var1) {
		return dataResourceBeanMapper.updateById(var1);
	}

	@Override
	public SysDataTypeBean selectDataTypeById(Serializable var1) {
		return dataTypeBeanMapper.selectById(var1);
	}

	@Override
	public SysDataResourceBean selectDataResourceById(Serializable var1) {
		return dataResourceBeanMapper.selectById(var1);
	}


	public List<SysDataTypeBean> findParentDataTypes(Integer haveChild) {
		EntityWrapper<SysDataTypeBean> wrapper = new EntityWrapper<SysDataTypeBean>();

		wrapper.eq("del_status", 1);
		if (haveChild != null) {
			if (haveChild.compareTo(1) == 0) {//有下一级
				//
				wrapper.eq("have_child", haveChild);
			} else {
				wrapper.eq("have_child", 0);
			}
		}

		return dataTypeBeanMapper.selectList(wrapper);//未删除为1

	}

	@Override
	public Page<SysDataTypeBean> selectDataTypePage(BasePageRequest pageRequest) {

		Page<SysDataTypeBean> page = new Page<SysDataTypeBean>(pageRequest.getPageNum(), pageRequest.getPageSize());
		int offset = 0;
		int currentTotal = dataTypeBeanMapper.selectCount(null);
		if (currentTotal > 0) {
			offset = pageRequest.getPageStartIndex();
			RowBounds rowBounds = new RowBounds(offset, pageRequest.getPageSize());
			List<SysDataTypeBean> sysDataTypeBeanList = dataTypeBeanMapper.selectPage(rowBounds, null);
			page.setRecords(sysDataTypeBeanList);
			page.setTotal(currentTotal);
		}
		return page;
	}

	@Override
	public Page<SysDataResourceBean> selectDataResourcePage(BasePageRequest pageRequest, Long dataTypeId) {

		Page<SysDataResourceBean> page = new Page<SysDataResourceBean>(pageRequest.getPageNum(), pageRequest.getPageSize());
		int offset = 0;
		int lastTotalCount = 0;
		if (!StringUtils.isEmpty(pageRequest.getBoundValues())) {
			lastTotalCount = Integer.parseInt(pageRequest.getBoundValues());
		}
		int currentTotal = dataResourceBeanMapper.selectCount(null);

		if (currentTotal > 0) {
			if (lastTotalCount != 0 && lastTotalCount > 0) {
				offset = currentTotal - lastTotalCount;
			}
			offset = pageRequest.getPageStartIndex() + offset;
			RowBounds rowBounds = new RowBounds(offset, pageRequest.getPageSize());
			EntityWrapper<SysDataResourceBean> wrapper = new EntityWrapper<SysDataResourceBean>();
			if (dataTypeId != null && dataTypeId > 0) {
				wrapper.eq("data_type_id", dataTypeId);
			}
			wrapper.orderBy("data_type_id");
//			wrapper.groupBy("data_type_id");
			List<SysDataResourceBean> sysDataTypeBeanList = dataResourceBeanMapper.selectPage(rowBounds, wrapper);
			page.setRecords(sysDataTypeBeanList);
			page.setTotal(currentTotal);
		}
		return page;
	}

	@Override
	public boolean isExistDataResourceCode(Long dataTypeId, String dataCode) {

		int count = dataResourceBeanMapper.selectCount(new EntityWrapper<SysDataResourceBean>().eq("data_type_id", dataTypeId).eq("data_code", dataCode));
		if (count > 0) {
			return true;
		}
		return false;
	}

	@Override
	public List<DataCommResponse> findCommDatas(Long dataTypeId) {
		List<DataCommResponse> responses = null;
		List<SysDataResourceBean> dataResourceBeanList = dataResourceBeanMapper.selectList(new EntityWrapper<SysDataResourceBean>().eq("data_type_id", dataTypeId).eq("del_status", 1));
		if (dataResourceBeanList != null && dataResourceBeanList.size() > 0) {
			responses = new ArrayList<DataCommResponse>();
			for (SysDataResourceBean resource : dataResourceBeanList
					) {
				DataCommResponse response = new DataCommResponse();
				try {
					PropertyUtils.copyProperties(response, resource);
				} catch (Exception e) {
					throw new LcException("system parse error");
				}
				responses.add(response);
			}
		}
		return responses;
	}
}
