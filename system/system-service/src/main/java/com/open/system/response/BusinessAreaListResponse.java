package com.open.system.response;

import java.io.Serializable;

/**
 * 商圈集合返回
 *
 * @date: 2017-06-19 2:05
 * @author: xfz
 */
public class BusinessAreaListResponse implements Serializable {

    //商圈id
    private Long id;

    //商圈名称
    private String name;

    //开放城市id
    /*private Long openZoneId;

    private String cityName;

    private String cityCode;

    private String areaCode;

    private String areaName;

    private String description;*/

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    /*public Long getOpenZoneId() {
        return openZoneId;
    }

    public void setOpenZoneId(Long openZoneId) {
        this.openZoneId = openZoneId;
    }

    public String getCityName() {
        return cityName;
    }

    public void setCityName(String cityName) {
        this.cityName = cityName;
    }

    public String getCityCode() {
        return cityCode;
    }

    public void setCityCode(String cityCode) {
        this.cityCode = cityCode;
    }

    public String getAreaCode() {
        return areaCode;
    }

    public void setAreaCode(String areaCode) {
        this.areaCode = areaCode;
    }

    public String getAreaName() {
        return areaName;
    }

    public void setAreaName(String areaName) {
        this.areaName = areaName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }*/
}
