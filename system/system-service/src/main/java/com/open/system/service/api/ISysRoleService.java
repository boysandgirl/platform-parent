package com.open.system.service.api;


import com.open.comm.service.IBaseService;
import com.open.system.domain.role.SysRoleBean;

import java.util.List;

/**
 * 系统角色接口服务  目前设立一个用户一个角色处理
 *
 * @date: 2017-06-19 23:50
 * @author: xfz
 */
public interface ISysRoleService extends IBaseService<SysRoleBean> {

	public List<SysRoleBean> findRoleByUserId(Long employeeId);

	/**
	 * @param resIds 资源ids  逗号隔开
	 * @param roleId 角色id
	 * @description: 为角色添加 资源权限信息
	 * @date: 2017/6/25 3:08
	 * @author: xfz
	 */

	public void addResesByRoleId(String resIds, Long roleId);
}
