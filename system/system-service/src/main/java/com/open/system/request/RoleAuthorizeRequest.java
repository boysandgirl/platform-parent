package com.open.system.request;

import java.io.Serializable;

/**
 * 角色资源授权
 *
 * @date: 2017-06-25 15:43
 * @author: xfz
 */
public class RoleAuthorizeRequest implements Serializable {
	private Long roleId;

	private String resIds;

	public Long getRoleId() {
		return roleId;
	}

	public void setRoleId(Long roleId) {
		this.roleId = roleId;
	}

	public String getResIds() {
		return resIds;
	}

	public void setResIds(String resIds) {
		this.resIds = resIds;
	}
}
