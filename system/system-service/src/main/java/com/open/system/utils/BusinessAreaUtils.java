package com.open.system.utils;

import com.open.comm.utils.exception.SystemException;
import com.open.system.domain.data.SysBusinessAreaBean;
import com.open.system.response.BusinessAreaResponse;
import org.apache.commons.beanutils.PropertyUtils;

/**
 * 商圈转化工具
 *
 * @date: 2017-06-18 22:48
 * @author: xfz
 */
public class BusinessAreaUtils {

	public static void convert(BusinessAreaResponse target, SysBusinessAreaBean resource) {
		try {
			PropertyUtils.copyProperties(target, resource);
		} catch (Exception e) {
			throw new SystemException("system parse error");
		}
	}
}
