package com.open.system.response;

import java.io.Serializable;

/**
 * 资源基础实体类
 *
 * @date: 2017-06-24 1:38
 * @author: xfz
 */
public class ResourceInfoResponse implements Serializable {

    private Long id;

    private Long parentId;

    private String name;

    private String permission;

    private Boolean bParent;

    //是否被选中  跟所有的资源对比时
    private Boolean  bCheck;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getParentId() {
        return parentId;
    }

    public void setParentId(Long parentId) {
        this.parentId = parentId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Boolean isbParent() {
        return bParent;
    }

    public void setbParent(Boolean bParent) {
        this.bParent = bParent;
    }

    public Boolean getbCheck() {
        return bCheck;
    }

    public void setbCheck(Boolean bCheck) {
        this.bCheck = bCheck;
    }

    public String getPermission() {
        return permission;
    }

    public void setPermission(String permission) {
        this.permission = permission;
    }
}
