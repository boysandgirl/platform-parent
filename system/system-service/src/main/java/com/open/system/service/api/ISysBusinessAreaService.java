package com.open.system.service.api;


import com.baomidou.mybatisplus.plugins.Page;
import com.open.comm.service.IBaseService;
import com.open.comm.utils.page.BasePageRequest;
import com.open.system.domain.data.SysBusinessAreaBean;
import com.open.system.response.BusinessAreaListResponse;
import com.open.system.response.BusinessAreaResponse;

import java.util.List;

/**
 * 字典商圈服务
 *
 * @date: 2017-06-14 23:23
 * @author: xfz
 */
public interface ISysBusinessAreaService extends IBaseService<SysBusinessAreaBean> {

	public void addBusinessArea(SysBusinessAreaBean businessAreaBean);

	public void updateBusinessArea(SysBusinessAreaBean businessAreaBean);

	public BusinessAreaResponse findById(Long id);

	public List<BusinessAreaResponse> findList(Long openZoneId);

	public Page<BusinessAreaResponse> findPage(BasePageRequest pageRequest);

	List<BusinessAreaListResponse> appFindBusinessAreas(Long openZoneId, String areaCode);
}
