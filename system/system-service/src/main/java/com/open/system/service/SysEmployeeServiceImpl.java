package com.open.system.service;


import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.open.comm.service.BaseServiceImpl;
import com.open.comm.utils.exception.LcException;
import com.open.comm.utils.exception.ParameterException;
import com.open.comm.utils.exception.SystemException;
import com.open.comm.utils.page.BasePageRequest;
import com.open.system.dao.mapper.SysEmployeeBeanMapper;
import com.open.system.dao.mapper.SysEmployeeToOpenZoneBeanMapper;
import com.open.system.dao.mapper.SysEmployeeToRoleBeanMapper;
import com.open.system.dao.mapper.SysRoleBeanMapper;
import com.open.system.dao.response.EmployeeInfoResponse;
import com.open.system.domain.role.SysEmployeeToRoleBean;
import com.open.system.domain.user.SysEmployeeBean;
import com.open.system.domain.user.SysEmployeeToOpenZoneBean;
import com.open.system.request.AuthorizeOpenZoneRequest;
import com.open.system.request.EmployeeRequest;
import com.open.system.service.api.ISysEmployeeService;
import org.apache.commons.beanutils.PropertyUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import java.util.List;
import java.util.Map;

/**
 * 员工
 *
 * @date: 2017-06-13 17:32
 * @author: hlc
 */
@Service
public class SysEmployeeServiceImpl extends BaseServiceImpl<SysEmployeeBeanMapper, SysEmployeeBean> implements ISysEmployeeService {

	@Autowired
	private SysEmployeeBeanMapper employeeBeanMapper;

	@Autowired
	private SysRoleBeanMapper roleBeanMapper;

	@Autowired
	private SysEmployeeToRoleBeanMapper employeeToRoleBeanMapper;

	@Autowired
	private SysEmployeeToOpenZoneBeanMapper employeeToOpenZoneBeanMapper;

	public Page<EmployeeInfoResponse> findPageEmployees(BasePageRequest pageRequest) {

		Page<EmployeeInfoResponse> page = new Page<EmployeeInfoResponse>(pageRequest.getPageNum(), pageRequest.getPageSize());
		int currentTotal = employeeBeanMapper.selectCount(null);
		if (currentTotal > 0) {
			Map<String, Object> map = pageRequest.getMapParams();
			List<EmployeeInfoResponse> employeeInfoResponseList = null;
			employeeInfoResponseList = employeeBeanMapper.findEmployees(map);
			page.setRecords(employeeInfoResponseList);
			page.setTotal(currentTotal);
		}
		return page;
	}

	@Override
	public int isExistAccountName(String accountName) {
		int account = employeeBeanMapper.selectCount(new EntityWrapper<SysEmployeeBean>().eq("account_name", accountName));
		return account;
	}

	@Transactional
	@Override
	public void addUser(EmployeeRequest employeeBean) {
		if (StringUtils.isEmpty(employeeBean.getAccountName())) {
			throw new LcException("登入名不能为空");
		}
		if (StringUtils.isEmpty(employeeBean.getPassword())) {
			employeeBean.setPassword("123456");
		}
		SysEmployeeBean sysEmployeeBean = new SysEmployeeBean();

		try {
			PropertyUtils.copyProperties(sysEmployeeBean, employeeBean);
		} catch (Exception e) {
			throw new SystemException("system parse error");
		}
		sysEmployeeBean.preInsert();
		employeeBeanMapper.insert(sysEmployeeBean);

		if ((!StringUtils.isEmpty(employeeBean.getRoleIds())) && employeeBean.getRoleIds().length() > 0) {

			String roleIds[] = employeeBean.getRoleIds().split(",");
			for (String roleId : roleIds) {
				Long rId = Long.parseLong(roleId);
				SysEmployeeToRoleBean employeeToRoleBean = new SysEmployeeToRoleBean();
				employeeToRoleBean.setEmployeeId(sysEmployeeBean.getId());
				employeeToRoleBean.setRoleId(rId);
				employeeToRoleBean.preInsert();
				employeeToRoleBeanMapper.insert(employeeToRoleBean);
			}
		}


	}

	@Override
	public EmployeeRequest findUserById(Long userId) {
		EmployeeRequest request = null;
		SysEmployeeBean employeeBean = employeeBeanMapper.selectById(userId);
		if (employeeBean != null) {
			try {
				request = new EmployeeRequest();
				PropertyUtils.copyProperties(request, employeeBean);

				List<SysEmployeeToRoleBean> employeeToRoleBeans = employeeToRoleBeanMapper.selectList(new EntityWrapper<SysEmployeeToRoleBean>().eq("del_status", 1));
				if (employeeToRoleBeans != null && employeeToRoleBeans.size() > 0) {
					StringBuilder sb = new StringBuilder();
					for (SysEmployeeToRoleBean employeeToRoleBean : employeeToRoleBeans) {

						sb.append(employeeToRoleBean.getRoleId() + "").append(",");
					}
					request.setRoleIds(sb.toString().substring(0, sb.toString().length() - 1));
				}
			} catch (Exception e) {
				throw new SystemException(e.getMessage());
			}
		}
		return request;
	}

	@Transactional
	@Override
	public void updateUser(EmployeeRequest employeeRequest) {
		if (employeeRequest.getId() != null) {
			SysEmployeeBean employeeBean = super.selectById(employeeRequest.getId());
			if (employeeBean == null) {
				throw new LcException("用户不存在");
			}

			if (!StringUtils.isEmpty(employeeRequest.getRoleIds())) {


				String roleIds[] = employeeRequest.getRoleIds().split(",");

				if (roleIds != null && roleIds.length > 0) {
					employeeToRoleBeanMapper.delete(new EntityWrapper<SysEmployeeToRoleBean>().eq("employee_id", employeeBean.getId()));
					for (String roleId : roleIds) {
						Long rId = Long.parseLong(roleId);
						SysEmployeeToRoleBean employeeToRoleBean = new SysEmployeeToRoleBean();
						employeeToRoleBean.setRoleId(rId);
						employeeToRoleBean.setEmployeeId(employeeBean.getId());
						employeeToRoleBean.preInsert();
						employeeToRoleBeanMapper.insert(employeeToRoleBean);
					}
				}
			} else {
				employeeToRoleBeanMapper.delete(new EntityWrapper<SysEmployeeToRoleBean>().eq("employee_id", employeeBean.getId()));

			}
			//不能修改登入名
			if (StringUtils.isEmpty(employeeRequest.getPassword())) {
				employeeBean.setPassword("123456");
			} else {
				employeeBean.setPassword(employeeRequest.getPassword());
			}
			employeeBean.setName(employeeRequest.getName());
			employeeBean.setAge(employeeRequest.getAge());
			employeeBean.setSex(employeeRequest.getSex());
			employeeBean.setDescription(employeeRequest.getDescription());
			employeeBean.setDelStatus(employeeRequest.getDelStatus());
			super.insertOrUpdate(employeeBean);

		} else {
			throw new ParameterException("参数有误,id不能为空");
		}
	}

	@Transactional
	@Override
	public void addAuthorizeOpenZone(AuthorizeOpenZoneRequest openZoneRequest) {
		SysEmployeeBean employeeBean = super.selectById(openZoneRequest.getUserId());
		if (employeeBean == null) {
			throw new LcException("用户不存在");
		}

		if (StringUtils.isEmpty(openZoneRequest.getOpenZoneIds())) {
			throw new LcException("未指定开放城市");
		}
		employeeToOpenZoneBeanMapper.delete(new EntityWrapper<SysEmployeeToOpenZoneBean>().eq("employee_id", employeeBean.getId()));
		String openZoneIds[] = openZoneRequest.getOpenZoneIds().split(",");
		if (openZoneIds != null && openZoneIds.length > 0) {

			for (String openZoneId : openZoneIds
					) {
				Long zoneId = Long.parseLong(openZoneId);
				SysEmployeeToOpenZoneBean employeeToOpenZoneBean = new SysEmployeeToOpenZoneBean();
				employeeToOpenZoneBean.setEmployeeId(employeeBean.getId());
				employeeToOpenZoneBean.setOpenZoneId(zoneId);
				employeeToOpenZoneBean.preInsert();
				employeeToOpenZoneBeanMapper.insert(employeeToOpenZoneBean);
			}
		}
	}

	@Override
	public String findOpenZonesByEmployeeId(Long employeeId) {

		StringBuilder sb = new StringBuilder();
		List<SysEmployeeToOpenZoneBean> openZoneBeans = employeeToOpenZoneBeanMapper.selectList(new EntityWrapper<SysEmployeeToOpenZoneBean>().eq("employee_id", employeeId));
		if (openZoneBeans != null && openZoneBeans.size() > 0) {
			for (SysEmployeeToOpenZoneBean eo : openZoneBeans
					) {
				sb.append(eo.getOpenZoneId() + "").append(",");
			}

		}
		if (sb.length() > 0) {
			return sb.toString().substring(0, sb.toString().length() - 1);
		}
		return sb.toString();
	}

}
