package com.open.system.service.api;


import com.baomidou.mybatisplus.plugins.Page;
import com.open.comm.utils.page.BasePageRequest;
import com.open.system.domain.data.SysDataResourceBean;
import com.open.system.domain.data.SysDataTypeBean;
import com.open.system.response.DataCommResponse;

import java.io.Serializable;
import java.util.List;

/**
 * abc系统数据字典服务
 *
 * @Date 2017-06-13
 * @Time 17:50
 * @Author 徐福周
 */

public interface ISysDataService {

	Integer addSysDataType(SysDataTypeBean var1);

	Integer addSysDataResource(SysDataResourceBean var1);

	public void deleteDataType(Long id);

	public void deleteDataResource(Long id);

	Integer updateDataType(SysDataTypeBean var1);

	Integer updateDataResource(SysDataResourceBean var1);


	SysDataTypeBean selectDataTypeById(Serializable var1);

	SysDataResourceBean selectDataResourceById(Serializable var1);

	/**
	 * 获取所有的字典类型
	 *
	 * @param haveChild 是否有下一级 1有 0 没有
	 * @return
	 */
	List<SysDataTypeBean> findParentDataTypes(Integer haveChild);

	Page<SysDataTypeBean> selectDataTypePage(BasePageRequest pageRequest);

	Page<SysDataResourceBean> selectDataResourcePage(BasePageRequest pageRequest, Long dataTypeId);


	public boolean isExistDataResourceCode(Long dataTypeId, String dataCode);


	/**
	 * @param dataTypeId 数据字典类型
	 * @description: 根据数据字典类型获取对应的字典列表
	 * @date: 2017/6/17 23:28
	 * @author: xfz
	 */

	public List<DataCommResponse> findCommDatas(Long dataTypeId);
}
