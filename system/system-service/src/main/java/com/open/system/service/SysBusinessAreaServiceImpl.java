package com.open.system.service;


import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.mapper.Wrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.open.comm.service.BaseServiceImpl;
import com.open.comm.utils.exception.SystemException;
import com.open.comm.utils.page.BasePageRequest;
import com.open.system.dao.mapper.SysBusinessAreaBeanMapper;
import com.open.system.domain.data.SysBusinessAreaBean;
import com.open.system.response.BusinessAreaListResponse;
import com.open.system.response.BusinessAreaResponse;
import com.open.system.service.api.ISysBusinessAreaService;
import com.open.system.utils.BusinessAreaUtils;
import org.apache.commons.beanutils.PropertyUtils;
import org.apache.ibatis.session.RowBounds;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * 字典商圈服务实现
 *
 * @date: 2017-06-14 23:43
 * @author: xfz
 */
@Service
public class SysBusinessAreaServiceImpl extends BaseServiceImpl<SysBusinessAreaBeanMapper, SysBusinessAreaBean> implements ISysBusinessAreaService {

	@Autowired
	private SysBusinessAreaBeanMapper businessBeanMapper;

	@Override
	public void addBusinessArea(SysBusinessAreaBean businessAreaBean) {

		businessBeanMapper.insert(businessAreaBean);
	}

	@Override
	public void updateBusinessArea(SysBusinessAreaBean businessAreaBean) {

		businessBeanMapper.updateById(businessAreaBean);
	}

	@Override
	public BusinessAreaResponse findById(Long id) {
		BusinessAreaResponse response = null;
		SysBusinessAreaBean businessAreaBean = businessBeanMapper.selectById(id);
		if (businessAreaBean != null) {
			try {
				response = new BusinessAreaResponse();
				PropertyUtils.copyProperties(response, businessAreaBean);
			} catch (Exception e) {
				throw new SystemException("system copy error");
			}
		}
		return response;
	}

	@Override
	public List<BusinessAreaResponse> findList(Long openZoneId) {
		List<BusinessAreaResponse> responses = null;
		String sqlReturnSelect = " id,open_zone_id,province_code,province_name,city_code,city_name,area_code,area_name ";
		List<SysBusinessAreaBean> businessAreaBeans = businessBeanMapper.selectList(new EntityWrapper<SysBusinessAreaBean>().setSqlSelect(sqlReturnSelect).eq("open_zone_id", sqlReturnSelect).
				eq("del_status", 1));
		if (businessAreaBeans != null && businessAreaBeans.size() > 0) {
			responses = new ArrayList<BusinessAreaResponse>();
			for (SysBusinessAreaBean businessAreaBean : businessAreaBeans) {
				BusinessAreaResponse response = new BusinessAreaResponse();
				BusinessAreaUtils.convert(response, businessAreaBean);
				responses.add(response);
			}
		}
		return responses;
	}

	@Override
	public Page<BusinessAreaResponse> findPage(BasePageRequest pageRequest) {
		Page<BusinessAreaResponse> page = new Page<BusinessAreaResponse>(pageRequest.getPageNum(), pageRequest.getPageSize());
		int offset = 0;
		int lastTotalCount = 0;
		if (!StringUtils.isEmpty(pageRequest.getBoundValues())) {
			lastTotalCount = Integer.parseInt(pageRequest.getBoundValues());
		}
		int currentTotal = businessBeanMapper.selectCount(null);

		if (currentTotal > 0) {
			if (lastTotalCount != 0 && lastTotalCount > 0) {
				offset = currentTotal - lastTotalCount;
			}
			offset = pageRequest.getPageStartIndex() + offset;
			RowBounds rowBounds = new RowBounds(offset, pageRequest.getPageSize());
			List<SysBusinessAreaBean> businessAreaBeans = businessBeanMapper.selectPage(rowBounds, null);
			//包括所有的
			List<BusinessAreaResponse> responses = new ArrayList<BusinessAreaResponse>();
			for (SysBusinessAreaBean businessAreaBean : businessAreaBeans) {
				BusinessAreaResponse response = new BusinessAreaResponse();
				BusinessAreaUtils.convert(response, businessAreaBean);
				responses.add(response);
			}
			page.setRecords(responses);
			page.setTotal(currentTotal);
		}
		return page;
	}

	/**
	 * c端 根据 开放城市id ，查询商圈字典
	 *
	 * @param openZoneId
	 * @param areaCode
	 * @return
	 */
	@Override
	public List<BusinessAreaListResponse> appFindBusinessAreas(Long openZoneId, String areaCode) {

		Wrapper<SysBusinessAreaBean> wrapper = new EntityWrapper<SysBusinessAreaBean>();

		wrapper.setSqlSelect("id, name");

		wrapper.eq("open_zone_id", openZoneId);
		if (!StringUtils.isEmpty(areaCode)) {
			wrapper.eq("area_code", areaCode);
		}

		List<SysBusinessAreaBean> lists = businessBeanMapper.selectList(wrapper);
		List<BusinessAreaListResponse> responsesList = new ArrayList<BusinessAreaListResponse>();
		if (null != lists && lists.size() > 0) {
			BusinessAreaListResponse businessAreaListResponse = null;
			for (SysBusinessAreaBean businessAreaBean : lists) {
				businessAreaListResponse = new BusinessAreaListResponse();
				BeanUtils.copyProperties(businessAreaBean, businessAreaListResponse);

				responsesList.add(businessAreaListResponse);
			}
		}

		return responsesList;
	}
}
